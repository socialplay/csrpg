﻿//NSdesignGames @ 2012 - 2014
//FPS Kit | Version 2.0 + Multiplayer

using UnityEngine;
using System.Collections;

public class WeaponSync_Catcher : MonoBehaviour {

	//This script is used to catch fire messages from WeaponScript.js
	//Then pass those messages to WeaponSync.cs
	//Should be attached to every wepaon with WeaponScript.js
	
	Transform playerRoot;
	WeaponScript weapScript;
	PlayerNetworkController pnc;
	
	void Awake(){
		weapScript = gameObject.GetComponent<WeaponScript>();
		playerRoot = transform.root;
		pnc = playerRoot.GetComponent<PlayerNetworkController>();

		if(!weapScript){
			Debug.LogError("WeaponScript.js should be attached to same gameObject");
		}
	}

	public void Fire(){
		if(!weapScript){
			Debug.LogError("WeaponScript.js should be attached to same gameObject");
		}

		if(weapScript.GunType == WeaponScript.gunType.MACHINE_GUN){
			pnc.syncMachineGun(weapScript.errorAngle);
		}

		if(weapScript.GunType == WeaponScript.gunType.SHOTGUN){
			pnc.syncShotGun(weapScript.ShotGun.fractions);
		}

		if(weapScript.GunType == WeaponScript.gunType.GRENADE_LAUNCHER){
			pnc.syncGrenadeLauncher(weapScript.grenadeLauncher.initialSpeed);
		}

		if(weapScript.GunType == WeaponScript.gunType.KNIFE){
			pnc.syncKnife();
		}
	}
}
