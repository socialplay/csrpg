//NSdesignGames @ 2012 - 2013
//FPS Kit | Version 2.0 + Multiplayer

//This script will be attached automatically to every hit box
//It will receive damage and transmit it to PlayerDamage.cs

using UnityEngine;
using System.Collections;

public class HitBox : MonoBehaviour {
	
	public float damageMultiplier;
    public DamagableObject damagableObject;

	public void ApplyDamage(float damage){

        if (WeaponSlot.GetActiveWeapon() == null)
        {
            damage = 80;
        }
        else
        {
            damage = WeaponSlot.GetWeaponDamage(WeaponSlot.GetActiveWeapon());
        }

        int finalDamage = Mathf.RoundToInt(damage * damageMultiplier);

		//So we receive bullet damage and also add other damage value we have configured in PlayerDamage.cs
        damagableObject.TotalDamage(finalDamage, this);
	}
}
