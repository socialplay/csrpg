//NSdesignGames @ 2012 - 2014
//FPS Kit | Version 2.0 + Multiplayer


using UnityEngine;
using System.Collections;

[RequireComponent (typeof (AudioSource))]

public class WeaponSync : MonoBehaviour {
	//This script is used to controll third person weapons over network, It receives messages from PlayerNetworkController.js

	public WeaponScript firstPersonWeapon; //Assign respective first person weapon
	public Transform firePoint;
	public GameObject bullet; //Assign this if we synchronize MACHINE_GUN or SHOTGUN or MELEE
	public Renderer muzzleFlash; //Assign only for MACHINE_GUN
	public Rigidbody projectile; //Assign this if we synchronize GRENADE_LAUNCHER
	public AudioClip fireAudio;
	
	void Start(){
		if(muzzleFlash){
			muzzleFlash.renderer.enabled = false;	
		}
		audio.playOnAwake = false;

		if(firstPersonWeapon){
			if(firstPersonWeapon.GunType == WeaponScript.gunType.MACHINE_GUN){
				if(firstPersonWeapon.machineGun.bullet){
					bullet = firstPersonWeapon.machineGun.bullet.gameObject;
				}
				if(firstPersonWeapon.machineGun.fireSound){
					fireAudio = firstPersonWeapon.machineGun.fireSound;
				}
			}
			if(firstPersonWeapon.GunType == WeaponScript.gunType.SHOTGUN){
				if(firstPersonWeapon.ShotGun.bullet){
					bullet = firstPersonWeapon.ShotGun.bullet.gameObject;
				}
				if(firstPersonWeapon.ShotGun.fireSound){
					fireAudio = firstPersonWeapon.ShotGun.fireSound;
				}
			}
			if(firstPersonWeapon.GunType == WeaponScript.gunType.GRENADE_LAUNCHER){
				if(firstPersonWeapon.grenadeLauncher.projectile){
					projectile = firstPersonWeapon.grenadeLauncher.projectile;
				}
				if(firstPersonWeapon.grenadeLauncher.fireSound){
					fireAudio = firstPersonWeapon.grenadeLauncher.fireSound;
				}
			}
			if(firstPersonWeapon.GunType == WeaponScript.gunType.KNIFE){
				if(firstPersonWeapon.knife.fireSound){
					fireAudio = firstPersonWeapon.knife.fireSound;
				}
			}
		}
	}

	//We have 4 possible types of weapons (MACHINE_GUN, GRENADE_LAUNCHER, SHOTGUN, KNIFE)
	//So we will have 4 functions to receive message from each of those types
	
	//MACHINE GUN type weapons sync ************************************************
	void syncMachineGun(float errorAngle){
		this.StopAllCoroutines();
		StartCoroutine(machineGunShot(errorAngle));	
	}
	
	IEnumerator machineGunShot(float errorAngle){
		Quaternion oldRotation = firePoint.rotation;
		firePoint.rotation = Quaternion.Euler(Random.insideUnitSphere * errorAngle) * firePoint.rotation;
		GameObject instantiatedBullet = Instantiate (bullet, firePoint.position, firePoint.rotation) as GameObject;
		instantiatedBullet.GetComponent<Bullet>().doDamage = false; //This is remotebullet, enable damage
		firePoint.rotation = oldRotation;
		//Play fire sound 
		audio.clip = fireAudio;
		audio.Play();
		//Muzzle flash
		if(muzzleFlash){
			muzzleFlash.renderer.enabled = true;
			yield return new WaitForSeconds(0.04f);
			muzzleFlash.renderer.enabled = false;
		}
	}
	//************************************************************************
	
	//SHOTGUN type weapons sync **************************************************
	void syncShotGun(float fractions){
		this.StopAllCoroutines();
		StartCoroutine(shotGunShot(fractions));	
	}
	
	IEnumerator shotGunShot(float fractions){
		for (int i = 0;i < (int)fractions; i++) {
			Quaternion oldRotation = firePoint.rotation;
			firePoint.rotation = Quaternion.Euler(Random.insideUnitSphere * 3) * firePoint.rotation;
			GameObject instantiatedBullet = Instantiate (bullet, firePoint.position, firePoint.rotation) as GameObject;
			instantiatedBullet.GetComponent<Bullet>().doDamage = false; //This is remotebullet, enable damage

			firePoint.rotation = oldRotation;
		}
		//Play fire sound 
		audio.clip = fireAudio;
		audio.Play();
		//Muzzle flash
		if(muzzleFlash){
			muzzleFlash.renderer.enabled = true;
			yield return new WaitForSeconds(0.04f);
			muzzleFlash.renderer.enabled = false;
		}
	}
	//************************************************************************
	
	//GRENADE LAUNCHER type weapon sync ******************************************
	void syncGrenadeLauncher(float initialSpeed){
		grenadeLauncherShot(initialSpeed);	
	}
	
	void grenadeLauncherShot(float initialSpeed){
		// create a new projectile, use the same position and rotation as the Launcher.
		Rigidbody instantiatedProjectile = Instantiate(projectile, firePoint.position, firePoint.rotation) as Rigidbody;
		instantiatedProjectile.GetComponent<Projectile>().isRemote = true; //This projectile is remote so disable damage
		// Give it an initial forward velocity. The direction is along the z-axis of the missile launcher's transform.
		//instantiatedProjectile.velocity = transform.TransformDirection(new Vector3 (initialSpeed, 0, 0));
		instantiatedProjectile.AddForce(instantiatedProjectile.transform.forward * initialSpeed * 50);
		// Ignore collisions between the missile and the character controller
		Physics.IgnoreCollision(instantiatedProjectile.collider, transform.root.GetComponent<Collider>());
		foreach(Collider c in transform.root.GetComponentsInChildren<Collider>()){
			Physics.IgnoreCollision(instantiatedProjectile.collider, c);
		}
		
		audio.clip = fireAudio;
		audio.Play();
	}
	//************************************************************************
	
	//MELEE type sync ***********************************************************
	void syncKnife(float temp){
		knifeOneHit();	
	}
	
	void knifeOneHit(){
		audio.clip = fireAudio;
		audio.Play();
	}
	//************************************************************************
}
