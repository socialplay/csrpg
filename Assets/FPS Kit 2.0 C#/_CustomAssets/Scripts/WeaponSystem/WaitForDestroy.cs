﻿using UnityEngine;
using System.Collections;

public class WaitForDestroy : MonoBehaviour {

	public float lifeTime = 2.0f; //Wait this time before destroying object
	
	void Awake (){
		Destroy (gameObject, lifeTime);
	}
}
