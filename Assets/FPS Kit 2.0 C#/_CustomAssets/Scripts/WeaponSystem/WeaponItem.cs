﻿//NSdesignGames @ 2012 - 2014
//FPS Kit | Version 2.0

using UnityEngine;
using System.Collections;

public class WeaponItem : MonoBehaviour {

	//Assign this script to weapon item so WeaponPickUp.cs assigned to player knows that we can pick this item

	void OnTriggerEnter(Collider other){
		//Detect if we on pickable weapon
		if(other.transform.root.tag == "Player"){
			WeaponPickUp wpu = other.transform.root.GetComponent<WeaponPickUp>();
			wpu.AssignWeapon(gameObject);
		}
	}
	
	void OnTriggerExit(Collider other){
		if(other.transform.root.tag == "Player"){
			WeaponPickUp wpu = other.transform.root.GetComponent<WeaponPickUp>();
			if(wpu.WeaponToPick == gameObject){
				wpu.WeaponToPick = null;
				//wpu.newWeapon = null;
			}
		}
	}
}
