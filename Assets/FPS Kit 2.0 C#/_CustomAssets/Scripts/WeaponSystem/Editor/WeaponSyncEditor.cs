﻿//NSdesignGames @ 2012 - 2014
//FPS Kit | Version 2.0

using UnityEditor;

[CustomEditor (typeof(WeaponSync))]

public class WeaponSyncEditor : Editor {

	string weaponType = "NULL";

	public override void OnInspectorGUI() {
		WeaponSync script = (WeaponSync) target;
		bool allowSceneObjects = !EditorUtility.IsPersistent(script);

		EditorGUILayout.LabelField("", "Assigned weapon type - " + weaponType, "button");
		//Depending on assigned weapon type show different variables
		script.firstPersonWeapon = EditorGUILayout.ObjectField(" First Person Weapon:", script.firstPersonWeapon, typeof(WeaponScript), allowSceneObjects) as WeaponScript;
		if(script.firstPersonWeapon != null){
			if(script.firstPersonWeapon.GunType != WeaponScript.gunType.KNIFE){
				script.firePoint = EditorGUILayout.ObjectField(" Fire Point:", script.firePoint, typeof(UnityEngine.Transform), allowSceneObjects) as UnityEngine.Transform;
			}
			/*if(script.firstPersonWeapon.GunType == WeaponScript.gunType.MACHINE_GUN || script.firstPersonWeapon.GunType == WeaponScript.gunType.SHOTGUN){
				script.bullet = EditorGUILayout.ObjectField(" Bullet Prefab:", script.bullet, typeof(UnityEngine.GameObject), allowSceneObjects) as UnityEngine.GameObject;
			}*/
		   	if(script.firstPersonWeapon.GunType == WeaponScript.gunType.MACHINE_GUN){
				script.muzzleFlash = EditorGUILayout.ObjectField(" Muzzle Flash:", script.muzzleFlash, typeof(UnityEngine.Renderer), allowSceneObjects) as UnityEngine.Renderer;
			}
			/*if(script.firstPersonWeapon.GunType == WeaponScript.gunType.GRENADE_LAUNCHER){
				script.projectile = EditorGUILayout.ObjectField(" Projectile:", script.projectile, typeof(UnityEngine.Rigidbody), allowSceneObjects) as UnityEngine.Rigidbody;
			}
			script.fireAudio = EditorGUILayout.ObjectField(" Fire Audio:", script.fireAudio, typeof(UnityEngine.AudioClip), allowSceneObjects) as UnityEngine.AudioClip;*/

			weaponType = script.firstPersonWeapon.GunType.ToString();

			
			if(script.gameObject.name != script.firstPersonWeapon.weaponName){
				script.gameObject.name = script.firstPersonWeapon.weaponName;
			}
		}else{
			weaponType = "NONE";
		}
	}
}
