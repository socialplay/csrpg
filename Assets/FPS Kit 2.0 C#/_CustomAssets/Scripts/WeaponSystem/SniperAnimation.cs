﻿//NSdesignGames @ 2012 - 2014
//FPS Kit | Version 2.0

using UnityEngine;
using System.Collections;

//[AddComponentMenu ("FPS system/Weapon System/SniperAnimation")]

public class SniperAnimation : MonoBehaviour {
	//This script is used to control main weapon animations
	//Should be attached to the object that contain weapon/hand animation
	//NOTE: this is Modifired version of WeaponAnimation.js and its adapted to work with per bullet reload animation

	//This is animation names that should correspond to attached animation names
	public string Idle = "Idle";
	public string ReloadBegin = "Reload_1_3";
	public string ReloadMiddle = "Reload_2_3";
	public string ReloadEnd = "Reload_3_3";
	public string Shoot = "Fire";
	public string TakeIn = "TakeIn";
	public string TakeOut = "TakeOut";
	public float FireAnimationSpeed = 1;
	public float TakeInOutSpeed = 1;
	public float ReloadMiddleRepeat = 4;
	
	string PlayThis;
	
	FPScontroller motor;
	GameObject player;
	
	void Awake () {
		animation.Play(Idle);
		animation[Idle].wrapMode = WrapMode.Once;
		animation[ReloadBegin].wrapMode = WrapMode.Once;
		animation[ReloadMiddle].wrapMode = WrapMode.Once;
		animation[ReloadEnd].wrapMode = WrapMode.Once;
		animation[Shoot].wrapMode = WrapMode.Once;
		animation[TakeIn].wrapMode = WrapMode.Once;
		animation[TakeOut].wrapMode = WrapMode.Once;
	}
	
	public void Fire(){
		animation.Rewind(Shoot);
		animation[Shoot].speed = FireAnimationSpeed;
		animation.Play(Shoot);
	}
	
	public void Reloading(float reloadTime) {
		float totalLength = animation[ReloadBegin].clip.length + animation[ReloadMiddle].clip.length*ReloadMiddleRepeat + animation[ReloadEnd].clip.length;
		
		AnimationState newReload1 = animation.CrossFadeQueued(ReloadBegin);
		newReload1.speed = (totalLength/reloadTime)/2;
		//4 is number of bullets to reload
		for(int i = 0; i < ReloadMiddleRepeat; i++){
			AnimationState newReload2 = animation.CrossFadeQueued(ReloadMiddle);
			newReload2.speed = (totalLength/reloadTime)/1.4f;
		}
		AnimationState newReload3 = animation.CrossFadeQueued(ReloadEnd);
		newReload3.speed = (totalLength/reloadTime)/2;
	}
	
	public void takeIn(){
		animation.Rewind(TakeIn);
		animation[TakeIn].speed = TakeInOutSpeed;
		animation[TakeIn].time = 0;
		animation.Play(TakeIn);
	}
	
	public void takeOut(){
		animation.Rewind(TakeOut);
		animation[TakeOut].speed = TakeInOutSpeed;
		animation[TakeOut].time = 0;
		animation.Play(TakeOut);
	}
}
