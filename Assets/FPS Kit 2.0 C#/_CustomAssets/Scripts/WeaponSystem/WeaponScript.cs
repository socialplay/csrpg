﻿//NSdesignGames @ 2012 - 2014
//FPS Kit | Version 2.0

using UnityEngine;
using System.Collections;


[RequireComponent (typeof(AudioSource))]
//[AddComponentMenu ("FPS system/Weapon System/WeaponScript")]

public class WeaponScript : MonoBehaviour {
	//Booleans
	[HideInInspector]
	public bool aimed;
	[HideInInspector]
	public bool fire;
	[HideInInspector]
	public bool canAim;
	[HideInInspector]
	public bool  isReload;
	[HideInInspector]
	public bool  noBullets;
	[HideInInspector]
	public bool Recoil;
	[HideInInspector]
	public bool  canFire;
	[HideInInspector]
	public bool  singleFire;
	
	FPScontroller motor;
	GameObject player;
	CharacterController controller;
	FPSMouseLook mouseLook;
	
	//make walk sway amount smaller when player is aim 
	//or aim mode walk sway control
	WalkSway walkSway;
	float defaultBobbingAmount;
	GameObject managerObject;

	//[System.Serializable]
	public enum gunType {MACHINE_GUN, GRENADE_LAUNCHER, SHOTGUN, KNIFE}
	public gunType GunType = gunType.MACHINE_GUN;
	public bool FlashLight;
	
	//Use classes to make a group of variables
	
	public string weaponName = "";
	
	//Aim variables
	[System.Serializable]
	public class AimVariables {
		public Vector3 aimPosition = Vector3.zero;
		public float smoothTime = 5;
		public int toFov = 45;
		public float aimBobbingAmount;
		public bool playAnimation;
	}
	public AimVariables Aim;

	float defaultFov;
	Vector3 defaultPosition;
	float currentFov;
	Vector3 currentPosition;
	
	public Transform firePoint; //Fire point is a point from where bullets/projectiles are spawned
	
	//Shotgun variables
	[System.Serializable]
	public class shotGun{
		public Transform bullet;
		public int fractions = 5;
		public float errorAngle = 3;
		public float fireRate = 1;
		public float reloadTime = 2;
		public AudioClip fireSound;
		public AudioClip reloadSound;
		public int bulletsPerClip = 40;
		public int bulletsLeft;
		public int clips = 15;
		public ParticleEmitter smoke;
	}
	public shotGun ShotGun;
	
	//Grenade launcher variables
	[System.Serializable]
	public class GrenadeLauncher{
		public Rigidbody projectile;
		public AudioClip fireSound;
		public AudioClip reloadSound;
		public int initialSpeed = 20;
		//Delay shot (ex. greande throw)
		public float shotDelay = 0;
		public float waitBeforeReload = 0.5f;
		public float reloadTime = 0.5f;
		public int ammoCount = 20;
	}
	public GrenadeLauncher grenadeLauncher;
	
	float lastShot = -10.0f;
	
	//Machine gun variables
	[System.Serializable]
	public class MachineGun{
		public Transform bullet;
		public GameObject muzzleFlash;
		public AudioClip fireSound;
		public AudioClip reloadSound;
		public Light pointLight;
		public float fireRate = 0.05f;
		public int bulletsPerClip = 40;
		public int clips = 15;
		public int bulletsLeft;
		public float reloadTime = 1.0f;
		public float NoAimErrorAngle = 3.0f;
		public float AimErrorAngle = 0.0f;
	}
	public MachineGun machineGun;
	[HideInInspector]
	public float errorAngle;
	float nextFireTime = 0.0f;

	//Knife variables
	[System.Serializable]
	public class Knife{
		public Transform bullet;
		public AudioClip fireSound;
		public float fireRate = 0.5f;
		public float delayTime = 0;
	}
	public Knife knife;

	//Rotation realisn variables
	[System.Serializable]
	public class RotationReal{
		public int RotationAmplitude = 2;
		public int smooth = 7;
	}
	public RotationReal RotRealism;

	float currentAnglex;
	float currentAngley;
	
	//Smooth move variables
	[System.Serializable]
	public class SmoothMov {
		public float maxAmount = 0.5f;
		public int Smooth = 3;
	}
	public SmoothMov SmoothMovement;
	Vector3 DefaultPos;

	//Camera Recoil effect NOTE : Be sure that is your player camera is tagged as "MainCamera"
	[System.Serializable]
	public class cameraRecoil{
		public float recoilPower = 0.5f;
		public float shakeAmount = 6;
		public int smooth = 3;
	}
	public cameraRecoil CameraRecoil;

	Quaternion camDefaultRotation;
	Quaternion camPos;

	WeaponAnimation weaponAnimation; //If there is weapon animation controller in child, assign it
	SniperAnimation sniperAnimation; //Check for sniper type animation
	Transform mainCameraT;
	WeaponSync_Catcher wsc; //Check if WeaponSync_Catcher.cs is assigned to same object (needed for multiplayer only)

	//PlayerNetworkController pnc; //Use this only in multiplayer to check if players is ours or remote
	
	//Awake function is always called before Start function
	void Awake(){
		//Find nesessary objects and scripts (Player, CharacterMotor script, and weapon projectile spawn points etc.)
		player = transform.root.gameObject;
		motor = player.GetComponent<FPScontroller>();
		controller = player.GetComponent<CharacterController>();
		FPSMouseLook[] mouseLooks = transform.root.GetComponentsInChildren<FPSMouseLook>();
		foreach(FPSMouseLook fml in mouseLooks){
			if(fml.axes == FPSMouseLook.RotationAxes.MouseY){
				mouseLook = fml;
			}
		}

		if(gameObject.GetComponentInChildren<WeaponAnimation>() != null){
			weaponAnimation = gameObject.GetComponentInChildren<WeaponAnimation>();
		}else{
			if(gameObject.GetComponentInChildren<SniperAnimation>() != null){
				sniperAnimation = gameObject.GetComponentInChildren<SniperAnimation>();
			}
		}

		/*if(transform.root.GetComponent<PlayerNetworkController>() != null){
			pnc = transform.root.GetComponent<PlayerNetworkController>();
		}*/
		if(gameObject.GetComponent<WeaponSync_Catcher>() != null){
			wsc = gameObject.GetComponent<WeaponSync_Catcher>();
		}
	}
	
	void Start (){
		//Aim mode walk sway control
		walkSway = transform.root.GetComponentInChildren<WalkSway>();
		defaultBobbingAmount = walkSway.bobbingAmount;
		
		//Camera recoil
		camDefaultRotation = Camera.main.transform.localRotation;
		
		//Aim setup
		defaultFov = Camera.main.fieldOfView;
		defaultPosition = transform.localPosition;
		
		//Call machineGun awake
		if(GunType == gunType.MACHINE_GUN){
			machineGunAwake();
		}
		//Call grenadelauncher awake
		if(GunType == gunType.GRENADE_LAUNCHER){
			grenadeLauncherAwake();
		}
		
		//Call shotgun Awake
		if(GunType == gunType.SHOTGUN){
			shotGunAwake();
		}
		
		//Call knife awake
		if(GunType == gunType.KNIFE){
			knifeAwake();
		}

		mainCameraT = Camera.main.transform;
	}
	
	void Update (){
		if(Time.timeScale < 0.01f)
			return;
		Aiming();
		RotationRealism();
		SmoothMove();
		//PickUpUpdate();
		//input();
		if(Recoil){
			cameraRecoilDo();
		}
		//Call machine gun fixed update function
		if(GunType == gunType.MACHINE_GUN){
			machineGunFixedUpdate();
		}
		//Call grenade launcher fixed update function
		if(GunType == gunType.GRENADE_LAUNCHER){
			grenadeLauncherFixedUpdate();
		}
		
		//Call shotgun fixed update function
		if(GunType == gunType.SHOTGUN){
			shotGunFixedUpdate ();
		}
		if(motor.Running){
			aimed = false;
		}

		InputUpdate();
	}
	
	//////////////////////////////////////////////////INPUT//////////////////////////////////////////////////////////////////////
	void InputUpdate(){
		//if(Time.timeScale < 0.01f)
			//return;
		//1.Aim input 
		if(Input.GetMouseButtonDown(1) && canAim && !motor.Running){
			aimed = !aimed;
		}
		//2.Fire input 
		//Automatic fire input
		if(Input.GetMouseButton(0) && canFire && !singleFire){
			fire = true;
		}else{
			fire = false;
		}
		//Single fire iputs
		if(GunType == gunType.MACHINE_GUN){
			//Single fire input for machine gun mode
			if(Input.GetMouseButtonDown(0) && canFire && !isReload && singleFire){
				machineGunFire();
			}else{
				machineGunStopFire ();
			}
		}
		
		if(GunType == gunType.GRENADE_LAUNCHER){
			//Single fire input for grenade launcher mode
			if(Input.GetMouseButtonDown(0) && canFire && !isReload && singleFire){
				grenadeLauncherFIre();
			}
		}
		
		if(GunType == gunType.SHOTGUN){
			//Single fire input for shotgun mode
			if(Input.GetMouseButtonDown(0) && canFire && !isReload && singleFire){
				shotGunFire ();
			}	
		}
		
		if(GunType == gunType.KNIFE){
			if(Input.GetMouseButtonDown(0) && canFire && !isReload && singleFire){
				StartCoroutine(knifeOneShot());
			}
		}
		
		//3.Reload input
        if (Input.GetKeyDown(KeyCode.R) && !isReload)
        {
            if (GunType == gunType.MACHINE_GUN && machineGun.clips > 0 && machineGun.bulletsLeft != machineGun.bulletsPerClip)
            {
                StartCoroutine(machineGunReload());
            }

            if (GunType == gunType.SHOTGUN && ShotGun.clips > 0 && ShotGun.bulletsLeft != ShotGun.bulletsPerClip)
            {
                StartCoroutine(shotGunReload());
            }
        }
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	///////////////////////////////////////////////MACHINE GUN FUNCTIONS///////////////////////////////////////////////////////
	void machineGunAwake(){
		machineGun.bulletsLeft = machineGun.bulletsPerClip;
		//Deactivate muzzleFlash if we didnt
		if(machineGun.muzzleFlash){
			machineGun.muzzleFlash.SetActive(false);
		}
		canAim = true;
		canFire = true;
	}
	
	void machineGunFixedUpdate (){
		//Machine gun fire
		if(fire && !isReload){
			machineGunFire();
		}else{
			machineGunStopFire();
			if(machineGun.muzzleFlash){
				machineGun.muzzleFlash.SetActive(false);
			}
		}
		
		if(isReload){
			//motor.canRun = false;
			canAim = false;
		}
	}
	
	public void machineGunFire (){
		if (machineGun.bulletsLeft == 0)
			return;
		
		// If there is more than one bullet between the last and this frame
		// Reset the nextFireTime
		if (Time.time - machineGun.fireRate > nextFireTime){
			nextFireTime = Time.time - Time.deltaTime;
		}
		// Keep firing until we used up the fire time
		while( nextFireTime < Time.time && machineGun.bulletsLeft != 0){
			StartCoroutine(machineGunOneShot());
			nextFireTime += machineGun.fireRate;
		}
		//motor.canRun = false;
		
	}
	
	void machineGunStopFire (){
		motor.canRun = true;
	}

	IEnumerator machineGunOneShot () {
		Quaternion oldRotation = firePoint.rotation;
		firePoint.rotation =  Quaternion.Euler(Random.insideUnitSphere * errorAngle) * transform.rotation;
		Transform instantiatedProjectile;
		firePoint.LookAt(GetCenterOfScreen());
		instantiatedProjectile = Instantiate (machineGun.bullet, firePoint.position, firePoint.rotation) as Transform;
		instantiatedProjectile.GetComponent<Bullet>().doDamage = true; //This is local bullet, enable damage

		firePoint.rotation = oldRotation;
		lastShot = Time.time;
		machineGun.bulletsLeft--;
		//Play fire sound attached to a Weapon object
		audio.clip = machineGun.fireSound;
		audio.Play();
		//Do a light effect when shoot
		StartCoroutine(machineGunMuzzleFlash());
		//Send message to weapon animation script
		if(aimed){
			if(Aim.playAnimation){
				//BroadcastMessage ("Fire", SendMessageOptions.DontRequireReceiver);
				if(weaponAnimation){
					weaponAnimation.Fire();
				}
				if(sniperAnimation){
					sniperAnimation.Fire();
				}
			}
		}else{
			//BroadcastMessage ("Fire", SendMessageOptions.DontRequireReceiver);
			if(weaponAnimation){
				weaponAnimation.Fire();
			}
			if(sniperAnimation){
				sniperAnimation.Fire();
			}
		}
		if(Recoil){
			if(mouseLook){
				mouseLook.Recoil(CameraRecoil.recoilPower);
			}
			StartCoroutine(machineGunCameraRecoil());
		}

		//Needed for multiplayer part to sync shots
		if(wsc){
			wsc.Fire();
		}
		
		// Reload gun in reload Time
		if(machineGun.clips > 0)			
		if (machineGun.bulletsLeft == 0){
			noBullets = true;
			yield return new WaitForSeconds(1);
			if(!isReload){
				StartCoroutine(machineGunReload());
			}
		}
	}
	
	IEnumerator machineGunMuzzleFlash(){
		if(machineGun.muzzleFlash){
			machineGun.muzzleFlash.transform.localRotation = Quaternion.AngleAxis(Random.Range(0, 359), Vector3.left);
			machineGun.muzzleFlash.SetActive(true);
		}
		if(machineGun.pointLight){
			machineGun.pointLight.enabled = true;
		}
		yield return new WaitForSeconds(0.04f);
		if(machineGun.muzzleFlash){
			machineGun.muzzleFlash.SetActive(false);
		}
		if(machineGun.pointLight){
			machineGun.pointLight.enabled = false;
		}
	}
	
	IEnumerator machineGunReload() {
		isReload = true;
		aimed = false;
		canAim = false;
		//BroadcastMessage ("Reloading", machineGun.reloadTime, SendMessageOptions.DontRequireReceiver);
		if(weaponAnimation){
			weaponAnimation.Reloading(machineGun.reloadTime);
		}
		if(sniperAnimation){
			sniperAnimation.Reloading(machineGun.reloadTime);
		}
		//Play reload sound
		audio.clip = machineGun.reloadSound;
		audio.Play();
		// Wait for reload time first - then add more bullets!
		yield return new WaitForSeconds(machineGun.reloadTime);
		
		// We have a clip left reload
		if (machineGun.clips > 0)
		{
			var difference = machineGun.bulletsPerClip-machineGun.bulletsLeft;
			if(machineGun.clips > difference ){
				machineGun.clips = machineGun.clips - difference;
				machineGun.bulletsLeft = machineGun.bulletsLeft + difference;
			}else{
				machineGun.bulletsLeft = machineGun.bulletsLeft + machineGun.clips;
				machineGun.clips = 0;
			}
			noBullets = false;
			isReload = false;
			canAim = true;
			motor.canRun = true;
		}
	}
	
	IEnumerator machineGunCameraRecoil(){
		camPos = Quaternion.Euler (Random.Range(0, -CameraRecoil.shakeAmount), Random.Range(-CameraRecoil.shakeAmount, CameraRecoil.shakeAmount), 0);
		yield return new WaitForSeconds(0.05f);
		camPos = camDefaultRotation;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/////////////////////////////////////////////////////////GRENADE LAUNCHER FUNCTIONS////////////////////////////////////////////
	void grenadeLauncherAwake(){
		canAim = true;
		canFire = true;
	}
	
	void grenadeLauncherFixedUpdate (){
		//GrenadeLauncher fire
		if(fire && !isReload){
			grenadeLauncherFIre();
			//motor.canRun = false;
		}else{
			motor.canRun = true;
		}
	}
	
	public void grenadeLauncherFIre (){
		// Did the time exceed the reload time?
		if (grenadeLauncher.ammoCount == 0 || !canFire)
			return;
		
		// If there is more than one bullet between the last and this frame
		// Reset the nextFireTime
		if (Time.time - grenadeLauncher.reloadTime > nextFireTime){
			nextFireTime = Time.time - Time.deltaTime;
		}
		// Keep firing until we used up the fire time
		while( nextFireTime < Time.time && grenadeLauncher.ammoCount > 0){
			StartCoroutine(grenadeLauncherOneShot());
			nextFireTime += grenadeLauncher.reloadTime;
		}
		//motor.canRun = false;
		
	}
	
	public IEnumerator grenadeLauncherOneShot(){
		if(grenadeLauncher.shotDelay > 0){
			//Send message to weapon animation script
			if(aimed && Aim.playAnimation){
				//BroadcastMessage ("Fire", SendMessageOptions.DontRequireReceiver);
				if(weaponAnimation){
					weaponAnimation.Fire();
				}
			}
			if(!aimed){
				//BroadcastMessage ("Fire", SendMessageOptions.DontRequireReceiver);
				if(weaponAnimation){
					weaponAnimation.Fire();
				}
			}

			if(Recoil){
				if(mouseLook){
					mouseLook.Recoil(CameraRecoil.recoilPower);
				}
				StartCoroutine(grenadeLauncherCameraRecoil());
			}
			StartCoroutine(grenadeLauncherReload());
			yield return new WaitForSeconds(grenadeLauncher.shotDelay);
		}

		//Needed for multiplayer part to sync shots
		if(wsc){
			wsc.Fire();
		}

		// create a new projectile, use the same position and rotation as the Launcher.
		Rigidbody instantiatedProjectile = Instantiate(grenadeLauncher.projectile, firePoint.position, firePoint.rotation) as Rigidbody; 
		instantiatedProjectile.GetComponent<Projectile>().isRemote = false; //This projectile is local so enable damage
		// Give it an initial forward velocity. The direction is along the z-axis of the missile launcher's transform.
		instantiatedProjectile.velocity = transform.TransformDirection(new Vector3 (0, 0, grenadeLauncher.initialSpeed));
		
		// Ignore collisions between the missile and the character controller
		foreach(Collider c in transform.root.GetComponentsInChildren<Collider>()){
			Physics.IgnoreCollision(instantiatedProjectile.collider, c);
		}
		
		lastShot = Time.time;
		grenadeLauncher.ammoCount--;
		audio.clip = grenadeLauncher.fireSound;
		audio.Play();
		if(grenadeLauncher.shotDelay == 0){
			//Send message to weapon animation script
			if(aimed && Aim.playAnimation){
				//BroadcastMessage ("Fire", SendMessageOptions.DontRequireReceiver);
				if(weaponAnimation){
					weaponAnimation.Fire();
				}
			}
			if(!aimed){
				//BroadcastMessage ("Fire", SendMessageOptions.DontRequireReceiver);
				if(weaponAnimation){
					weaponAnimation.Fire();
				}
			}
			if(Recoil){
				if(mouseLook){
					mouseLook.Recoil(CameraRecoil.recoilPower);
				}
				StartCoroutine(grenadeLauncherCameraRecoil());
			}
			if(grenadeLauncher.ammoCount > 0){
				StartCoroutine(grenadeLauncherReload());
			}
		}
	}
	
	IEnumerator grenadeLauncherReload(){
		isReload = true;
		yield return new WaitForSeconds(grenadeLauncher.waitBeforeReload);
		aimed = false;
		//BroadcastMessage ("Reloading", grenadeLauncher.reloadTime, SendMessageOptions.DontRequireReceiver);
		if(weaponAnimation){
			weaponAnimation.Reloading(grenadeLauncher.reloadTime);
		}
		//Play reload sound
		audio.clip = grenadeLauncher.reloadSound;
		audio.Play();
		yield return new WaitForSeconds(grenadeLauncher.reloadTime);
		isReload = false;
	}
	
	IEnumerator grenadeLauncherCameraRecoil(){
		camPos = Quaternion.Euler (Random.Range(-CameraRecoil.shakeAmount * 1.5f, -CameraRecoil.shakeAmount), Random.Range(CameraRecoil.shakeAmount/3, CameraRecoil.shakeAmount/2), 0);
		yield return new WaitForSeconds(0.1f);
		camPos = camDefaultRotation;
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//////////////////////////////////////////////////SHOTGUN FUNCTIONS/////////////////////////////////////////////////////////
	void shotGunAwake(){
		ShotGun.bulletsLeft = ShotGun.bulletsPerClip;
		if(ShotGun.smoke){
			ShotGun.smoke.emit = false;
		}
		canAim = true;
		canFire = true;
	}
	
	void shotGunFixedUpdate(){
		if(fire && !isReload){
			shotGunFire();
		}else{
			shotGunStopFire();
		}	
		
		if(isReload){
			//motor.canRun = false;
			canAim = false;
		}
	}
	
	public void shotGunFire (){
		if (ShotGun.bulletsLeft == 0)
			return;
		
		// If there is more than one bullet between the last and this frame
		// Reset the nextFireTime
		if (Time.time - ShotGun.fireRate > nextFireTime){
			nextFireTime = Time.time - Time.deltaTime;
		}
		
		// Keep firing until we used up the fire time
		while( nextFireTime < Time.time && ShotGun.bulletsLeft != 0){
			StartCoroutine(shotGunOneShot());
			nextFireTime += ShotGun.fireRate;
		}
		//motor.canRun = false;
	}
	
	void shotGunStopFire (){
		motor.canRun = true;
	}
	
	IEnumerator shotGunOneShot() {
		//Before fire, move aim point to right position
		firePoint.LookAt(GetCenterOfScreen());
		Quaternion oldRotation = firePoint.rotation;
		for (int i = 0; i < ShotGun.fractions; i++) {
			firePoint.rotation =  Quaternion.Euler(Random.insideUnitSphere * ShotGun.errorAngle) * transform.rotation;
			GameObject instantiatedBullet = Instantiate (ShotGun.bullet, firePoint.position, firePoint.rotation) as GameObject;
			if(instantiatedBullet){
				instantiatedBullet.GetComponent<Bullet>().doDamage = true; //This is local bullet, enable damage
			}
		}
		firePoint.rotation = oldRotation;
		lastShot = Time.time;	
		//Play fire sound attached to a Weapon object
		audio.clip = ShotGun.fireSound;
		audio.Play();
		ShotGun.bulletsLeft--;
		//Send message to weapon animation script
		if(aimed && Aim.playAnimation){
			//BroadcastMessage ("Fire", SendMessageOptions.DontRequireReceiver);
			if(weaponAnimation){
				weaponAnimation.Fire();
			}
		}
		if(!aimed){
			//BroadcastMessage ("Fire", SendMessageOptions.DontRequireReceiver);
			if(weaponAnimation){
				weaponAnimation.Fire();
			}
		}

		//Needed for multiplayer part to sync shots
		if(wsc){
			wsc.Fire();
		}

		StartCoroutine(shotGunSmokeEffect());
		if(Recoil){
			StartCoroutine(shotGunCameraRecoil());
			if(mouseLook){
				mouseLook.Recoil(CameraRecoil.recoilPower);
			}
		}
		
		// Reload gun in reload Time	
		if(ShotGun.clips > 0)	
		if (ShotGun.bulletsLeft == 0){
			noBullets = true;
			yield return new WaitForSeconds(1);
			if(!isReload){
				StartCoroutine(shotGunReload());
			}
		}
	}
	
	IEnumerator shotGunReload() {
		isReload = true;
		aimed = false;
		//BroadcastMessage ("Reloading", ShotGun.reloadTime, SendMessageOptions.DontRequireReceiver);
		if(weaponAnimation){
			weaponAnimation.Reloading(ShotGun.reloadTime);
		}
		//Play reload sound
		audio.clip = ShotGun.reloadSound;
		audio.Play();
		// Wait for reload time first - then add more bullets!
		yield return new WaitForSeconds(ShotGun.reloadTime);
		
		// We have a clip left reload
		if (ShotGun.clips > 0){
			var difference = ShotGun.bulletsPerClip-ShotGun.bulletsLeft;
			if(ShotGun.clips > difference ){
				ShotGun.clips = ShotGun.clips - difference;
				ShotGun.bulletsLeft = ShotGun.bulletsLeft + difference;
			}else{
				ShotGun.bulletsLeft = ShotGun.bulletsLeft + ShotGun.clips;
				ShotGun.clips = 0;
			}
			noBullets = false;
			isReload = false;
			canAim = true;
			motor.canRun = true;
		}
	}
	
	IEnumerator shotGunSmokeEffect(){
		if(!ShotGun.smoke)
			yield return null;
		ShotGun.smoke.emit = true;
		yield return new WaitForSeconds(0.3f);
		ShotGun.smoke.emit = false;	
	}
	
	IEnumerator shotGunCameraRecoil(){
		camPos = Quaternion.Euler (Random.Range(-CameraRecoil.shakeAmount * 1.5f, -CameraRecoil.shakeAmount), Random.Range(CameraRecoil.shakeAmount/3, CameraRecoil.shakeAmount/2), 0);
		yield return new WaitForSeconds(0.1f);
		camPos = camDefaultRotation;
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	
	/////////////////////////////////////////////////KNIFE FUNCTIONS///////////////////////////////////
	void knifeAwake(){
		canAim = false;
		canFire = true;
	}
	
	IEnumerator knifeOneShot() {
		if (Time.time > knife.fireRate + lastShot){
			//Before fire, move aim point to right position
			firePoint.LookAt(GetCenterOfScreen());
			//Play fire sound attached to a Weapon object
			audio.clip = knife.fireSound;
			audio.Play();
			//BroadcastMessage ("Fire", SendMessageOptions.DontRequireReceiver);
			if(weaponAnimation){
				weaponAnimation.Fire();
			}

			//Needed for multiplayer part to sync shots
			if(wsc){
				wsc.Fire();
			}
			
			yield return new WaitForSeconds(knife.delayTime);
			
			GameObject instantiatedProjectile = Instantiate (knife.bullet, firePoint.position, firePoint.rotation) as GameObject;
			if(instantiatedProjectile != null){
				instantiatedProjectile.GetComponent<Bullet>().doDamage = true; //This is local bullet, enable damage
			}
			lastShot = Time.time;	
		}
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////
	
	////////////////////////////////////////////////AIM FUNCTIONS////////////////////////////////////
	void Aiming(){
		//Change camera FOV and weapon transform to aim values
		if(aimed && !motor.Running){
			currentPosition = Aim.aimPosition;
			currentFov = Aim.toFov;
			errorAngle = machineGun.AimErrorAngle;
			walkSway.bobbingAmount = Aim.aimBobbingAmount;
			//mouseLook.sensitivityX = mouseLook.defaultSensitivityX/1.1;
			//mouseLook.sensitivityY = mouseLook.defaultSensitivityY/1.1;
		}else{
			currentPosition = defaultPosition;
			currentFov = defaultFov;
			errorAngle = machineGun.NoAimErrorAngle;
			walkSway.bobbingAmount = defaultBobbingAmount;
			//mouseLook.sensitivityX = mouseLook.defaultSensitivityX;
			//mouseLook.sensitivityY = mouseLook.defaultSensitivityY;
		}
		//Change weapon position and camera FOV when player aim or no aim
		transform.localPosition = Vector3.Lerp(transform.localPosition, currentPosition, Time.deltaTime/Aim.smoothTime);
		Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, currentFov, Time.deltaTime/Aim.smoothTime);
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////
	void cameraRecoilDo(){
		Camera.main.transform.localRotation = Quaternion.Slerp(Camera.main.transform.localRotation, camPos, Time.deltaTime * CameraRecoil.smooth); 
	}
	
	void RotationRealism (){
		//ROTATION REALISM  
		float Xinput = Input.GetAxis("Mouse X");
		float Yinput = Input.GetAxis("Mouse Y");
		float currentAngley = 0;
		float currentAnglex = 0;
		
		if(Mathf.Abs(Xinput) > 0.1f){ 
			if(Xinput < 0.1f){ 
				//Left
				currentAngley = -RotRealism.RotationAmplitude * Mathf.Abs(Xinput);	
			} 
			else if(Xinput > 0.1f){ 
				//Right; 
				currentAngley = RotRealism.RotationAmplitude *  Mathf.Abs(Xinput);
			} 
		}else{ 
			//Center
			currentAngley = 0;
		} 
		
		if(Mathf.Abs(Yinput) > 0.1f){ 
			if(Yinput < 0.1f){ 
				//Down
				currentAnglex = RotRealism.RotationAmplitude * Mathf.Abs(Yinput);
			} 
			else if(Yinput > 0.1){ 
				//Up
				currentAnglex = -RotRealism.RotationAmplitude *  Mathf.Abs(Yinput);		
			} 
		}else{ 
			//Center
			currentAnglex = 0;
		} 
		
		Quaternion target = Quaternion.Euler (currentAnglex, currentAngley, 0);
		transform.localRotation = Quaternion.Slerp(transform.localRotation, target, Time.deltaTime * RotRealism.smooth);
	}
	
	void SmoothMove (){
		//var MoveOnX : float = -Input.GetAxis("Horizontal");
		float MoveOnY = controller.velocity.y;
		float m = 0;
		float MoveOnZ = -Input.GetAxis("Vertical");
		/*
	if (MoveOnX > SmoothMovement.maxAmount)
	MoveOnX = SmoothMovement.maxAmount;
	
	if (MoveOnX < -SmoothMovement.maxAmount)
	MoveOnX = -SmoothMovement.maxAmount;
	*/
		if(MoveOnY > SmoothMovement.maxAmount+1)
			m = -SmoothMovement.maxAmount;
		
		if(MoveOnY < -SmoothMovement.maxAmount-1)
			m = SmoothMovement.maxAmount;
		
		if (MoveOnZ> SmoothMovement.maxAmount)
			MoveOnZ = SmoothMovement.maxAmount;
		
		if (MoveOnZ < -SmoothMovement.maxAmount)
			MoveOnZ = -SmoothMovement.maxAmount;
		
		//var NewGunPos = new Vector3 (defaultPosition.x+ MoveOnX, defaultPosition.y + m, defaultPosition.z+ MoveOnZ);
		Vector3 NewGunPos = new Vector3 (transform.localPosition.x, transform.localPosition.y + m, transform.localPosition.z + MoveOnZ);
		transform.localPosition = Vector3.Lerp (transform.localPosition, NewGunPos, Time.deltaTime * SmoothMovement.Smooth);
	}
	
	internal void selectWeapon(){
		canFire = true;
		if(GunType != gunType.KNIFE){
			canAim = true;
		}
		aimed = false;
		//BroadcastMessage ("takeIn");
		if(weaponAnimation){
			weaponAnimation.takeIn();
		}
		if(sniperAnimation){
			sniperAnimation.takeIn();
		}
	}
	
	internal void deselectWeapon(){
		aimed = false;
		isReload = false;
		canFire = false;
		canAim = false;
		isReload = false;
		//BroadcastMessage ("takeOut");
		if(weaponAnimation){
			weaponAnimation.takeOut();
		}
		if(sniperAnimation){
			sniperAnimation.takeOut();
		}
	}

	Vector3 GetCenterOfScreen(){
		Ray ray = new Ray();
		ray.origin = mainCameraT.position;
		ray.direction =  mainCameraT.transform.forward;
		RaycastHit raycastHit = new RaycastHit();
		if( Physics.Raycast( ray, out raycastHit, 300) ){
			return raycastHit.point;
		}
		return mainCameraT.position + mainCameraT.forward * 100;
	}
}
