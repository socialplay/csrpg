﻿//NSdesignGames @ 2012 - 2014
//FPS Kit | Version 2.0

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ExplosionDamage : MonoBehaviour {
	public float explosionRadius = 5.0f;
	public float explosionPower = 10.0f;
	public float explosionDamage = 100.0f;
	public float explosionTimeout = 2.0f;
	GameObject player1;
	[HideInInspector]
	public bool isRemote; //If explosion is remote do not deal any damage
	
	IEnumerator Start () {
        List<GameObject> hitTargets = new List<GameObject>();
		Vector3 explosionPosition = transform.position;
		
		// Apply damage to close by objects first
		Collider[] colliders = Physics.OverlapSphere (explosionPosition, explosionRadius);
		foreach (Collider hit in colliders) {
			// Calculate distance from the explosion position to the closest point on the collider
			Vector3 closestPoint = hit.ClosestPointOnBounds(explosionPosition);
			float distance = Vector3.Distance(closestPoint, explosionPosition);
			
			// The hit points we apply fall decrease with distance from the explosion point
			float hitPoints = 1.0f - Mathf.Clamp01(distance / explosionRadius);
			hitPoints *= explosionDamage;
			
			// Tell the rigidbody or any other script attached to the hit object how much damage is to be applied!
			if(!isRemote && hit.GetComponent<HitBox>() != null){
                //hit.SendMessageUpwards("ApplyDamage", hitPoints, SendMessageOptions.DontRequireReceiver);
                if (!hitTargets.Contains(hit.GetComponent<HitBox>().damagableObject.gameObject))
                {
                    hitTargets.Add(hit.GetComponent<HitBox>().damagableObject.gameObject);
                    hit.GetComponent<HitBox>().ApplyDamage(0);
                }
			}
		}
		
		// Apply explosion forzces to all rigidbodies
		// This needs to be in two steps for ragdolls to work correctly.
		// (Enemies are first turned into ragdolls with ApplyDamage then we apply forces to all the spawned body parts)
		colliders = Physics.OverlapSphere (explosionPosition, explosionRadius);
		foreach (Collider hit in colliders) {
			if (hit.rigidbody){
				hit.rigidbody.AddExplosionForce(explosionPower, explosionPosition, explosionRadius, 3.0f);
			}
		}
		
		// stop emitting particles
		if (particleEmitter) {
			particleEmitter.emit = true;
			yield return new WaitForSeconds(0.5f);
			particleEmitter.emit = false;
		}
		
		Destroy (gameObject, explosionTimeout);
	}
}
