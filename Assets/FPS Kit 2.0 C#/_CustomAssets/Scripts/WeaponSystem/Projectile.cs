﻿//NSdesignGames @ 2012 - 2014
//FPS Kit | Version 2.0

using UnityEngine;
using System.Collections;


[RequireComponent (typeof(Rigidbody))]

public class Projectile : MonoBehaviour {
	// The reference to the explosion prefab
	public GameObject explosion;
	public float destroyDelay = 0;
	public float timeOut = 3.0f;
	public GameObject[] objectsToDestroy;

	ContactPoint contact;
	Quaternion rotation;
	[HideInInspector]
	public bool isRemote; //If projectile is remote do not deal any damage
	
	// Kill the rocket after a while automatically
	void Start () {
		if(destroyDelay > 0){
			Invoke("Kill", destroyDelay);
		}else{
			Invoke("Kill", timeOut);
		}
		
	}
	
	void FixedUpdate(){
		//Make projectile to look on direction of his trajectory
		transform.rotation = Quaternion.LookRotation(rigidbody.velocity);
	}
	
	void OnCollisionEnter (Collision collision) {
		// Instantiate explosion at the impact point and rotate the explosion
		// so that the y-axis faces along the surface normal
		contact = collision.contacts[0];
		rotation = Quaternion.FromToRotation(Vector3.up, contact.normal);

		if(destroyDelay > 0)
			return;
		// And kill our selves
		Kill ();    
	}
	
	void Kill (){
		GameObject explosionObject = Instantiate (explosion, transform.position, rotation) as GameObject;
		explosionObject.GetComponent<ExplosionDamage>().isRemote = isRemote;
		// Stop emitting particles in any children
		ParticleEmitter emitter = GetComponentInChildren<ParticleEmitter>();
		if (emitter){
			emitter.emit = false;
		}
		
		// Detach children - We do this to detach the trail rendererer which should be set up to auto destruct
		transform.DetachChildren();
		
		// Destroy the projectile
		Destroy(gameObject);
		//Destroy some other objects that assigned to array (if needed)
		if(objectsToDestroy.Length > 0){
			for(var i = 0; i < objectsToDestroy.Length; i++){
				Destroy(objectsToDestroy[i]);
			}
		}
	}
}
