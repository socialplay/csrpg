﻿//NSdesignGames @ 2012 - 2014
//FPS Kit | Version 2.0

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[AddComponentMenu ("FPS system/Weapon System/WeaponPickUp")]

public class WeaponPickUp : MonoBehaviour {

	//This script is used to pick up available weapons
	//Should be attached to GameObject with CharacterController component
	public GUISkin guiStyle;
	
	public enum PickUpStyle {Replace, Add}; //Should newly picked weapon replace current or add to weapon list
	public PickUpStyle pickUpStyle;
	//When player pick weapon he have alredy add (BulletsPerClip*pickAmmoMultiply) amount of bullets
	public int pickAmmoMultiply = 1;
	//If wepon have more then (BulletsPerClip*reserveAmmoLimit) bullets, dont auto pick it
	public int reserveAmmoLimit = 3;
	public float throwForce = 500;
	public Transform spawnObject;
	public WeaponManager weapManager;

	//Display actions properties
	int actionsToDisplay = 5;
	float messageTimeOut = 5;
	
	//Wepon models to throw
	public List<GameObject>weapons = new List<GameObject>();
	//All available weapons (assign all existing weapons)
	List<WeaponScript> playerWeapons = new List<WeaponScript>();

	//Player actions ti be displayed
	[HideInInspector]
	public List<string> actionsList = new List<string>();
	[HideInInspector]
	public List<float> timer = new List<float>();
	
	string weapName;
	GameObject weaponToThrow;
	[HideInInspector]
	public WeaponScript newWeapon;
	[HideInInspector]
	public GameObject WeaponToPick;
	//GUI Color fade
	float color;
	string text;
	
	//CharacterController controller;
	//float prevHeight;
	
	void Start () {
		//controller = gameObject.GetComponent<CharacterController>();
		//prevHeight = controller.height;

		playerWeapons.Clear();
		foreach(WeaponScript ws in weapManager.availableWeapons){
			playerWeapons.Add(ws);
		}
	}
	
	void Update () {
		if(WeaponToPick){
			//Press F key to pick up weapon
			if(Input.GetKeyDown(KeyCode.F)){
				if(weapManager.allWeapons.Contains(newWeapon))
					return;

				if(pickUpStyle == PickUpStyle.Replace){
					//Throw current weapon       
					GameObject clone = Instantiate(weaponToThrow, spawnObject.position, spawnObject.rotation) as GameObject;
					clone.name = weaponToThrow.name;
					//Add force when we throw weapon
					clone.rigidbody.AddForce (-spawnObject.transform.up * throwForce);
					StartCoroutine(weapManager.SwitchWeapons(weapManager.allWeapons[weapManager.index].gameObject, newWeapon.gameObject));
					weapManager.allWeapons[weapManager.index] = newWeapon;
					Destroy(WeaponToPick);
					//Register Action
					actionsList.Add(("Picked | " + newWeapon.weaponName).ToString());
					timer.Add(messageTimeOut);
				}
				if(pickUpStyle == PickUpStyle.Add){
					weapManager.allWeapons.Add(newWeapon);
					weapManager.index = weapManager.allWeapons.Count-1;
					StartCoroutine(weapManager.SwitchWeapons(weapManager.SelectedWeapon.gameObject, weapManager.allWeapons[weapManager.index].gameObject));
					Destroy(WeaponToPick);
					//Register Action
					actionsList.Add(("Picked | " + newWeapon.weaponName).ToString());
					timer.Add(messageTimeOut);
				}
			}
		}
		
		//Remove actions
		if(timer.Count > 0){
			for(int b = 0; b < timer.Count; b ++){
				timer[b] -= Time.deltaTime;
				if(timer[b] < 0){
					timer.Remove(timer[b]);
					actionsList.Remove(actionsList[b]);
				}
			}
			
			if(timer.Count > actionsToDisplay &&  actionsList.Count > actionsToDisplay){
				timer.Remove(timer[0]);
				actionsList.Remove(actionsList[0]);
			}
		}
	}

	public void AssignWeapon(GameObject weapon){
		WeaponToPick = weapon;
		for(int a = 0; a < playerWeapons.Count; a++){
			if(playerWeapons[a].weaponName == WeaponToPick.name){
				newWeapon = playerWeapons[a];
			}
		}
		
		for(int i = 0; i<weapons.Count;i++){
			if(weapons[i].name == weapManager.SelectedWeapon.weaponName){
				weaponToThrow = weapons[i];
			}
		}

		if(!newWeapon)
			return;

		if(weapManager.allWeapons.Contains(newWeapon)){
			if(newWeapon.GunType == WeaponScript.gunType.MACHINE_GUN){
				if(newWeapon.machineGun.clips < newWeapon.machineGun.bulletsPerClip * reserveAmmoLimit){
					newWeapon.machineGun.clips += newWeapon.machineGun.bulletsPerClip * pickAmmoMultiply;
					Destroy(WeaponToPick);
					//Register Action
					actionsList.Add(("Picked ammo for | " + newWeapon.weaponName).ToString());
					timer.Add(messageTimeOut);
				}else{
					text = "Full Ammo    ";
				}
			}
			if(newWeapon.GunType == WeaponScript.gunType.GRENADE_LAUNCHER){
				if(newWeapon.grenadeLauncher.ammoCount < reserveAmmoLimit){
					newWeapon.grenadeLauncher.ammoCount += pickAmmoMultiply;
					Destroy(WeaponToPick);
					//Register Action
					actionsList.Add(("Picked ammo for | " + newWeapon.weaponName).ToString());
					timer.Add(messageTimeOut);
				}else{
					text = "Full Ammo    ";
				}
			}
			if(newWeapon.GunType == WeaponScript.gunType.SHOTGUN){
				if(newWeapon.ShotGun.clips < newWeapon.ShotGun.bulletsPerClip * reserveAmmoLimit){
					newWeapon.ShotGun.clips += newWeapon.ShotGun.bulletsPerClip * pickAmmoMultiply;
					Destroy(WeaponToPick);
					//Register Action
					actionsList.Add(("Picked ammo for | " + newWeapon.weaponName).ToString());
					timer.Add(messageTimeOut);
				}else{
					text = "Full Ammo    ";
				}
			}
		}
	}
	
	/*void OnTriggerStay(Collider weapon){
		//Detect if we on pickable weapon
		if(weapon.gameObject.tag == "PickUp"){
			WeaponToPick = weapon.gameObject;
		}
	}
	
	void OnTriggerExit(Collider weapon){
		if(weapon.gameObject.tag == "PickUp"){
			WeaponToPick = null;
		}
	}*/
	
	void OnGUI(){
		GUI.skin = guiStyle;
		if(WeaponToPick){
			weapName = WeaponToPick.name;
			color = Mathf.Lerp(color, 0.9f, Time.deltaTime*10);
		}else{
			color = Mathf.Lerp(color, 0, Time.deltaTime*10);
		}
		
		GUI.color = new Color(1,1,1, color);
		
		if(weapManager && !weapManager.allWeapons.Contains(newWeapon)){
			text = "Press `F` to pick  |  " + weapName;
			
			Rect rect = new Rect (Screen.width/2 - text.Length*10/2,Screen.height - 105,text.Length*10, 45);
			GUI.Box (rect, text );
		}
		
		GUI.color = new Color(1,1,1, 0.6f);
		//Display actions
		GUILayout.BeginArea(new Rect (10,Screen.height - (actionsList.Count*33)-10, 300 ,Screen.height));
		GUILayout.BeginVertical();
		for(int i = 0; i < actionsList.Count; i++){
			GUILayout.Box(actionsList[i], GUILayout.Width(300), GUILayout.Height(30));
		}
		GUILayout.EndVertical();
		GUILayout.EndArea();
	}
}
