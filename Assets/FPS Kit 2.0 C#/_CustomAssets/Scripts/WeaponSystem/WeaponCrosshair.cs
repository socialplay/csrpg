﻿//NSdesignGames @ 2012 - 2014
//FPS Kit | Version 2.0

using UnityEngine;
using System.Collections;

public class WeaponCrosshair : MonoBehaviour {

	public Texture2D crosshairTexture; //Should be white square with small dimension 6 x 16 should be OK
	public float length = 15;
	public float width = 1;
	//Crosshair responce to player action
	public bool dynamicCrosshair = true;
	public float crosshairResponce = 60;
	public float defaultDistance = 40;
	public float smooth = 0.3f;

	bool crosshair = true;
	//Texture textu;
	GUIStyle lineStyle;
	float distance;
	float currentDistance;
	FPScontroller motor;
	WeaponManager weaponManager;
	WeaponScript weaponScript;
	
	void Awake () {
		lineStyle = new GUIStyle();
		lineStyle.normal.background = crosshairTexture;
		motor = transform.root.GetComponent<FPScontroller>();
		weaponManager = transform.root.GetComponentInChildren<WeaponManager>();
	}
	
	void Update(){
		if(weaponManager){
			if(weaponManager.SelectedWeapon){
				weaponScript = weaponManager.SelectedWeapon.GetComponent<WeaponScript>();
			}
		}
		
		if(Time.timeScale < 0.01f)
			return;
		
		if(dynamicCrosshair){
			var fireInput = Input.GetMouseButtonDown(0);
			//Dynamic crosshair***
			if(weaponScript && (fireInput || weaponScript.fire)){
				//Single weapons crosshair responce
				if(weaponScript.singleFire){
					if(fireInput && weaponScript.canFire && !weaponScript.isReload && !weaponScript.noBullets){
						if(distance < crosshairResponce * 4){
							distance = distance + crosshairResponce;
						}
					}else{
						distance = Mathf.Lerp(distance, defaultDistance, Time.deltaTime/smooth);
					}
				}else{
					//Automatic weapons crosshair responce
					if(weaponScript.fire && !weaponScript.noBullets){
						currentDistance = crosshairResponce * 2;
					}else{
						currentDistance = defaultDistance;	
					}
					distance = Mathf.Lerp(distance, currentDistance, Time.deltaTime/smooth);
				}
			}else{
				if(motor.Walking){
					currentDistance = crosshairResponce+motor.movement.velocity.magnitude * 2;
				}else{
					currentDistance = defaultDistance;
				}
				distance = Mathf.Lerp(distance, currentDistance, Time.deltaTime/smooth);
			}
		}else{
			distance = defaultDistance;
		}
		
		if(weaponScript){
			if(weaponScript.aimed){
				crosshair = false;
			}else{
				crosshair = true;
			}
		}
	}
	
	void OnGUI () {
		if(!(distance > (Screen.height/2)) && crosshair){
			
			GUI.Box(new Rect((Screen.width - distance)/2 - length, (Screen.height - width)/2, length, width), "", lineStyle);
			GUI.Box(new Rect((Screen.width + distance)/2, (Screen.height- width)/2, length, width), "", lineStyle);
			
			GUI.Box(new Rect((Screen.width - width)/2, (Screen.height - distance)/2 - length, width, length), "", lineStyle);
			GUI.Box(new Rect((Screen.width - width)/2, (Screen.height + distance)/2, width, length), "", lineStyle);
		}
	}
}
