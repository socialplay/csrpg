﻿//NSdesignGames @ 2012
//FPS Kit | Version 2.0

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof(AudioSource))]
//[AddComponentMenu ("FPS system/Weapon System/WeaponManager")]

public class WeaponManager : MonoBehaviour {
	//This script should be attached to an object called Weapons (Which children are all player weapons)
	//Weapons->Weapon1, Weapon2, Weapon3 etc.

	public float SwitchTime = 0.5f;
	[HideInInspector]
	public WeaponScript SelectedWeapon;
	public int index = 0; //Weapon index (What weapon we should take in the beginning of game). By default its 0 - take first weapon
	public AudioClip takeInAudio;
	public List<WeaponScript> allWeapons; //Resize this list in Inspector and assign as much weapons as you want to player spawn with
	
	GameObject defaultPrimaryWeap;
	GameObject defaultSecondaryWeap;

    internal WeaponScript[] availableWeapons; //Keep track of all weapons we have (Count any object with WeaponScript.cs assigned)
    internal event Action eventFinishedSwitching;
	
	internal bool canSwitch;

	void Awake(){
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(true);
        }
        availableWeapons = gameObject.GetComponentsInChildren<WeaponScript>();
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }

        for (int i = 0; i < allWeapons.Count; i++)
        {
            allWeapons[i].gameObject.SetActive(false);
        }
        TakeFirstWeapon(allWeapons[index].gameObject);
	}
	
	void Update () {
		if(Time.timeScale < 0.01f)
			return;

        if (index > allWeapons.Count - 1)
        {
            SelectedWeapon = allWeapons[0];
        }
        else
        {
            SelectedWeapon = allWeapons[index];
        }
		
        if(allWeapons.Count < 2)
			return;
		//Switch to next weapon
		if(Input.GetKeyDown(KeyCode.Alpha2) && canSwitch){
			if(index < allWeapons.Count-1){
				StartCoroutine(SwitchWeapons(allWeapons[index].gameObject, allWeapons[index+1].gameObject));
				index ++;
			}else{
				StartCoroutine(SwitchWeapons(allWeapons[allWeapons.Count-1].gameObject, allWeapons[0].gameObject));
				index = 0;
			}
		}
		
		//Switch to previous weapon
		if(Input.GetKeyDown(KeyCode.Alpha1) && canSwitch){
			if(index > 0){
				StartCoroutine(SwitchWeapons(allWeapons[index].gameObject, allWeapons[index-1].gameObject));
				index --;
			}else{
				StartCoroutine(SwitchWeapons(allWeapons[0].gameObject, allWeapons[allWeapons.Count-1].gameObject));
				index = allWeapons.Count-1;
			}
		}	
	}
	
	public void TakeFirstWeapon(GameObject nextWeapon){
		//Play take audio
		audio.clip = takeInAudio;
		audio.Play();
		
		nextWeapon.SetActive(true);	
        //nextWeapon.SendMessage("selectWeapon");
        nextWeapon.GetComponent<WeaponScript>().selectWeapon();
		canSwitch = true;
	}
	
	public IEnumerator SwitchWeapons(GameObject currentWeapon, GameObject nextWeapon){
        canSwitch = false;
		if(currentWeapon.activeSelf == true){
            //currentWeapon.SendMessage("deselectWeapon");
            currentWeapon.GetComponent<WeaponScript>().deselectWeapon();
		}
		yield return new WaitForSeconds(SwitchTime);
		//Play take audio
		audio.clip = takeInAudio;
		audio.Play();
		
		currentWeapon.SetActive(false);
		nextWeapon.SetActive(true);
        nextWeapon.GetComponent<WeaponScript>().selectWeapon();
        //nextWeapon.SendMessage("selectWeapon");
		canSwitch = true;

        if (eventFinishedSwitching != null)
        {
            eventFinishedSwitching();
        }
	}
}
