﻿//NSdesignGames @ 2012 - 2014
//FPS Kit | Version 2.0

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AmmoDeactivator : MonoBehaviour {
	//This script is used for next cases:
	//For example you pick RPG and shoot till last rocket
	//what this script do is deactivate rocked mesh, so it create and illusion
	//of having no ammo
	//Same for grenade and grenade launcher
	
	//Mesh that need to be deactivate while player have no ammo
	//For ex. Rocker, Grenade etc.
	public List<GameObject> objectsToDeactivate = new List<GameObject>();
	//This need to be attached to the same object as WeaponScript
	WeaponScript weapScript;
	
	void Start () {
		weapScript = gameObject.GetComponent<WeaponScript>();
	}
	
	void FixedUpdate () {
		//We make it work only with grenade launcher gun types
		if(weapScript.GunType == WeaponScript.gunType.GRENADE_LAUNCHER){
			for(var i = 0; i < objectsToDeactivate.Count; i++){
				if(weapScript.grenadeLauncher.ammoCount == 0){
					objectsToDeactivate[i].SetActive(false);
				}else{
					objectsToDeactivate[i].SetActive(true);
				}
			}
		}
	}
}
