﻿//NSdesignGames @ 2012 - 2014
//FPS Kit | Version 2.0

using UnityEngine;
using System.Collections;

public class AmmoDisplay : MonoBehaviour {
	//Display available weapons on top of screen, should be attached to the same game object as WeaponManager.cs

	public GUISkin guiStyle;
	public bool display = true;

	int bulletsLeft;
	float clips;
	WeaponScript weaponscript;
	WeaponManager weaponManager;
	
	WeaponScript currentWeapon;
	float color;
	
	
	void Awake () {
		weaponManager = gameObject.GetComponent<WeaponManager>();
	}
	
	void Update(){
		if(weaponManager.SelectedWeapon){
			weaponscript = weaponManager.SelectedWeapon.GetComponent<WeaponScript>();
		}
		if(!weaponscript)
			return;
		if(weaponscript.GunType == WeaponScript.gunType.MACHINE_GUN){
			bulletsLeft = weaponscript.machineGun.bulletsLeft;
			clips = weaponscript.machineGun.clips;
		}
		
		if(weaponscript.GunType == WeaponScript.gunType.SHOTGUN){
			bulletsLeft = weaponscript.ShotGun.bulletsLeft;
			clips = weaponscript.ShotGun.clips;
		}
		
		if(weaponscript.GunType == WeaponScript.gunType.GRENADE_LAUNCHER){
			clips = weaponscript.grenadeLauncher.ammoCount;
		}
		
		if(currentWeapon != weaponManager.SelectedWeapon){
			color = Mathf.Lerp(color, 0.3f, Time.deltaTime * 20);
			if(color < 0.32f){
				currentWeapon = weaponManager.SelectedWeapon;
			}
		}
	}
	
	void OnGUI (){
		if(!display)
			return;
		GUI.skin = guiStyle;
		GUI.color = new Color(1,1,1,0.9f);
		Rect rect = new Rect (Screen.width - 110,Screen.height - 55,100,45);
		if(weaponscript){
			if(weaponscript.GunType != WeaponScript.gunType.KNIFE){
				if(weaponscript.GunType == WeaponScript.gunType.GRENADE_LAUNCHER){ 
					GUI.Box (rect, clips.ToString());
				}else{
					GUI.Box (rect, bulletsLeft + " | " + clips);
				}
			}else{
				GUI.Box (rect, "∞");
			}
		}
		
		//SHow weapon list (Smoothly fade In/Out)
		if(weaponManager){
			GUILayout.BeginArea (new Rect (0,0,Screen.width,30));
			GUILayout.BeginHorizontal();
			for(int i = 0; i < weaponManager.allWeapons.Count; i++){
				if(weaponManager.allWeapons[i] == currentWeapon && currentWeapon == weaponManager.SelectedWeapon ){
					color = Mathf.Lerp(color, 0.9f, Time.deltaTime * 20);
					GUI.color = new Color(1,1,1, color);
				}else{
					GUI.color = new Color(1,1,1, 0.3f);
                }
                GUILayout.Box(weaponManager.allWeapons[i].weaponName, GUILayout.Height(30));
			}
			GUILayout.EndHorizontal();
			GUILayout.EndArea();
		}
	}

}
