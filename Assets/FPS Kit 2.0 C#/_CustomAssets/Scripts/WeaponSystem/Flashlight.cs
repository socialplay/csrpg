﻿//NSdesignGames @ 2012 - 2014
//FPS Kit | Version 2.0

using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]

public class Flashlight : MonoBehaviour {
	//Use this to add portable flash light to any weapon

	public bool turnOn = false;
	public Light flashLight;
	public AudioClip OnOffAudio;
	
	void Start () {
		if(turnOn){
			flashLight.enabled = true;
		}else{
			flashLight.enabled = false;
		}
	}
	
	void Update () {
		//Flash light input
		if(Input.GetKeyDown(KeyCode.G)){
			turnOn = !turnOn;
			flashLightOnOff();
		}
	}
	
	void flashLightOnOff(){
		//Play flash light On/Off sound
		audio.clip = OnOffAudio;
		audio.Play();
		//Activate flash light
		if(turnOn){
			flashLight.enabled = true;
		}else{
			flashLight.enabled = false;
		}
	}
}
