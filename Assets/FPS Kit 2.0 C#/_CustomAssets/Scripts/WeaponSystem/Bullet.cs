﻿//NSdesignGames @ 2012 - 2014
//FPS Kit | Version 2.0

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//[AddComponentMenu ("FPS system/Weapon System/Bullet Controller")]

public class Bullet : MonoBehaviour {
	public int speed = 500;
	public float life = 3;
	public int damage = 20;
	public int impactForce = 10;
	public bool impactHoles = true;
	//Does bullet do any damage to target?
	[HideInInspector]
	public bool doDamage = false;
	//Impact prefab name corresponds a tag it should hit
	public List<GameObject> impactObjects = new List<GameObject>();
	
	
	Vector3 velocity;
	Vector3 newPos;
	Vector3 oldPos;
	bool hasHit = false;
	
	void Start () {
		newPos = transform.position;
		oldPos = newPos;
		velocity = speed * transform.forward;
		
		// schedule for destruction if bullet never hits anything
		Destroy( gameObject, life );
	}
	
	void Update () {
		if( hasHit )
			return;
		// assume we move all the way
		newPos += velocity * Time.deltaTime;
		// Check if we hit anything on the way
		Vector3 direction = newPos - oldPos;
		float distance = direction.magnitude;
		
		if (distance > 0) {
			RaycastHit hit;
			
			if (Physics.Raycast(oldPos, direction, out hit, distance)) {
				// adjust new position
				newPos = hit.point;
				// notify hit
				hasHit = true;
				Quaternion rotation = Quaternion.FromToRotation(Vector3.up, hit.normal);
				
				//Apply force if we hit rigidbody
                //EDIT: Added check for tag "ItemDrop" to not add force.
				if (hit.rigidbody && hit.transform.tag != "ItemDrop"){
					hit.rigidbody.AddForce( transform.forward * impactForce, ForceMode.Impulse );
				}
				
				//////////////////////////////////////////////////////////////////HIT SURFACES/////////////////////////////////////////////////////////////
				if(impactHoles){
					//var impact : GameObject;
					for(int i = 0; i<impactObjects.Count; i++){
						if(hit.transform.tag == impactObjects[i].name){
							Instantiate(impactObjects[i], hit.point, rotation);
						}
					}
				}
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				//We cant hit ourselfs
				if(hit.transform.tag != "Player" && doDamage)
                {
                    //hit.transform.SendMessage("ApplyDamage", damage, SendMessageOptions.DontRequireReceiver);
                    if (hit.transform.GetComponent<HitBox>())
                    {
                        hit.transform.GetComponent<HitBox>().ApplyDamage(0);
                    }
				}
				
				Destroy (gameObject, 1);
			}
		}
		
		oldPos = transform.position;
		transform.position = newPos;
	}
}
