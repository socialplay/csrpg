﻿//NSdesignGames @ 2012
//FPS Kit | Version 2.0

using UnityEngine;
using System.Collections;


[AddComponentMenu ("FPS system/Weapon System/SniperScope")]

public class SniperScope : MonoBehaviour {

	public Texture2D scopeTexture; //Recommended size for scope texture is 1280 x 720
	public GameObject[] objectsToDeactivate; //When we aim, deactivate all visible Sniper parts (Hands, Gun etc.)
	
	WeaponScript weapScript;
	
	
	void Awake () {
		weapScript = gameObject.GetComponent<WeaponScript>();
	}
	
	void OnGUI () {
		if(weapScript.aimed){
			GUI.DrawTexture(new Rect(Screen.width/2- (Screen.height*1.8f)/2,Screen.height/2-Screen.height/2, Screen.height*1.8f, Screen.height), scopeTexture);
			for(int i = 0; i<objectsToDeactivate.Length;i++){
				objectsToDeactivate[i].SetActive(false);
			}
		}else{
			for(int a = 0; a<objectsToDeactivate.Length;a++){
				objectsToDeactivate[a].SetActive(true);
			}
		}
	}
}
