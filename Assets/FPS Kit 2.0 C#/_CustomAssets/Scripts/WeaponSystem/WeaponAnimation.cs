﻿//NSdesignGames @ 2012 - 2014
//FPS Kit | Version 2.0

using UnityEngine;
using System.Collections;

//[AddComponentMenu ("FPS system/Weapon System/WeaponAnimation")]

public class WeaponAnimation : MonoBehaviour {
	//This script is used to control main weapon animations
	//Should be attached to the object that contain weapon/hand animation
	public string Idle = "Idle";
	public string Reload = "Reload";
	public string Shoot = "Fire";
	public string TakeIn = "TakeIn";
	public string TakeOut = "TakeOut";
	public float FireAnimationSpeed = 1;
	public float TakeInOutSpeed = 1;
	
	string PlayThis;
	
	FPScontroller motor;
	GameObject player;
	
	void Awake () {
		animation.Play(Idle);
		animation.wrapMode = WrapMode.Once;
	}
	
	public void Fire(){
		animation.Rewind(Shoot);
		animation[Shoot].speed = FireAnimationSpeed;
		animation.Play(Shoot);
	}
	
	public void Reloading(float reloadTime) {
		animation.Stop(Reload);
		animation[Reload].speed = (animation[Reload].clip.length/reloadTime);
		animation.Rewind(Reload);
		animation.Play(Reload);
	}
	
	public void takeIn(){
		animation.Rewind(TakeIn);
		animation[TakeIn].speed = TakeInOutSpeed;
		animation[TakeIn].time = 0;
		animation.Play(TakeIn);
	}
	
	public void takeOut(){
		animation.Rewind(TakeOut);
		animation[TakeOut].speed = TakeInOutSpeed;
		animation[TakeOut].time = 0;
		animation.Play(TakeOut);
	}
}
