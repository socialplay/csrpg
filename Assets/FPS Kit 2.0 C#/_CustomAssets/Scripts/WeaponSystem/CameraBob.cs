﻿//NSdesignGames @ 2012 - 2014
//FPS Kit | Version 2.0

using UnityEngine;
using System.Collections;


//[AddComponentMenu ("FPS system/Character/FPS CameraBob")]

public class CameraBob : MonoBehaviour {
	public float walkBobbingSpeed = 0.21f;
	public float runBobbingSpeed = 0.35f;
	public float idleBobbingSpeed = 0.1f;
	public float bobbingAmount = 0.1f; 
	public float smooth = 1;

	Vector3 midpoint; 
	GameObject player;
	float timer = 0.0f; 
	float bobbingSpeed; 
	FPScontroller motor;
	float BobbingAmount;
	
	void Awake (){
		//Find player and FPScontroller script
		player = transform.root.gameObject;
		motor = player.GetComponent<FPScontroller>();
		midpoint = transform.localPosition;
	}
	
	void FixedUpdate () { 
		if(motor.prone)
			return;
		
		//This variables is used for slow motion effect (0.02 should be default fixed time value)
		float tempWalkSpeed = 0;
		float tempRunSpeed = 0;
		float tempIdleSpeed = 0;
		
		if(Time.timeScale == 1){
			if(tempWalkSpeed != walkBobbingSpeed || tempRunSpeed != runBobbingSpeed || tempIdleSpeed != idleBobbingSpeed){
				tempWalkSpeed = walkBobbingSpeed;
				tempRunSpeed = runBobbingSpeed;
				tempIdleSpeed = idleBobbingSpeed;
			}
		}else{
			tempWalkSpeed = walkBobbingSpeed*(Time.fixedDeltaTime/0.02f);
			tempRunSpeed = runBobbingSpeed*(Time.fixedDeltaTime/0.02f);
			tempIdleSpeed = idleBobbingSpeed*(Time.fixedDeltaTime/0.02f);
		}
		
		float waveslice = 0.0f;  
		float waveslice2 = 0.0f;
		Vector3 currentPosition = Vector3.zero;
		waveslice = Mathf.Sin(timer*2); 
		waveslice2 = Mathf.Sin(timer);
		timer = timer + bobbingSpeed; 
		if (timer > Mathf.PI * 2) { 
			timer = timer - (Mathf.PI * 2); 
		} 
		if (waveslice != 0) { 
			float TranslateChange = waveslice * BobbingAmount; 
			float TranslateChange2 = waveslice2 * BobbingAmount;
			float TotalAxes = Mathf.Clamp (1.0f, 0.0f, 1.0f); 
			float translateChange = TotalAxes * TranslateChange; 
			float translateChange2 = TotalAxes * TranslateChange2; 
			
			if(motor.grounded){
				//Player walk
				currentPosition = new Vector3(midpoint.x + translateChange2, midpoint.y + translateChange, currentPosition.z);
			}
			
		}else{
			//Player not move
			currentPosition = midpoint;
		} 
		//Walk/Run sway speed
		if (motor.Walking && !motor.Running) {
			bobbingSpeed = tempWalkSpeed;
			BobbingAmount = bobbingAmount;
		}else if(motor.Running) {
			bobbingSpeed = tempRunSpeed;
			BobbingAmount = bobbingAmount;
		}
		
		if(!motor.Running && !motor.Walking){
			bobbingSpeed = tempIdleSpeed;
			BobbingAmount = bobbingAmount*0.3f;
			
		}
		
		transform.localPosition = Vector3.Lerp(transform.localPosition, currentPosition, Time.deltaTime * smooth);
	}
}
