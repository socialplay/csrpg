﻿//NSdesignGames @ 2012 - 2014
//FPS Kit | Version 2.0

using UnityEngine;
using System.Collections;

public class TriggerActivate : MonoBehaviour {

	//This script is used in Single Player mode to teleport player to final point
	public GUISkin guiStyle;
	public GameObject weaps;
	public Transform teleportPoint;
	public MainMenu mm;
	
	bool inside;

	GameObject player;
	
	void OnTriggerEnter(Collider other){
		//Detect if we on pickable weapon
		if(other.gameObject.tag == "Player"){
			inside = true;
			player = other.gameObject;
		}
	}
	
	void OnTriggerExit(Collider other){
		if(other.gameObject.tag == "Player"){
			inside = false;
			player = null;
		}
	}
	
	void Update(){
		if(inside){
			if(Input.GetKeyDown(KeyCode.F) && player){
				player.transform.position = teleportPoint.position;
				player.transform.rotation = teleportPoint.rotation;
				inside = false;
				weaps.SetActive(true);
				mm.finishedGame = true;
				RenderSettings.fog = false;
				Destroy(gameObject);
			}
		}
	}
	
	void OnGUI(){
		if(!inside)
			return;
		GUI.skin = guiStyle;
		GUI.color = new Color(1,1,1, 0.9f);
		GUI.depth = -10;
		string text = "Press ´F´ for MORE GUNS!        ";
		Rect rect = new Rect (Screen.width/2 - text.Length*9/2,Screen.height/2 - 25,text.Length * 9, 50);
		GUI.Box (rect, text);
	}
}
