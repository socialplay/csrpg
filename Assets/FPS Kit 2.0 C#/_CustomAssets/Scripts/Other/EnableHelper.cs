﻿//NSdesignGames @ 2012 - 2014
//FPS Kit | Version 2.0

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnableHelper : MonoBehaviour {
	//EnableHelper used to interact between C# and JS
	//Create empty game object and tag it "EnableHelper"
	//When this object active, assigned scripts are enabled
	//Once we deactivate this object, all script become disabled aswell
	//Usefull for pausing game without changing timeScale
	
	public FPScontroller fpsController;
	public WeaponManager weaponManager;
	//Assign 2 mouse looks
	public FPSMouseLook mouseLook1;
	public FPSMouseLook mouseLook2;
	
    internal GameObject enablerReferenceObject;

    public bool keepWeaponActive = false;
	
	void Start () {
		enablerReferenceObject = GameObject.FindWithTag("EnableHelper");
	}
	
	void FixedUpdate () {
		//Reference object is enabled, enable all assigned scripts
		if(enablerReferenceObject && enablerReferenceObject.activeSelf == true){
			if(fpsController.canControl == false){
				fpsController.canControl = true;
			}
            //if(weaponManager.enabled == false && weaponManager.SelectedWeapon && weaponManager.SelectedWeapon.enabled == false){
            //    weaponManager.enabled = true;
            //    weaponManager.SelectedWeapon.enabled = true;
            //}
            
            //if (weaponManager != null) weaponManager.SelectedWeapon.canFire = true;
			
            if(mouseLook1.enabled == false && mouseLook2.enabled == false){
				mouseLook1.enabled = true;
				mouseLook2.enabled = true;
			}
		}
		
		//Reference object is disabled, deactivate all assigned scripts
		if(!enablerReferenceObject || enablerReferenceObject.activeSelf == false){
			if(fpsController.canControl == true ){
				fpsController.canControl = false;
			}
                //if (weaponManager.enabled == true && weaponManager.SelectedWeapon && weaponManager.SelectedWeapon.enabled == true)
                //{
                //    if (!keepWeaponActive)
                //    {
                //        weaponManager.SelectedWeapon.enabled = false;
                //        weaponManager.enabled = false;
                //    }
                //    else
                //    {
                //        weaponManager.SelectedWeapon.canFire = false;
                //    }
                //}
			if(mouseLook1.enabled == true && mouseLook2.enabled == true){
				mouseLook1.enabled = false;
				mouseLook2.enabled = false;
			}
		}

		if(!enablerReferenceObject){
			if(GameObject.FindWithTag("EnableHelper") != null){
				enablerReferenceObject = GameObject.FindWithTag("EnableHelper");
			}
		}
	}
}
