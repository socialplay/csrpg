﻿//NSdesignGames @ 2012 - 2014
//FPS Kit | Version 2.0

using UnityEngine;
using System.Collections;

public class SlowMotionEffect : MonoBehaviour {

	//Slow motion effect design to used in Single Player Mode
	public bool slowMotion;
	public GUISkin guiSkin;
	public float slowTimeTo = 0.5f; //How much should we slow time. Recomended values from 0.2 to 0.9 

	[HideInInspector]
	public AudioSource[] audios;
	
	void Update () {
		if(Time.timeScale < 0.01f)
			return;
		
		if(Input.GetKeyDown(KeyCode.Q)){
			slowMotion = !slowMotion;
			if(slowMotion){
				audios = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
				for (int i = 0; i < audios.Length; i++){
					audios[i].pitch = slowTimeTo;
				}
				Time.timeScale = slowTimeTo;
				Time.fixedDeltaTime = 0.005f;
			}
			else if(!slowMotion && Time.deltaTime != 1){
				audios = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
				for (int a = 0; a < audios.Length; a++){
					audios[a].pitch = 1;
				}
				Time.timeScale = 1;
				Time.fixedDeltaTime = 0.02f;
			}
		}
	}
	
	void OnGUI(){
		GUI.skin = guiSkin;
		GUI.color = new Color(1,1,1, 0.8f);
		GUI.Box(new Rect(Screen.width-205, 60, 200, 30), "Slow Mo: " + slowMotion.ToString());
	}
}
