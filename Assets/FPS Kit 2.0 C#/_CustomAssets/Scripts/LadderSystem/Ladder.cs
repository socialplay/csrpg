﻿//NSdesignGames @ 2012 - 2014
//FPS Kit | Version 2.0

using UnityEngine;
using System.Collections;

//[AddComponentMenu("FPS system/Ladder System/Attach to Ladder")]

public class Ladder : MonoBehaviour {

	//Attach this script to a ladder object, add collider to it and mark "trigger"
	//and dont forget to tag it "Ladder"
	//make 2 GameObjects rename them to LadderTop and LadderBottom 
	//and place relative in bottom and top of ladder
	//Assign that objects on ladderBottom and ladderTop variables

	public GameObject ladderBottom;
	public GameObject ladderTop;
	
	Vector3 climbDirection = Vector3.zero;
	
	void Start () {
		climbDirection = ladderTop.transform.position -  ladderBottom.transform.position;
	}
	
	public Vector3 ClimbDirection () {
		return climbDirection;
	}

	void OnTriggerEnter (Collider other) {
		if(other.transform.root.tag == "Player") {	
			LadderPlayer lp = other.transform.root.GetComponent<LadderPlayer>();
			lp.LatchLadder(this);
		}
	}
}
