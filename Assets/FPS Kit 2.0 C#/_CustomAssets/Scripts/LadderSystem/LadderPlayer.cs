﻿//NSdesignGames @ 2012 - 2014
//FPS Kit | Version 2.0

using UnityEngine;
using System.Collections;


[RequireComponent(typeof(CharacterController))]
//[AddComponentMenu("FPS system/Ladder System/Attach to Player")]

public class LadderPlayer : MonoBehaviour {
	//This script should be attached to player instance, it will comunicate with FPSController.cs and Ladder.cs

	public float climbSpeed = 6.0f; // The speed of the player up and down the ladder. Roughly equal to walking speed is a good starting point.
	public float climbDownThreshold = -0.4f; // In the range -1 to 1 where -1 == -90 deg, 1 = 90 deg, angle of view camera at which the user climbs down rather than up when moving with the forward key.
	
	//Private vars
	Vector3 climbDirection = Vector3.zero;
	Vector3 lateralMove = Vector3.zero;
	//Vector3 forwardMove = Vector3.zero;
	Vector3 ladderMovement = Vector3.zero;
	Ladder currentLadder = null;
	bool latchedToLadder = false;
	bool inLandingPad = false;
	GameObject mainCamera = null;
	CharacterController controller = null;
 	bool trigger = false;
	
	
	void Start () {
		mainCamera = Camera.main.gameObject;
		controller = GetComponent<CharacterController>();
	}
	
	/*void OnTriggerEnter (Collider other) {
		if(other.tag == "Ladder") {		
			LatchLadder(other.gameObject, other);
			trigger = true;
		}
	}*/
	
	/*void OnTriggerExit () {		
		UnlatchLadder();
	}*/
	
	//Connect the player to the ladder, and shunt FixedUpdate calls to the special ladder movment functions.
	public void LatchLadder (Ladder latchedLadder) {
		// typecast the latchedLadder as a Ladder object from GameObject
		//if(latchedLadder.GetComponent<Ladder>() != null){
			currentLadder = latchedLadder;
			latchedToLadder = true;
			
			// get the climb direction
			climbDirection = currentLadder.ClimbDirection();
			
			// let the other scripts know we are on the ladder now
			gameObject.SendMessage("OnLadder", null, SendMessageOptions.RequireReceiver);
			trigger = true;
		//}
	}
	
	
	//Shut off special ladder movement controls and return to normal movement operations.
	void UnlatchLadder () {
		latchedToLadder = false;
		currentLadder = null;
		trigger = false;
		
		// let the other scripts know we are off the ladder now
		gameObject.SendMessage("OffLadder", ladderMovement, SendMessageOptions.RequireReceiver);
	}
	
	
	//Convert the player's normal forward and backward motion into up and down motion on the ladder.
	void FixedUpdate () {
		if(!latchedToLadder)
			return;
		
		if(trigger){
			RaycastCheck ();
		}
		
		// If we jumpped, then revert back to the original behavior
		if (Input.GetKey(KeyCode.Space)) {
			UnlatchLadder();
		}
		
		// find the climbing direction
		Vector3 verticalMove = climbDirection.normalized;
		
		// convert forward motion to vertical motion
		verticalMove *= Input.GetAxis("Vertical");
		verticalMove *= (mainCamera.transform.forward.y > climbDownThreshold) ? 1 : -1;
		
		// find lateral component of motion
		if (inLandingPad) {
			lateralMove = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
		} else {
			lateralMove = new Vector3(Input.GetAxis("Horizontal"), 0, 0);
		}
		lateralMove = transform.TransformDirection(lateralMove);
		
		// move
		ladderMovement = verticalMove + lateralMove;
		controller.Move(ladderMovement * climbSpeed * Time.deltaTime);
	}
	
	void RaycastCheck () {
		RaycastHit hit;
		Vector3 p1 = transform.position + controller.center + Vector3.up * (-controller.height * 0.5f);
		Vector3 p2 = p1 + Vector3.up * controller.height;
		// Cast character controller shape 10 meters forward, to see if it is about to hit anything
		if (!Physics.CapsuleCast (p1, p2, controller.radius, transform.forward, out hit, controller.radius)){
			UnlatchLadder();
		}
	}
}
