﻿//NSdesignGames @ 2012 - 2014
//FPS Kit | Version 2.0 + Multiplayer

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterAnimation : MonoBehaviour {

	//This script will track of player input and than pass it over Network to remote instance through PlayerNetworkController.cs and than play then play animations remotely
	
	//Use this gameObject to send messages to AnimationSync.cs
	[HideInInspector]
	public string animationSyncHelper;
	[HideInInspector]
	public string animationForHands;
	[HideInInspector]
	public string activeWeapon; //This gameObject.name will tell PlayerNetworkController.cs which third person weapon is active now
	[HideInInspector]
	public string animationType; //Object that will send current animation type to PlayerNetworkController.cs

	public WeaponManager weaponManager;

	public enum Action {Stand, Crouch, Prone}
	Action action;

	[System.Serializable]
	public class animations{
		//Idle animations
		public AnimationClip jumpPose;
		public AnimationClip stayIdle;
		public AnimationClip crouchIdle;
		public AnimationClip proneIdle;
		//Walk Animations
		public AnimationClip walkFront;
		public AnimationClip walkBack;
		public AnimationClip walkLeft;
		public AnimationClip walkRight;
		//Run animations
		public AnimationClip runFront;
		//Crouch animations
		public AnimationClip crouchFront;
		public AnimationClip crouchLeft;
		public AnimationClip crouchRight;
		public AnimationClip crouchBack;
		//Prone Animations
		public AnimationClip proneFront;
		public AnimationClip proneLeft;
		public AnimationClip proneRight;
		public AnimationClip proneBack;
		//Weapon animations
		public AnimationClip pistolIdle;
		public AnimationClip knifeIdle;
		public AnimationClip gunIdle;
		/*var fire : AnimationClip;
		var reload : AnimationClip;*/
	}
	
	public animations Animations;
	
	public List<WeaponScript> twoHandedWeapons = new List<WeaponScript>();
	public List<WeaponScript> pistols = new List<WeaponScript>();
	public List<WeaponScript> knivesNades = new List<WeaponScript>();
	
	FPScontroller fpsController;
	
	void Start () {
		fpsController = transform.root.GetComponent<FPScontroller>();
		configureAnimations();
		if(weaponManager){
			ThirdPersonWeaponControl();
		}
	}
	
	void LateUpdate () {
		if(weaponManager.SelectedWeapon){
			activeWeapon = weaponManager.SelectedWeapon.weaponName;
		}
		if(!fpsController.crouch && !fpsController.prone){
			action = Action.Stand;
		}else{
			if(fpsController.crouch && !fpsController.prone){
				action = Action.Crouch;
			}
			if(!fpsController.crouch && fpsController.prone){
				action = Action.Prone;
			}
			if(fpsController.crouch && fpsController.prone){
				action = Action.Crouch;
			}
		}
		
		if(action == Action.Stand){
			if(fpsController.grounded){
				if(fpsController.Walking){
					if(!fpsController.Running){
						animationType = "Walking";
						if( Input.GetKey(KeyCode.W)){
							animation.CrossFade(Animations.walkFront.name, 0.2f);
							//Send animation name (needed for multiplayer)
							animationSyncHelper = Animations.walkFront.name;
						}
						else if(Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.S)){
							animation.CrossFade(Animations.walkLeft.name, 0.2f);
							//Send animation name (needed for multiplayer)
							animationSyncHelper = Animations.walkLeft.name;
						}
						else if( Input.GetKey(KeyCode.D) && !Input.GetKey(KeyCode.S)){
							animation.CrossFade(Animations.walkRight.name, 0.2f);
							//Send animation name (needed for multiplayer)
							animationSyncHelper = Animations.walkRight.name;
						}
						else if(Input.GetKey(KeyCode.S)){
							animation.CrossFade(Animations.walkBack.name, 0.2f);
							//Send animation name (needed for multiplayer)
							animationSyncHelper = Animations.walkBack.name;
						}
					}else{
						animationType = "Running";
						if( Input.GetKey(KeyCode.W) ){
							animation.CrossFade(Animations.runFront.name, 0.2f);
							//Send animation name (needed for multiplayer)
							animationSyncHelper = Animations.runFront.name;
						}
					}
				}else{
					animation.CrossFade(Animations.stayIdle.name, 0.2f);
					//Send animation name (needed for multiplayer)
					animationSyncHelper = Animations.stayIdle.name;
				}
			}else{
				animation.CrossFade(Animations.jumpPose.name, 0.2f);
				//Send animation name (needed for multiplayer)
				animationSyncHelper = Animations.jumpPose.name;
			}
		}
		
		if(action == Action.Crouch){
			animationType = "Crouch";
			if(fpsController.Walking ){
				if( Input.GetKey(KeyCode.W) ){
					animation.CrossFade(Animations.crouchFront.name, 0.2f);
					//Send animation name (needed for multiplayer)
					animationSyncHelper = Animations.crouchFront.name;
				}
				else if(Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.S)){
					animation.CrossFade(Animations.crouchLeft.name, 0.2f);
					//Send animation name (needed for multiplayer)
					animationSyncHelper = Animations.crouchLeft.name;
				}
				else if( Input.GetKey(KeyCode.D) && !Input.GetKey(KeyCode.S)){
					animation.CrossFade(Animations.crouchRight.name, 0.2f);
					//Send animation name (needed for multiplayer)
					animationSyncHelper = Animations.crouchRight.name;
				}
				else if(Input.GetKey(KeyCode.S)){
					animation.CrossFade(Animations.crouchBack.name, 0.2f);
					//Send animation name (needed for multiplayer)
					animationSyncHelper = Animations.crouchBack.name;
				}
			}else{
				animation.CrossFade(Animations.crouchIdle.name, 0.2f);
				//Send animation name (needed for multiplayer)
				animationSyncHelper = Animations.crouchIdle.name;
			}
		}
		if(action == Action.Prone){
			animationType = "Prone";
			if(fpsController.Walking ){
				if( Input.GetKey(KeyCode.W) ){
					animation.CrossFade(Animations.proneFront.name, 0.2f);
					//Send animation name (needed for multiplayer)
					animationSyncHelper = Animations.proneFront.name;
				}
				else if(Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.S)){
					animation.CrossFade(Animations.proneLeft.name, 0.2f);
					//Send animation name (needed for multiplayer)
					animationSyncHelper = Animations.proneLeft.name;
				}
				else if( Input.GetKey(KeyCode.D) && !Input.GetKey(KeyCode.S)){
					animation.CrossFade(Animations.proneRight.name, 0.2f);
					//Send animation name (needed for multiplayer)
					animationSyncHelper = Animations.proneRight.name;
				}
				else if(Input.GetKey(KeyCode.S)){
					animation.CrossFade(Animations.proneBack.name, 0.2f);
					//Send animation name (needed for multiplayer)
					animationSyncHelper = Animations.proneBack.name;
				}
			}else{
				animation.CrossFade(Animations.proneIdle.name, 0.2f);
				//Send animation name (needed for multiplayer)
				animationSyncHelper = Animations.proneIdle.name;
			}
		}
		ThirdPersonWeaponControl();
	}
	
	void ThirdPersonWeaponControl(){
		if(action != Action.Prone){
			if(twoHandedWeapons.Contains(weaponManager.SelectedWeapon)){
				animationForHands = Animations.gunIdle.name;
			}
			else if(pistols.Contains(weaponManager.SelectedWeapon)){
				animationForHands = Animations.pistolIdle.name;
			}
			else if(knivesNades.Contains(weaponManager.SelectedWeapon)){
				animationForHands = Animations.knifeIdle.name;
			}
		}else{
			animationForHands = "Null";
		}
	}
	
	void configureAnimations(){
		//Set animations Wrap Mode and Speed
		if(Animations.stayIdle){
			animation[Animations.stayIdle.name].wrapMode = WrapMode.Loop;
		}
		if(Animations.crouchIdle){
			animation[Animations.crouchIdle.name].wrapMode = WrapMode.Loop;
		}
		if(Animations.proneIdle){
			animation[Animations.proneIdle.name].wrapMode = WrapMode.Loop;
		}
		if(Animations.walkFront){
			animation[Animations.walkFront.name].wrapMode = WrapMode.Loop;
			//animation[Animations.walkFront.name].speed = Animations.walkAnimationsSpeed;
		}
		if(Animations.walkBack){
			animation[Animations.walkBack.name].wrapMode = WrapMode.Loop;
			//animation[Animations.walkBack.name].speed = Animations.walkAnimationsSpeed;
		}
		if(Animations.walkLeft){
			animation[Animations.walkLeft.name].wrapMode = WrapMode.Loop;
			//animation[Animations.walkLeft.name].speed = Animations.walkAnimationsSpeed;
		}
		if(Animations.walkRight){
			animation[Animations.walkRight.name].wrapMode = WrapMode.Loop;
			//animation[Animations.walkRight.name].speed = Animations.walkAnimationsSpeed;
		}
		if(Animations.runFront){
			animation[Animations.runFront.name].wrapMode = WrapMode.Loop;
			//animation[Animations.runFront.name].speed = Animations.runAnimationsSpeed;
		}
		if(Animations.crouchFront){
			animation[Animations.crouchFront.name].wrapMode = WrapMode.Loop;
			//animation[Animations.crouchFront.name].speed = Animations.crouchAnimationsSpeed;
		}
		if(Animations.crouchLeft){
			animation[Animations.crouchLeft.name].wrapMode = WrapMode.Loop;
			//animation[Animations.crouchLeft.name].speed = Animations.crouchAnimationsSpeed;
		}
		if(Animations.crouchRight){
			animation[Animations.crouchRight.name].wrapMode = WrapMode.Loop;
			//animation[Animations.crouchRight.name].speed = Animations.crouchAnimationsSpeed;
		}
		if(Animations.crouchBack){
			animation[Animations.crouchBack.name].wrapMode = WrapMode.Loop;
			//animation[Animations.crouchBack.name].speed = Animations.crouchAnimationsSpeed;
		}
		if(Animations.proneFront){
			animation[Animations.proneFront.name].wrapMode = WrapMode.Loop;
			//animation[Animations.proneFront.name].speed = Animations.proneAnimationsSpeed;
		}
		if(Animations.proneLeft){
			animation[Animations.proneLeft.name].wrapMode = WrapMode.Loop;
			//animation[Animations.proneLeft.name].speed = Animations.proneAnimationsSpeed;
		}
		if(Animations.proneRight){
			animation[Animations.proneRight.name].wrapMode = WrapMode.Loop;
			//animation[Animations.proneRight.name].speed = Animations.proneAnimationsSpeed;
		}
		if(Animations.proneBack){
			animation[Animations.proneBack.name].wrapMode = WrapMode.Loop;
			//animation[Animations.proneBack.name].speed = Animations.proneAnimationsSpeed;
		}
	}
}
