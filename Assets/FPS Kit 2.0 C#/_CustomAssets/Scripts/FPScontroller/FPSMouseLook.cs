﻿//NSdesignGames @ 2012
//FPS Kit | Version 2.0
using UnityEngine;
using System.Collections;

//[AddComponentMenu ("FPS system/Character/FPS MouseLook")]

public class FPSMouseLook : MonoBehaviour {

	public enum RotationAxes{ MouseXAndY, MouseX, MouseY }
	public RotationAxes axes = RotationAxes.MouseXAndY;
	public float sensitivity = 4;
	public float aimSensitivity = 2;
	
	[HideInInspector]
	public float sensitivityX = 15;
	[HideInInspector]
	public float sensitivityY = 15;
	//float minimumX = -360;
	//float maximumX = 360;
	
	public float minimumY = -80;
	public float maximumY = 80;
	
	float rotationY = 0;
	
	//Find WeaponManager script to know when selected weapon is aimed or not
	WeaponManager weapManager;
	WeaponScript weapScript;
	
	//Added for sniper scope
	[HideInInspector]
	public float currentSensitivity;
	
	void Awake (){
		//weapManager = GameObject.FindWithTag("WeaponManager").GetComponent<WeaponManager>();
		weapManager = transform.root.GetComponentInChildren<WeaponManager>();
	}
	
	void Update (){
		if(Time.timeScale < 0.01f)
			return;
		if (axes == RotationAxes.MouseXAndY){
			float rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;
			
			rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
			rotationY = Mathf.Clamp (rotationY, minimumY, maximumY);
			
			transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
		}
		else if (axes == RotationAxes.MouseX){
			transform.Rotate(0, Input.GetAxis("Mouse X") * sensitivityX, 0);
		}else{
			rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
			rotationY = Mathf.Clamp (rotationY, minimumY, maximumY);
			
			transform.localEulerAngles = new Vector3(-rotationY, transform.localEulerAngles.y, 0);
		}
		
		//Find current selected weapon script
		if(weapManager.SelectedWeapon){
			if(!weapScript){
				weapScript = weapManager.SelectedWeapon.GetComponent<WeaponScript>();
			}
		}else{
			weapScript = null;
		}

		//Check if weapon aimed or not
		if(weapScript && weapScript.aimed){
			currentSensitivity = aimSensitivity;
		}else{
			currentSensitivity = sensitivity;
		}
		sensitivityX = currentSensitivity;
		sensitivityY = currentSensitivity;
	}
	
	public void Recoil(float amount){
		rotationY += amount;
	}
}
