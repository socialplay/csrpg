﻿//NSdesignGames @ 2012 - 2014
//FPS Kit | Version 2.0

using UnityEngine;
using System.Collections;


[RequireComponent (typeof(AudioSource))]

public class FPSSoundController : MonoBehaviour {

	public AudioClip[] walkSounds;
	public float walkStepLength = 0.45f;
	public float runStepLenght = 0.38f;
	public float crouchStepLenght = 0.38f;
	
	//CharacterController controller;
	FPScontroller motor;
	float lastStep= -10.0f;
	float StepLenght;
	
	void Awake(){
		StepLenght = walkStepLength;
		//controller = GetComponent<CharacterController>();
		motor = GetComponent<FPScontroller>();
	}
	
	void FixedUpdate(){
		//Check when player walk or run to play footstep sounds with different speed
		if(motor.prone)
			return;
		if(motor.Walking && motor.grounded && !motor.crouch){
			PlayStepSounds();
			StepLenght = walkStepLength;
		}
		if(motor.Running && motor.grounded){
			PlayStepSounds();
			StepLenght = runStepLenght;
		}
		if(motor.Walking && motor.crouch && motor.grounded){
			PlayStepSounds();
			StepLenght = crouchStepLenght;
		}
	}
	
	
	void PlayStepSounds(){
		if(StepLenght == 0)
			return;
		if (Time.time > StepLenght + lastStep){
			audio.clip = walkSounds[Random.Range(0, walkSounds.Length)];
			audio.Play();
			lastStep = Time.time;
		}
	}
}
