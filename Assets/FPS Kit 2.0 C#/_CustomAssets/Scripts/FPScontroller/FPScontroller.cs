﻿//NSdesignGames @ 2012
//FPS Kit | Version 2.0
using UnityEngine;
using System.Collections;


//@script AddComponentMenu ("FPS system/Character/FPS Controller")
// Require a character controller to be attached to the same game object
[RequireComponent (typeof (CharacterController))]
[RequireComponent (typeof (FPSinput))]

public class FPScontroller : MonoBehaviour {
	
	// Should this script respond to input?
	public bool canControl = true;
	public GameObject lookObj; //This is root object that containc MainCamera, Weapons etc.
	
	bool useFixedUpdate = false;
	
	//Check when run, walk or when can run or not
	[HideInInspector]
	public bool Running ;
	[HideInInspector]
	public bool Walking;
	[HideInInspector]
	public bool canRun;
	
	//Ladder variables
	private GameObject mainCamera = null;
	[HideInInspector]
	public bool onLadder = false;
	//private float ladderHopSpeed = 6.0f;
	
	// For the next variables, @System.NonSerialized tells Unity to not serialize the variable or show it in the inspector view.
	// Very handy for organization!
	
	// The current global direction we want the character to move in.
	[System.NonSerialized]
	public Vector3 inputMoveDirection = Vector3.zero;
	
	// Is the jump button held down? We use this interface instead of checking
	// for the jump button directly so this script can also be used by AIs.
	[System.NonSerialized]
	public bool inputJump = false;
	
	[HideInInspector]
	public bool inputRun = false;
	
	[HideInInspector]
	public bool inputCrouch = false;
	
	[HideInInspector]
	public bool inputProne = false;

	[System.Serializable]
	public class  FPScontrollerMovement {
		// The maximum horizontal speed when moving
		[HideInInspector]
		public float maxForwardSpeed = 10.0f;
		[HideInInspector]
		public float maxSidewaysSpeed = 10.0f;
		[HideInInspector]
		public float maxBackwardsSpeed = 10.0f;
		
		//Run and walk variables
		public float WalkSpeed = 6.0f;
		public float RunSpeed = 9.0f;
		//Crouch
		public bool canCrouch = true;
		public float CrouchSpeed = 3.0f;
		public float crouchHeight = 1.5f;
		public float crouchSmooth = 8;
		//prone
		public bool canProne = true;
		public float ProneSpeed = 1.5f;
		public float proneHeight = 0.7f;
		
		// Curve for multiplying speed based on slope (negative = downwards)
		public AnimationCurve slopeSpeedMultiplier = new AnimationCurve(new Keyframe(-90, 1), new Keyframe(0, 1), new Keyframe(90, 0));
		
		// How fast does the character change speeds?  Higher is faster.
		public float maxGroundAcceleration = 30.0f;
		public float maxAirAcceleration = 20.0f;
		
		// The gravity for the character
		public float gravity = 10.0f;
		public float maxFallSpeed = 20.0f;
		
		[HideInInspector]
		public bool enableGravity = true;
		
		// For the next variables, @System.NonSerialized tells Unity to not serialize the variable or show it in the inspector view.
		// Very handy for organization!
		
		// The last collision flags returned from controller.Move
		[System.NonSerialized]
		public CollisionFlags collisionFlags;
		
		// We will keep track of the character's current velocity,
		[System.NonSerialized]
		public Vector3 velocity;
		
		// This keeps track of our current velocity while we're not grounded
		[System.NonSerialized]
		public Vector3 frameVelocity = Vector3.zero;
		
		[System.NonSerialized]
		public Vector3 hitPoint = Vector3.zero;
		
		[System.NonSerialized]
		public Vector3 lastHitPoint = new Vector3(Mathf.Infinity, 0, 0);
	}

	public FPScontrollerMovement movement = new FPScontrollerMovement();
	
	//Crouching vars
	[HideInInspector]
	public bool crouch;
	float standartHeight;
	float centerY;
	bool canStand;
	bool canStandCrouch = true;
	
	//Prone vars
	[HideInInspector]
	public bool prone;

	public enum FPSMovementTransferOnJump {
		None, // The jump is not affected by velocity of floor at all.
		InitTransfer, // Jump gets its initial velocity from the floor, then gradualy comes to a stop.
		PermaTransfer, // Jump gets its initial velocity from the floor, and keeps that velocity until landing.
		PermaLocked // Jump is relative to the movement of the last touched floor and will move together with that floor.
	}
	
	// We will contain all the jumping related variables in one helper class for clarity.
	[System.Serializable]
	public class FPScontrollerJumping {
		// Can the character jump?
		public bool enabled = true;
		
		// How high do we jump when pressing jump and letting go immediately
		public float baseHeight = 1.0f;
		
		// We add extraHeight units (meters) on top when holding the button down longer while jumping
		public float extraHeight = 4.1f;
		
		// How much does the character jump out perpendicular to the surface on walkable surfaces?
		// 0 means a fully vertical jump and 1 means fully perpendicular.
		public float perpAmount = 0.0f;
		
		// How much does the character jump out perpendicular to the surface on too steep surfaces?
		// 0 means a fully vertical jump and 1 means fully perpendicular.
		public float steepPerpAmount = 0.5f;
		
		// For the next variables, @System.NonSerialized tells Unity to not serialize the variable or show it in the inspector view.
		// Very handy for organization!
		
		// Are we jumping? (Initiated with jump button and not grounded yet)
		// To see if we are just in the air (initiated by jumping OR falling) see the grounded variable.
		[System.NonSerialized]
		public bool jumping = false;
		
		[System.NonSerialized]
		public bool holdingJumpButton = false;
		
		// the time we jumped at (Used to determine for how long to apply extra jump power after jumping.)
		[System.NonSerialized]
		public float lastStartTime = 0.0f;
		
		[System.NonSerialized]
		public float lastButtonDownTime = -100;
		
		[System.NonSerialized]
		public Vector3 jumpDir = Vector3.up;
	}
	
	public FPScontrollerJumping jumping = new FPScontrollerJumping();

	[System.Serializable]
	public class FPScontrollerMovingPlatform {
		public bool enabled = true;
		
		public FPSMovementTransferOnJump movementTransfer = FPSMovementTransferOnJump.PermaTransfer;
		
		[System.NonSerialized]
		public Transform hitPlatform;
		
		[System.NonSerialized]
		public Transform activePlatform;
		
		[System.NonSerialized]
		public Vector3 activeLocalPoint;
		
		[System.NonSerialized]
		public Vector3 activeGlobalPoint;
		
		[System.NonSerialized]
		public Quaternion activeLocalRotation;
		
		[System.NonSerialized]
		public Quaternion activeGlobalRotation;
		
		[System.NonSerialized]
		public Matrix4x4 lastMatrix;
		
		[System.NonSerialized]
		public Vector3 platformVelocity;
		
		[System.NonSerialized]
		public bool newPlatform;
	}
	
	public FPScontrollerMovingPlatform movingPlatform = new FPScontrollerMovingPlatform();

	[System.Serializable]
	public class FPScontrollerSliding {
		// Does the character slide on too steep surfaces?
		public bool enabled = true;
		
		// How fast does the character slide on steep surfaces?
		public float slidingSpeed = 15;
		
		// How much can the player control the sliding direction?
		// If the value is 0.5 the player can slide sideways with half the speed of the downwards sliding speed.
		public float sidewaysControl = 1.0f;
		
		// How much can the player influence the sliding speed?
		// If the value is 0.5 the player can speed the sliding up to 150% or slow it down to 50%.
		public float speedControl = 0.4f;
	}
	
	public FPScontrollerSliding sliding = new FPScontrollerSliding();

	[System.Serializable]
	public class FPScontrollerPushing {	
		//Push RigidBodies
		public bool canPush = true;
		public float pushPower = 2.0f;
	}
	public FPScontrollerPushing pushing;


	//Fall Damage NOTE: This require PlayerDamage.cs assigned to same game object (Multiplayer Only)
	[System.Serializable]
	public class FallDamage {
		public float fallLimit = 0.5f; //How long in seconds player can remain in free fall before damage is taken
		public float fallDamageMultiplier = 5; //This multiplier determines how much damage deal at certain speed ex. Total Damage = playerYSpeed * fallDamageMultiplier
	}
	public FallDamage fallDamage;
	
	[System.NonSerialized]
	public bool grounded = true;
	
	[System.NonSerialized]
	public Vector3 groundNormal = Vector3.zero;
	
	Vector3 lastGroundNormal = Vector3.zero;
	Transform tr;
	CharacterController controller;

	PlayerDamage pd; //Apply fall damage to our player (Multiplayer only)

	float playerYSpeed; //Keep track of vertical speed
	float previousPositionY;
	bool previousGrounded = false;
	float timeInAir;

	void Awake () {
		controller = gameObject.GetComponent<CharacterController>();
		standartHeight = controller.height;
		/*if(GameObject.FindWithTag("LookObject") != null){
			lookObj = GameObject.FindWithTag("LookObject");
		}*/
		centerY = controller.center.y;
		tr = transform;
		mainCamera = Camera.main.gameObject;
		canRun = true;
		canStand = true;
		StartCoroutine(setupBools());

		if(gameObject.GetComponent<PlayerDamage>() != null){
			pd = gameObject.GetComponent<PlayerDamage>();
		}
	}

	IEnumerator setupBools(){
		if(!movement.canProne){
			movement.canProne = true;
			yield return new WaitForSeconds(0.2f);
			movement.canProne = false;
		}
	}
	
	void UpdateFunction () {
		// We copy the actual velocity into a temporary variable that we can manipulate.
		Vector3 velocity = movement.velocity;
		
		// Update velocity based on input
		velocity = ApplyInputVelocityChange(velocity);
		
		// Apply gravity and jumping force
		if(movement.enableGravity){
			if((prone || crouch) && inputJump)
				return;
			velocity = ApplyGravityAndJumping (velocity);
		}
		// Moving platform support
		Vector3 moveDistance = Vector3.zero;
		if (MoveWithPlatform()) {
			Vector3 newGlobalPoint = movingPlatform.activePlatform.TransformPoint(movingPlatform.activeLocalPoint);
			moveDistance = (newGlobalPoint - movingPlatform.activeGlobalPoint);
			if (moveDistance != Vector3.zero)
				controller.Move(moveDistance);
			
			// Support moving platform rotation as well:
			Quaternion newGlobalRotation = movingPlatform.activePlatform.rotation * movingPlatform.activeLocalRotation;
			Quaternion rotationDiff = newGlobalRotation * Quaternion.Inverse(movingPlatform.activeGlobalRotation);
			
			float yRotation = rotationDiff.eulerAngles.y;
			if (yRotation != 0) {
				// Prevent rotation of the local up vector
				tr.Rotate(0, yRotation, 0);
			}
		}
		
		// Save lastPosition for velocity calculation.
		Vector3 lastPosition = tr.position;
		
		// We always want the movement to be framerate independent.  Multiplying by Time.deltaTime does this.
		Vector3 currentMovementOffset = velocity * Time.deltaTime;
		
		// Find out how much we need to push towards the ground to avoid loosing grouning
		// when walking down a step or over a sharp change in slope.
		float pushDownOffset = Mathf.Max(controller.stepOffset, new Vector3(currentMovementOffset.x, 0, currentMovementOffset.z).magnitude);
		if (grounded)
			currentMovementOffset -= pushDownOffset * Vector3.up;
		
		// Reset variables that will be set by collision function
		movingPlatform.hitPlatform = null;
		groundNormal = Vector3.zero;
		
		// Move our character!
		movement.collisionFlags = controller.Move (currentMovementOffset);
		
		movement.lastHitPoint = movement.hitPoint;
		lastGroundNormal = groundNormal;
		
		if (movingPlatform.enabled && movingPlatform.activePlatform != movingPlatform.hitPlatform) {
			if (movingPlatform.hitPlatform != null) {
				movingPlatform.activePlatform = movingPlatform.hitPlatform;
				movingPlatform.lastMatrix = movingPlatform.hitPlatform.localToWorldMatrix;
				movingPlatform.newPlatform = true;
			}
		}
		
		// Calculate the velocity based on the current and previous position.  
		// This means our velocity will only be the amount the character actually moved as a result of collisions.
		Vector3 oldHVelocity = new Vector3(velocity.x, 0, velocity.z);
		movement.velocity = (tr.position - lastPosition) / Time.deltaTime;
		Vector3 newHVelocity = new Vector3(movement.velocity.x, 0, movement.velocity.z);
		
		// The CharacterController can be moved in unwanted directions when colliding with things.
		// We want to prevent this from influencing the recorded velocity.
		if (oldHVelocity == Vector3.zero) {
			movement.velocity = new Vector3(0, movement.velocity.y, 0);
		}
		else {
			float projectedNewVelocity = Vector3.Dot(newHVelocity, oldHVelocity) / oldHVelocity.sqrMagnitude;
			movement.velocity = oldHVelocity * Mathf.Clamp01(projectedNewVelocity) + movement.velocity.y * Vector3.up;
		}
		
		if (movement.velocity.y < velocity.y - 0.001f) {
			if (movement.velocity.y < 0) {
				// Something is forcing the CharacterController down faster than it should.
				// Ignore this
				movement.velocity = new Vector3(movement.velocity.x, velocity.y, movement.velocity.z);
			}
			else {
				// The upwards movement of the CharacterController has been blocked.
				// This is treated like a ceiling collision - stop further jumping here.
				jumping.holdingJumpButton = false;
			}
		}
		
		// We were grounded but just loosed grounding
		if (grounded && !IsGroundedTest()) {
			grounded = false;
			
			// Apply inertia from platform
			if (movingPlatform.enabled &&
			    (movingPlatform.movementTransfer == FPSMovementTransferOnJump.InitTransfer ||
			 movingPlatform.movementTransfer == FPSMovementTransferOnJump.PermaTransfer)
			    ) {
				movement.frameVelocity = movingPlatform.platformVelocity;
				movement.velocity += movingPlatform.platformVelocity;
			}
			
			SendMessage("OnFall", SendMessageOptions.DontRequireReceiver);
			// We pushed the character down to ensure it would stay on the ground if there was any.
			// But there wasn't so now we cancel the downwards offset to make the fall smoother.
			tr.position += pushDownOffset * Vector3.up;
		}
		// We were not grounded but just landed on something
		else if (!grounded && IsGroundedTest()) {
			grounded = true;
			jumping.jumping = false;
			SubtractNewPlatformVelocity();
			
			SendMessage("OnLand", SendMessageOptions.DontRequireReceiver);
		}
		
		// Moving platforms support
		if (MoveWithPlatform()) {
			// Use the center of the lower half sphere of the capsule as reference point.
			// This works best when the character is standing on moving tilting platforms. 
			movingPlatform.activeGlobalPoint = tr.position + Vector3.up * (controller.center.y - controller.height * 0.5f + controller.radius);
			movingPlatform.activeLocalPoint = movingPlatform.activePlatform.InverseTransformPoint(movingPlatform.activeGlobalPoint);
			
			// Support moving platform rotation as well:
			movingPlatform.activeGlobalRotation = tr.rotation;
			movingPlatform.activeLocalRotation = Quaternion.Inverse(movingPlatform.activePlatform.rotation) * movingPlatform.activeGlobalRotation; 
		}
	}
	
	void FixedUpdate () {
		if (movingPlatform.enabled) {
			if (movingPlatform.activePlatform != null) {
				if (!movingPlatform.newPlatform) {
					//Vector3 lastVelocity = movingPlatform.platformVelocity;
					
					movingPlatform.platformVelocity = (
						movingPlatform.activePlatform.localToWorldMatrix.MultiplyPoint3x4(movingPlatform.activeLocalPoint)
						- movingPlatform.lastMatrix.MultiplyPoint3x4(movingPlatform.activeLocalPoint)
						) / Time.deltaTime;
				}
				movingPlatform.lastMatrix = movingPlatform.activePlatform.localToWorldMatrix;
				movingPlatform.newPlatform = false;
			}
			else {
				movingPlatform.platformVelocity = Vector3.zero;	
			}
		}
		
		if (useFixedUpdate){
			UpdateFunction();
		}

		//Keep track of player Y speed (needed for fall damage)
		playerYSpeed = (previousPositionY - transform.position.y) / Time.deltaTime;

		if(!grounded && playerYSpeed > 0){
			timeInAir += Time.deltaTime;
		}

		if(onLadder){
			timeInAir = 0;
		}

		previousPositionY = transform.position.y;

		//Fall Damage
		if(pd){
			if(previousGrounded != grounded){
				if(grounded){
					if(timeInAir > fallDamage.fallLimit){
						pd.ApplyFallDamage(playerYSpeed * fallDamage.fallDamageMultiplier);
					}
					timeInAir = 0;
				}
				previousGrounded = grounded;
			}
		}
	}
	
	void Update () {
		if (!useFixedUpdate){
			UpdateFunction();
		}
		
		//Run input
		if(Input.GetAxis("Vertical") > 0.1f && inputRun && canRun && !onLadder && Walking){
			if(canStand && canStandCrouch){
				OnRunning();
			}
		}else{
			OffRunning();
		}	
		
		//Check when walk or not
		if ((movement.velocity.x  > 0.01f || movement.velocity.z  > 0.01f) || (movement.velocity.x < -0.01f || movement.velocity.z < -0.01f)) {
			Walking = true;
		}else{
			Walking = false;
		}
		
		if(!canControl)
			return;
		
		if(movement.canCrouch){
			if(!onLadder){	
				Crouch();
			}
		}
		
		if(movement.canProne){
			if(!onLadder){	
				Prone();
			}
		}
		
		if(onLadder){
			grounded = false;
			crouch = false;
			prone = false;
		}
		
		if(!crouch && !prone && controller.height < standartHeight - 0.01f){
			controller.height = Mathf.Lerp(controller.height, standartHeight, Time.deltaTime/movement.crouchSmooth);
			controller.center = new Vector3(controller.center.x, Mathf.Lerp(controller.center.y, centerY, Time.deltaTime/movement.crouchSmooth), controller.center.z);
			lookObj.transform.localPosition = new Vector3(lookObj.transform.localPosition.x, Mathf.Lerp(lookObj.transform.localPosition.y, standartHeight, Time.deltaTime/movement.crouchSmooth), lookObj.transform.localPosition.z);
		}
	}
	
	void Prone(){
		//Vector3 up = transform.TransformDirection(Vector3.up);
		RaycastHit hit;    
		CharacterController charCtrl = controller;
		Vector3 p1 = transform.position;
		//Input control
		if(inputProne && !Running && !onLadder && (canStand || crouch)){
			crouch = false;
			prone = !prone;
			if(!prone){
				crouch = true;	
			}else{
				canStandCrouch = true;
			}
			if(canStandCrouch){
				crouch = false;
			}
		}
		
		if(inputJump && prone){
			if(canStand){
				prone = false;
				crouch = true;
				if(canStandCrouch){
					crouch = false;
				}else{
					crouch = true;
				}
			}
		}
		
		if(prone || Running){
			if (!Physics.SphereCast (p1, charCtrl.radius, transform.up, out hit, movement.crouchHeight * 0.9f)) {
				if(Running && prone){
					prone = false;
					if(!prone){
						crouch = true;	
					}
					if(canStandCrouch){
						crouch = false;
					}
				}
				if(prone){
					canStand = true;
				}
			}else{
				if(prone){
					canStand = false;
				}
				//print("We hit something");
			}
		}
		
		if(prone && !crouch){
			if(controller.height > movement.proneHeight+0.01f){
				controller.height = Mathf.Lerp(controller.height, movement.proneHeight, Time.deltaTime/movement.crouchSmooth);
				controller.center = new Vector3(controller.center.x, Mathf.Lerp(controller.center.y, movement.proneHeight/2, Time.deltaTime/movement.crouchSmooth), controller.center.z);
				lookObj.transform.localPosition = new Vector3(lookObj.transform.localPosition.x, Mathf.Lerp(lookObj.transform.localPosition.y, movement.proneHeight, Time.deltaTime/movement.crouchSmooth), lookObj.transform.localPosition.z);
				movement.maxForwardSpeed = movement.ProneSpeed;
				movement.maxSidewaysSpeed = movement.ProneSpeed;
				movement.maxBackwardsSpeed = movement.ProneSpeed;
			}
		}
	}
	
	void Crouch(){
		//Vector3 up = transform.TransformDirection(Vector3.up);
		RaycastHit hit;    
		CharacterController charCtrl = controller;
		Vector3 p1 = transform.position;
		//Input control
		if(inputCrouch && ! Running && !onLadder && canStand){
			prone = false;
			crouch = !crouch;
		}
		
		//if(crouch){
		if (!Physics.SphereCast (p1, charCtrl.radius, transform.up, out hit, standartHeight)) {
			if(inputJump && crouch){
				crouch = false;
			}
			if(Running && crouch){
				crouch = false;
			}
			if(crouch){
				canStand = true;
			}
			canStandCrouch = true;
		}else{
			if(crouch){
				canStand = false;
			}
			canStandCrouch = false;
			//print("We hit something");
		}
		//}
		
		if(crouch && !prone){
			if(controller.height < movement.crouchHeight + 0.01f && controller.height > movement.crouchHeight - 0.01f)
				return;
			controller.height = Mathf.Lerp(controller.height, movement.crouchHeight, Time.deltaTime/movement.crouchSmooth);
			controller.center = new Vector3(controller.center.x, Mathf.Lerp(controller.center.y, movement.crouchHeight/2, Time.deltaTime/movement.crouchSmooth), controller.center.z);
			lookObj.transform.localPosition = new Vector3(lookObj.transform.localPosition.x, Mathf.Lerp(lookObj.transform.localPosition.y, movement.crouchHeight, Time.deltaTime/movement.crouchSmooth), lookObj.transform.localPosition.z);
			movement.maxForwardSpeed = movement.CrouchSpeed;
			movement.maxSidewaysSpeed = movement.CrouchSpeed;
			movement.maxBackwardsSpeed = movement.CrouchSpeed;
		}
	}
	
	void OnRunning (){
		Running = true;
		movement.maxForwardSpeed = movement.RunSpeed;
		movement.maxSidewaysSpeed = movement.RunSpeed;
		//Make bigger extra height when player run to increase jump distance
		jumping.extraHeight = jumping.baseHeight + 0.15f;
	}
	
	void OffRunning (){
		Running = false;
		if(crouch || prone)
			return;
		movement.maxForwardSpeed = movement.WalkSpeed;
		movement.maxSidewaysSpeed = movement.WalkSpeed;
		movement.maxBackwardsSpeed = movement.WalkSpeed/2;
		//Change extraheight value to default when player walk
		jumping.extraHeight = jumping.baseHeight;
	}
	
	//Ladder functions
	void OnLadder () {
		onLadder = true;
		//moveDirection = Vector3.zero;
		inputMoveDirection = Vector3.zero;
		//Disable gravity when climb on ladder
		movement.enableGravity = false;
	}
	
	void OffLadder (/*ladderMovement*/) {
		onLadder = false;
		// perform off-ladder hop
		Vector3 hop = mainCamera.transform.forward;
		hop = transform.TransformDirection(hop);
		//Enable gravity when climb off ladder
		movement.enableGravity = true;
	}
	
	Vector3 ApplyInputVelocityChange (Vector3 velocity) {	
		if (!canControl)
			inputMoveDirection = Vector3.zero;
		
		// Find desired velocity
		Vector3 desiredVelocity;
		if (grounded && TooSteep()) {
			// The direction we're sliding in
			desiredVelocity = new Vector3(groundNormal.x, 0, groundNormal.z).normalized;
			// Find the input movement direction projected onto the sliding direction
			Vector3 projectedMoveDir = Vector3.Project(inputMoveDirection, desiredVelocity);
			// Add the sliding direction, the spped control, and the sideways control vectors
			desiredVelocity = desiredVelocity + projectedMoveDir * sliding.speedControl + (inputMoveDirection - projectedMoveDir) * sliding.sidewaysControl;
			// Multiply with the sliding speed
			desiredVelocity *= sliding.slidingSpeed;
		}
		else
			desiredVelocity = GetDesiredHorizontalVelocity();
		
		if (movingPlatform.enabled && movingPlatform.movementTransfer == FPSMovementTransferOnJump.PermaTransfer) {
			desiredVelocity += movement.frameVelocity;
			desiredVelocity = new Vector3(desiredVelocity.x, 0, desiredVelocity.z);
		}
		
		if (grounded)
			desiredVelocity = AdjustGroundVelocityToNormal(desiredVelocity, groundNormal);
		else
			velocity = new Vector3(velocity.x, 0, velocity.z);
		
		// Enforce max velocity change
		float maxVelocityChange = GetMaxAcceleration(grounded) * Time.deltaTime;
		Vector3 velocityChangeVector = (desiredVelocity - velocity);
		if (velocityChangeVector.sqrMagnitude > maxVelocityChange * maxVelocityChange) {
			velocityChangeVector = velocityChangeVector.normalized * maxVelocityChange;
		}
		// If we're in the air and don't have control, don't apply any velocity change at all.
		// If we're on the ground and don't have control we do apply it - it will correspond to friction.
		if (grounded || canControl)
			velocity += velocityChangeVector;
		
		if (grounded) {
			// When going uphill, the CharacterController will automatically move up by the needed amount.
			// Not moving it upwards manually prevent risk of lifting off from the ground.
			// When going downhill, DO move down manually, as gravity is not enough on steep hills.
			velocity = new Vector3(velocity.x, Mathf.Min(velocity.y, 0), velocity.z);
		}
		
		return velocity;
	}
	
	Vector3 ApplyGravityAndJumping (Vector3 velocity) {
		
		if (!inputJump || !canControl) {
			jumping.holdingJumpButton = false;
			jumping.lastButtonDownTime = -100;
		}
		
		if (inputJump && jumping.lastButtonDownTime < 0 && canControl)
			jumping.lastButtonDownTime = Time.time;
		
		if (grounded)
			velocity = new Vector3(velocity.x, Mathf.Min(0, velocity.y) - movement.gravity * Time.deltaTime, velocity.z);
		else {
			velocity = new Vector3(velocity.x, movement.velocity.y - movement.gravity * Time.deltaTime, velocity.z);
			
			// When jumping up we don't apply gravity for some time when the user is holding the jump button.
			// This gives more control over jump height by pressing the button longer.
			if (jumping.jumping && jumping.holdingJumpButton) {
				// Calculate the duration that the extra jump force should have effect.
				// If we're still less than that duration after the jumping time, apply the force.
				if (Time.time < jumping.lastStartTime + jumping.extraHeight / CalculateJumpVerticalSpeed(jumping.baseHeight)) {
					// Negate the gravity we just applied, except we push in jumpDir rather than jump upwards.
					velocity += jumping.jumpDir * movement.gravity * Time.deltaTime;
				}
			}
			
			// Make sure we don't fall any faster than maxFallSpeed. This gives our character a terminal velocity.
			velocity = new Vector3(velocity.x, Mathf.Max (velocity.y, -movement.maxFallSpeed), velocity.z);
		}
		
		if (grounded) {
			// Jump only if the jump button was pressed down in the last 0.2 seconds.
			// We use this check instead of checking if it's pressed down right now
			// because players will often try to jump in the exact moment when hitting the ground after a jump
			// and if they hit the button a fraction of a second too soon and no new jump happens as a consequence,
			// it's confusing and it feels like the game is buggy.
			if (jumping.enabled && canControl && (Time.time - jumping.lastButtonDownTime < 0.2f)) {
				grounded = false;
				jumping.jumping = true;
				jumping.lastStartTime = Time.time;
				jumping.lastButtonDownTime = -100;
				jumping.holdingJumpButton = true;
				
				// Calculate the jumping direction
				if (TooSteep())
					jumping.jumpDir = Vector3.Slerp(Vector3.up, groundNormal, jumping.steepPerpAmount);
				else
					jumping.jumpDir = Vector3.Slerp(Vector3.up, groundNormal, jumping.perpAmount);
				
				// Apply the jumping force to the velocity. Cancel any vertical velocity first.
				velocity.y = 0;
				velocity += jumping.jumpDir * CalculateJumpVerticalSpeed (jumping.baseHeight);
				
				// Apply inertia from platform
				if (movingPlatform.enabled &&
				    (movingPlatform.movementTransfer == FPSMovementTransferOnJump.InitTransfer ||
				 movingPlatform.movementTransfer == FPSMovementTransferOnJump.PermaTransfer)
				    ) {
					movement.frameVelocity = movingPlatform.platformVelocity;
					velocity += movingPlatform.platformVelocity;
				}
				
				SendMessage("OnJump", SendMessageOptions.DontRequireReceiver);
			}
			else {
				jumping.holdingJumpButton = false;
			}
		}
		
		return velocity;
	}
	
	void OnControllerColliderHit (ControllerColliderHit hit) {
		if (hit.normal.y > 0 && hit.normal.y > groundNormal.y && hit.moveDirection.y < 0) {
			if ((hit.point - movement.lastHitPoint).sqrMagnitude > 0.001f || lastGroundNormal == Vector3.zero)
				groundNormal = hit.normal;
			else
				groundNormal = lastGroundNormal;
			
			movingPlatform.hitPlatform = hit.collider.transform;
			movement.hitPoint = hit.point;
			movement.frameVelocity = Vector3.zero;
		}
		
		//Did we push something?
		if(pushing.canPush && hit.transform.GetComponent<Rigidbody>() != null){
			Rigidbody body = hit.transform.rigidbody;
			// no rigidbody
			if (body == null || body.isKinematic)
				return;
			
			// We dont want to push objects below us
			if (hit.moveDirection.y < -0.3) 
				return;
			
			// Calculate push direction from move direction, 
			// we only push objects to the sides never up and down
			Vector3 pushDir = new Vector3 (hit.moveDirection.x, 0, hit.moveDirection.z);
			
			// If you know how fast your character is trying to move,
			// then you can also multiply the push velocity by that.
			
			// Apply the push
			body.velocity = pushDir * pushing.pushPower;
		}
	}
	
	void SubtractNewPlatformVelocity () {
		// When landing, subtract the velocity of the new ground from the character's velocity
		// since movement in ground is relative to the movement of the ground.
		if (movingPlatform.enabled &&
		    (movingPlatform.movementTransfer == FPSMovementTransferOnJump.InitTransfer ||
		 movingPlatform.movementTransfer == FPSMovementTransferOnJump.PermaTransfer)
		    ) {
			// If we landed on a new platform, we have to wait for two FixedUpdates
			// before we know the velocity of the platform under the character
			if (movingPlatform.newPlatform) {
				Transform platform = movingPlatform.activePlatform;
				//return WaitForFixedUpdate();
				//return WaitForFixedUpdate();
				if (grounded && platform == movingPlatform.activePlatform)
					return;
			}
			movement.velocity -= movingPlatform.platformVelocity;
		}
	}
	
	bool MoveWithPlatform () {
		return (
			movingPlatform.enabled
			&& (grounded || movingPlatform.movementTransfer == FPSMovementTransferOnJump.PermaLocked)
			&& movingPlatform.activePlatform != null
			);
	}
	
	Vector3 GetDesiredHorizontalVelocity () {
		// Find desired velocity
		Vector3 desiredLocalDirection = tr.InverseTransformDirection(inputMoveDirection);
		float maxSpeed = MaxSpeedInDirection(desiredLocalDirection);
		if (grounded) {
			// Modify max speed on slopes based on slope speed multiplier curve
			var movementSlopeAngle = Mathf.Asin(movement.velocity.normalized.y)  * Mathf.Rad2Deg;
			maxSpeed *= movement.slopeSpeedMultiplier.Evaluate(movementSlopeAngle);
		}
		return tr.TransformDirection(desiredLocalDirection * maxSpeed);
	}
	
	Vector3 AdjustGroundVelocityToNormal (Vector3 hVelocity, Vector3 groundNormal) {
		Vector3 sideways = Vector3.Cross(Vector3.up, hVelocity);
		return Vector3.Cross(sideways, groundNormal).normalized * hVelocity.magnitude;
	}
	
	bool IsGroundedTest () {
		return (groundNormal.y > 0.01f);
	}
	
	float GetMaxAcceleration (bool grounded){
		// Maximum acceleration on ground and in air
		if (grounded)
			return movement.maxGroundAcceleration;
		else
			return movement.maxAirAcceleration;
	}
	
	float CalculateJumpVerticalSpeed (float targetJumpHeight) {
		// From the jump height and gravity we deduce the upwards speed 
		// for the character to reach at the apex.
		return Mathf.Sqrt (2 * targetJumpHeight * movement.gravity);
	}
	
	bool IsJumping () {
		return jumping.jumping;
	}
	
	bool IsSliding () {
		return (grounded && sliding.enabled && TooSteep());
	}
	
	bool IsTouchingCeiling () {
		return (movement.collisionFlags & CollisionFlags.CollidedAbove) != 0;
	}
	
	bool IsGrounded () {
		return grounded;
	}
	
	bool TooSteep () {
		return (groundNormal.y <= Mathf.Cos(controller.slopeLimit * Mathf.Deg2Rad));
	}
	
	Vector3 GetDirection () {
		return inputMoveDirection;
	}
	
	void SetControllable (bool controllable) {
		canControl = controllable;
	}
	
	// Project a direction onto elliptical quater segments based on forward, sideways, and backwards speed.
	// The function returns the length of the resulting vector.
	float MaxSpeedInDirection (Vector3 desiredMovementDirection) {
		if (desiredMovementDirection == Vector3.zero)
			return 0;
		else {
			float zAxisEllipseMultiplier = (desiredMovementDirection.z > 0 ? movement.maxForwardSpeed : movement.maxBackwardsSpeed) / movement.maxSidewaysSpeed;
			Vector3 temp = new Vector3(desiredMovementDirection.x, 0, desiredMovementDirection.z / zAxisEllipseMultiplier).normalized;
			float length = new Vector3(temp.x, 0, temp.z * zAxisEllipseMultiplier).magnitude * movement.maxSidewaysSpeed;
			return length;
		}
	}
	
	void SetVelocity (Vector3 velocity) {
		grounded = false;
		movement.velocity = velocity;
		movement.frameVelocity = Vector3.zero;
		SendMessage("OnExternalVelocity");
	}
}
