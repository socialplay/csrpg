using UnityEngine;
using System.Collections;
using SocialPlay.ItemSystems;
using System.Collections.Generic;

public class ConfigurableNGUIItemTooltip : MonoBehaviour, ITooltipSetup
{
    public List<DisplayOption> displayOptions = new List<DisplayOption>();
    public bool isQualityColorUsed;

    public enum DisplayOption
    {
        name, stats, quantity, description, salePrice, behaviour, behaviourPlus, space, varianceID, itemID, energy, classID, stackID
    }

    ItemData item;

    public string Setup()
    {
        item = GetComponent<ItemData>();
        string formated = "";


        foreach (DisplayOption selectedOption in displayOptions)
        {
            switch (selectedOption)
            {
                case DisplayOption.name:
                    if (isQualityColorUsed)
                    {
                        formated += "[" + NGUITools.EncodeColor(ItemQuailityColorSelector.GetColorForItem(item)) + "]";
                    }
                    else
                    {
                        formated += "[" + NGUITools.EncodeColor(Color.white) + "]";
                    }

                    Debug.Log("formatted: " + formated + "  item name: " + item.itemName);

                    formated += item.itemName;
                    break;
                case DisplayOption.stats:
                    foreach (KeyValuePair<string, float> pair in item.stats)
                    {
                        if (pair.Key == "Not Available") continue;

                        formated = string.Format("{0}\n[" + NGUITools.EncodeColor(Color.white) + "]{1}: {2}", formated, pair.Key, pair.Value);
                    }
                    break;
                case DisplayOption.quantity:
                    formated = string.Format("{0}\n[" + NGUITools.EncodeColor(Color.white) + "]{1}", formated, item.stackSize);
                    break;
                case DisplayOption.description:
                    if (!string.IsNullOrEmpty(item.description))
                    {
                        formated = string.Format("{0}\n[" + NGUITools.EncodeColor(Color.white) + "]{1}", formated, item.description);
                    }
                    break;

                case DisplayOption.salePrice:
                    formated = string.Format("{0}\n[" + NGUITools.EncodeColor(Color.white) + "]{1}", formated, item.salePrice);
                    break;
                case DisplayOption.behaviour:
                    foreach (BehaviourDefinition behaviour in item.behaviours)
                    {
                        formated = string.Format("{0}\n[" + NGUITools.EncodeColor(Color.white) + "]{1}", formated, behaviour.Name);
                    }

                    break;
                case DisplayOption.behaviourPlus:
                    foreach (BehaviourDefinition behaviour in item.behaviours)
                    {
                        formated = string.Format("{0}\n[" + NGUITools.EncodeColor(Color.white) + "]{1}: {2}", formated, behaviour.Name, behaviour.Description);
                    }
                    break;
                case DisplayOption.space:
                    formated = string.Format("{0}\n", formated);
                    break;
                case DisplayOption.varianceID:
                    formated = string.Format("{0}\n[" + NGUITools.EncodeColor(Color.white) + "]{1}", formated, item.varianceID);
                    break;
                case DisplayOption.itemID:
                    formated = string.Format("{0}\n[" + NGUITools.EncodeColor(Color.white) + "]{1}", formated, item.itemID);
                    break;
                case DisplayOption.classID:
                    formated = string.Format("{0}\n[" + NGUITools.EncodeColor(Color.white) + "]{1}", formated, item.classID);
                    break;
                case DisplayOption.energy:
                    formated = string.Format("{0}\n[" + NGUITools.EncodeColor(Color.white) + "]{1}", formated, item.totalEnergy);
                    break;
                case DisplayOption.stackID:
                    formated = string.Format("{0}\n[" + NGUITools.EncodeColor(Color.white) + "]{1}", formated, item.stackID.ToString());
                    break;

                default: break;
            }
        }
        return formated;
    }
}


