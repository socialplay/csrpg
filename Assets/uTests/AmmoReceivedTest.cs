﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AmmoReceivedTest : MonoBehaviour
{
    public List<string> ammoTypes = new List<string>();

    public GameObject prefabReceivedAmmoDisplay;

    public Transform displayAnchor;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            int ammoAmount = Random.Range(1, 99);
            int randNumber = Random.Range(0, ammoTypes.Count);
            string ammoType = ammoTypes[randNumber];

            GameObject ammoUI = (GameObject)Instantiate(prefabReceivedAmmoDisplay);
            ammoUI.name = ammoType + " Ammo";
            ammoUI.transform.parent = displayAnchor;
            ammoUI.GetComponent<DisplayReceivedAmmo>().SetDisplayedAmmoInfo(ammoType, ammoAmount);
        }
    }
}
