﻿using UnityEngine;
using System.Collections;

public class SpawnTestPlayer : MonoBehaviour
{
    public GameObject Player;

    internal GameObject spawnedPlayer;

    void Start()
    {
        spawnedPlayer = (GameObject)Instantiate(Player, transform.position, transform.rotation);

        spawnedPlayer.transform.parent = gameObject.transform.parent;
    }
}
