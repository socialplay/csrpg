﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Hashtable = ExitGames.Client.Photon.Hashtable;//Replace default Hashtables with Photon hashtables

public class TestRoomInit : MonoBehaviour
{
    public KeyCode ammDropKey;
    public AmmoDropper ammoDropper;
    public GameObject testPlayerPrefab;

    AutoTimer dropTimer;
    GameObject player;

    bool runTimer = true;

    // Use this for initialization
    void Start()
    {
        PhotonNetwork.offlineMode = true;
        PhotonNetwork.CreateRoom("Test Room");

        player = PhotonNetwork.Instantiate(testPlayerPrefab.name, transform.position, transform.rotation, 0);

        dropTimer = new AutoTimer(1);
    }

    // Update is called once per frame
    void Update()
    {
        if (dropTimer.IsDone() && runTimer)
        {
            runTimer = false;
            Debug.Log("drop ammo ready!");
        }

        if (!runTimer)
        {
            dropTimer.Reset();
            runTimer = true;
            //drop ammo
            Vector3 dropPoint = new Vector3(Random.Range(-0.5f, 0.5f), 0, Random.Range(-0.5f, 0.5f));

            ammoDropper.InstantiateAmmoItemDrop(dropPoint);

            GameObject.FindGameObjectWithTag("ItemDrop").GetComponent<AmmoPickup>().reachDistance = 10;
        }
    }
}
