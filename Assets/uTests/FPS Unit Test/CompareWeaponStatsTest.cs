﻿using UnityEngine;
using System.Collections;

public class CompareWeaponStatsTest : WeaponSlotTests
{
    public override void Initialize()
    {
        setFireRateOrDelay();
        testFireRateOrDelay();

        StartCoroutine(ShootWeapon());
    }

    void setFireRateOrDelay()
    {
        switch (weaponSlot.weaponMan.SelectedWeapon.GunType)
        {
            case WeaponScript.gunType.MACHINE_GUN:
                weaponSlot.weaponMan.SelectedWeapon.machineGun.fireRate = weaponSlot.itemFireRateDelay;
                break;
            case WeaponScript.gunType.GRENADE_LAUNCHER:
                weaponSlot.weaponMan.SelectedWeapon.grenadeLauncher.shotDelay = weaponSlot.itemFireRateDelay;
                break;
            case WeaponScript.gunType.SHOTGUN:
                weaponSlot.weaponMan.SelectedWeapon.ShotGun.fireRate = weaponSlot.itemFireRateDelay;
                break;
        }
    }

    void testFireRateOrDelay()
    {
        string failMessage = string.Empty;

        switch (weaponSlot.weaponMan.SelectedWeapon.GunType)
        {
            case WeaponScript.gunType.MACHINE_GUN:
                failMessage = "COMPARE FIRE RATE FAILED;\nshould be: " + weaponSlot.itemFireRateDelay + ", was: " + weaponSlot.weaponMan.SelectedWeapon.machineGun.fireRate;

                if (!float.Equals(weaponSlot.itemFireRateDelay, weaponSlot.weaponMan.SelectedWeapon.machineGun.fireRate))
                {
                    IntegrationTest.Fail(weaponSlot.weaponMan.SelectedWeapon.gameObject, failMessage);
                }
                break;
            case WeaponScript.gunType.GRENADE_LAUNCHER:
                failMessage = "COMPARE FIRE DELAY FAILED;\nshould be: " + weaponSlot.itemFireRateDelay + ", was: " + weaponSlot.weaponMan.SelectedWeapon.grenadeLauncher.shotDelay;

                if (!float.Equals(weaponSlot.itemFireRateDelay, weaponSlot.weaponMan.SelectedWeapon.grenadeLauncher.shotDelay))
                {
                    IntegrationTest.Fail(weaponSlot.weaponMan.SelectedWeapon.gameObject, failMessage);
                }
                break;
            case WeaponScript.gunType.SHOTGUN:
                failMessage = "COMPARE FIRE RATE FAILED;\nshould be: " + weaponSlot.itemFireRateDelay + ", was: " + weaponSlot.weaponMan.SelectedWeapon.ShotGun.fireRate;

                if (!float.Equals(weaponSlot.itemFireRateDelay, weaponSlot.weaponMan.SelectedWeapon.ShotGun.fireRate))
                {
                    IntegrationTest.Fail(weaponSlot.weaponMan.SelectedWeapon.gameObject, failMessage);
                }
                break;
        }
    }

    IEnumerator ShootWeapon()
    {
        yield return new WaitForSeconds(1);

        switch (weaponSlot.weaponMan.SelectedWeapon.GunType)
        {
            case WeaponScript.gunType.MACHINE_GUN:
                weaponSlot.weaponMan.SelectedWeapon.machineGunFire();
                break;
            case WeaponScript.gunType.GRENADE_LAUNCHER:
                weaponSlot.weaponMan.SelectedWeapon.grenadeLauncherFIre();
                break;
            case WeaponScript.gunType.SHOTGUN:
                weaponSlot.weaponMan.SelectedWeapon.shotGunFire();
                break;
        }
    }
}
