﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(SpawnTestPlayer))]
public class SetupWeaponSlot : MonoBehaviour
{
    public ItemContainer equipment;
    public ItemContainer inventory;
    public ItemInsertTest itemInsertTest;
    public WeaponSlotTests weaponSlotTests;

    public int slotID = 0;

    public enum WeaponType { machinegun, explosive };
    public WeaponType weaponType = WeaponType.machinegun;
    public string itemName = "Name of the item.";
    public int itemId = 0;
    public float itemDamage = 10;
    public float itemFireRateDelay = 1;

    internal SpawnTestPlayer refPlayer;
    internal SlottedContainerSlotData equipmentSlot;
    internal WeaponManager weaponMan;

    internal List<ItemData> items = new List<ItemData>();

    void Start()
    {
        equipment.Clear(); //testing script
        inventory.Clear(); //testing script

        refPlayer = GetComponent<SpawnTestPlayer>();
    }

    void OnEnable()
    {
        PlayerData.eventPlayerSpawned += PlayerData_eventPlayerSpawned;
    }
    void OnDisable()
    {
        PlayerData.eventPlayerSpawned -= PlayerData_eventPlayerSpawned;
    }

    void PlayerData_eventPlayerSpawned(PlayerData pd)
    {
        weaponMan = PlayerData.weaponManager;
        equipmentSlot = equipment.GetComponent<SlottedItemContainer>().slots[slotID];

        string jsonItemData = string.Empty;

        if (weaponType == WeaponType.explosive)
        {
            jsonItemData = MockWeaponData.AddExplosiveWeapon(itemName, itemId, itemDamage, itemFireRateDelay);
        }
        else
        {
            jsonItemData = MockWeaponData.AddMachineGunWeapon(itemName, itemId, itemDamage, itemFireRateDelay);
        }

        items = itemInsertTest.AddItemToInventory(jsonItemData);
        items[0].ownerContainer = inventory;

        ItemContainerManager.MoveItem(items[0], equipment);

        weaponMan.index = weaponMan.allWeapons.Count - 1;
        weaponMan.SelectedWeapon = weaponMan.allWeapons[0];
        StartCoroutine(weaponMan.SwitchWeapons(weaponMan.SelectedWeapon.gameObject, weaponMan.allWeapons[weaponMan.allWeapons.Count - 1].gameObject));
        //weaponMan.SelectedWeapon = weaponMan.allWeapons[weaponMan.allWeapons.Count - 1];

        weaponSlotTests.Initialize();
    }
}
