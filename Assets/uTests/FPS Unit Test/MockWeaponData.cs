﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

public class MockWeaponData : MonoBehaviour
{
    /// <summary>
    /// Get a mock item data version of a Machine Gun weapon.
    /// </summary>
    /// <param name="itemName">Name of the item, this will also be used to set the tag.</param>
    /// <param name="itemId">Variance id of the item, this will be used to compare other mock items.</param>
    /// <param name="itemDamage">Damage value of the weapon.</param>
    /// <param name="itemFireRate">Fire rate value of the weapon.</param>
    /// <returns>A json serialized Socialplay ItemData.</returns>
    public static string AddMachineGunWeapon(string itemName, int itemId, float itemDamage, float itemFireRate)
    {
        SocialPlay.Data.ItemData[] itemDataArray = new SocialPlay.Data.ItemData[1];
        SocialPlay.Data.ItemData itemData = new SocialPlay.Data.ItemData();

        itemData.Amount = 1;
        itemData.AssetBundleName = "";
        itemData.BaseItemEnergy = 1040;
        itemData.BaseItemID = 328;
        itemData.Behaviours = "[]";
        itemData.Description = "WARNING: This is mock data.";
        itemData.Detail = "[{\"Name\":\"Damage\",\"Value\":" + itemDamage + ",\"InvertEnergy\":false},{\"Name\":\"Fire Rate\",\"Value\":" + itemFireRate + "}]";
        itemData.Energy = 1118;
        itemData.Image = "https://socialplay.blob.core.windows.net/itemimages/c688360e-8c1f-4606-b7a9-6ce65b09f798.png";
        itemData.ItemID = itemId;
        itemData.Location = 0;
        itemData.Name = itemName;
        itemData.Quality = 1;
        itemData.SellPrice = 447;
        itemData.StackLocationID = System.Guid.Empty;

        string itemTag = itemName.Replace(" ", string.Empty);
        itemData.Tags = "[\"" + itemTag + "\",\"droppable\"]";

        itemDataArray[0] = itemData;

        return JsonConvert.SerializeObject(itemDataArray);
    }

    /// <summary>
    /// Get a mock item data version of a Explosive weapon
    /// </summary>
    /// <param name="itemName">Name of the item, this will also be used to set the tag.</param>
    /// <param name="itemId">Variance id of the item, this will be used to compare other mock items.</param>
    /// <param name="itemDamage">Damage value of the weapon.</param>
    /// <param name="itemFireDelay">Fire rate value of the weapon.</param>
    /// <returns>A json serialized Socialplay ItemData.</returns>
    public static string AddExplosiveWeapon(string itemName, int itemId, float itemDamage, float itemFireDelay)
    {
        SocialPlay.Data.ItemData[] itemDataArray = new SocialPlay.Data.ItemData[1];
        SocialPlay.Data.ItemData itemData = new SocialPlay.Data.ItemData();

        itemData.Amount = 1;
        itemData.AssetBundleName = "";
        itemData.BaseItemEnergy = 1040;
        itemData.BaseItemID = 328;
        itemData.Behaviours = "[]";
        itemData.Description = "WARNING: This is mock data.";
        itemData.Detail = "[{\"Name\":\"Damage\",\"Value\":" + itemDamage + ",\"InvertEnergy\":false},{\"Name\":\"Fire Delay\",\"Value\":" + itemFireDelay + "}]";
        itemData.Energy = 1118;
        itemData.Image = "https://socialplay.blob.core.windows.net/itemimages/c688360e-8c1f-4606-b7a9-6ce65b09f798.png";
        itemData.ItemID = 49225;
        itemData.Location = 0;
        itemData.Name = itemName;
        itemData.Quality = 1;
        itemData.SellPrice = 447;
        itemData.StackLocationID = System.Guid.Empty;

        string itemTag = itemName.Replace(" ", string.Empty);
        itemData.Tags = "[\"" + itemTag + "\",\"droppable\"]";

        itemDataArray[0] = itemData;

        return JsonConvert.SerializeObject(itemDataArray);
    }
}
