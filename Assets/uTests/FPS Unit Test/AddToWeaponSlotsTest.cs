﻿using UnityEngine;
using System.Collections;

public class AddToWeaponSlotsTest : WeaponSlotTests
{
    public override void Initialize()
    {
        weaponSlot.weaponMan.SelectedWeapon = weaponSlot.weaponMan.allWeapons[1];

        weaponIsInSlot();
    }

    void weaponIsInSlot()
    {
        if (weaponSlot.items[0].IsSameItemAs(weaponSlot.equipmentSlot.slotData))
        {
            testForWeaponIsSelectable(weaponSlot.equipmentSlot.slotData);
        }
        else
        {
            string failString = "COMPAREING ITEMDATA;\nshould be " + weaponSlot.items[0] + ", was " + weaponSlot.equipmentSlot.slotData;

            IntegrationTest.Fail(gameObject, failString);
        }
    }

    void testForWeaponIsSelectable(ItemData item)
    {
        if (weaponSlot.weaponMan.allWeapons[1].weaponName == item.itemName)
        {
            testForWeaponIsEquipped();
        }
        else
        {
            string failString = "COMPARING WEAPON OBJECT TO ITEMDATA;\nshould be " + weaponSlot.weaponMan.allWeapons[1].weaponName + ", was " + item.itemName;

            IntegrationTest.Fail(gameObject, failString);
        }
    }

    void testForWeaponIsEquipped()
    {
        if (weaponSlot.weaponMan.SelectedWeapon.Equals(weaponSlot.weaponMan.allWeapons[1]))
        {
            IntegrationTest.Pass(gameObject);
        }
        else
        {
            string failString = "COMPARING EQUIPPED WEAPON;\nshould be " + weaponSlot.weaponMan.SelectedWeapon + ", was " + weaponSlot.weaponMan.allWeapons[1];

            IntegrationTest.Fail(gameObject, failString);
        }
    }
}
