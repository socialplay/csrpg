﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SetupWeaponSlot))]
public abstract class WeaponSlotTests : MonoBehaviour
{
    protected SetupWeaponSlot weaponSlot;

    void Start()
    {
        weaponSlot = GetComponent<SetupWeaponSlot>();
    }

    public abstract void Initialize();
}
