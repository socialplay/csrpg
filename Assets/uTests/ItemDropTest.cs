﻿using UnityEngine;
using System.Collections;

public class ItemDropTest : MonoBehaviour
{
    public LevelPlayer levelPlayer;
    public GameObject prefabItemDropper;
    public Vector3 positionOffset;
    public int expAmount = 100;

    Renderer thisRenderer;
    Color alpha;

    float waitTime = 3;
    bool canTakeDamage = true;

    void Start()
    {
        thisRenderer = GetComponentInChildren<Renderer>();
        alpha = thisRenderer.material.color;
    }

    void Update()
    {
        thisRenderer.material.color = alpha;

        if (canTakeDamage) return;

        if (waitTime <= 0)
        {
            canTakeDamage = true;
            waitTime = 3;
            alpha.a = 1;
        }
        else
        {
            waitTime -= Time.deltaTime;

            if (alpha.a < 1)
            {
                alpha.a += Time.deltaTime;
            }
        }
    }

    void ApplyDamage(float damage)
    {
        if (!canTakeDamage) return;

        canTakeDamage = false;
        alpha.a = 0.25f;

        InitializeItemDropper();
    }

    void InitializeItemDropper()
    {
        levelPlayer.GivePlayerExperience(expAmount);

        GameObject itemDropper = (GameObject)Instantiate(prefabItemDropper, transform.position + positionOffset, transform.rotation);

        itemDropper.GetComponent<ItemGetter>().GetItems();
    }
}
