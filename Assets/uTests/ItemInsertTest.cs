﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemInsertTest : MonoBehaviour
{
    GameObjectItemDataConverter itemDataConverter = new GameObjectItemDataConverter();
    IItemPutter itemPutter;

    void Start()
    {
        itemPutter = GetComponent(typeof(IItemPutter)) as IItemPutter;
    }

    public List<ItemData> AddItemToInventory(string itemData)
    {
        List<ItemData> items = itemDataConverter.convertToItemDataFromString(itemData);

        itemPutter.PutGameItem(items);

        return items;
    }
}
