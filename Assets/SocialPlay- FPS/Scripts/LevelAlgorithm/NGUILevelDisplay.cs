using UnityEngine;
using System.Collections;

public class NGUILevelDisplay : MonoBehaviour
{
    public GameObject levelUpPrefab;
    public GameObject xpGainedPrefab;

    public UILabel levelLabel;
    public UILabel experienceLabel;

    public UISlider experienceSlider;


    void OnEnable()
    {
        LevelPlayer.onPlayerXPGainedEvent += SpawnXpGained;
        LevelPlayer.onPlayerLevelEvent += PlayLevelUpSound;
    }
    void OnDisable()
    {
        LevelPlayer.onPlayerXPGainedEvent -= SpawnXpGained;
        LevelPlayer.onPlayerLevelEvent -= PlayLevelUpSound;
    }

    void SpawnXpGained(int xp)
    {
        GameObject xpGained = (GameObject)Instantiate(xpGainedPrefab);
        xpGained.transform.parent = transform;
        xpGained.GetComponent<NGUIExpDisplay>().SetXPGained(xp);
    }

    void PlayLevelUpSound(int level)
    {
        GameObject objLevelUp = (GameObject)Instantiate(levelUpPrefab);
        objLevelUp.transform.parent = transform;

        if (!audio.isPlaying)
        {
            audio.Play();
        }
    }

    void Update()
    {
        if (LevelPlayer.Instance == null)
            return;

      if(LevelPlayer.IsPlayerMaxLevel()){
          levelLabel.text = "Max";
          experienceLabel.gameObject.SetActive(false);
      }else{
        levelLabel.text = LevelPlayer.Instance.GetPlayerLevel().ToString();
        experienceLabel.gameObject.SetActive(true);
        experienceLabel.text = LevelPlayer.Instance.GetPlayerCurrentExperience() + " / " + LevelPlayer.Instance.GetPlayerRequiredExperience();
      }


        experienceSlider.value = (float)LevelPlayer.Instance.GetPlayerCurrentExperience() / (float)LevelPlayer.Instance.GetPlayerRequiredExperience();
    }
}
