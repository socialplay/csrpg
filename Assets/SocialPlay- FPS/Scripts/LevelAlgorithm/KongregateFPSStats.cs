﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KongregateFPSStats : MonoBehaviour
{
    public SkillsManager skillmanager;


    void OnEnable()
    {
        LevelPlayer.onPlayerLevelEvent += LevelPlayer_onPlayerLevelEvent;
        LevelPlayer.onPlayerXPGainedEvent += LevelPlayer_onPlayerXPGainedEvent;
        ItemPurchase.OnPurchasedItem += ItemPurchase_OnPurchasedItem;

        skillmanager.playerSkills.ForEach(delegate(Skill skill)
         {
             skill.OnLevelUp += skill_OnLevelUp;
         });
    }

    void OnDisable()
    {
        LevelPlayer.onPlayerLevelEvent -= LevelPlayer_onPlayerLevelEvent;
        LevelPlayer.onPlayerXPGainedEvent -= LevelPlayer_onPlayerXPGainedEvent;
        ItemPurchase.OnPurchasedItem -= ItemPurchase_OnPurchasedItem;

        skillmanager.playerSkills.ForEach(delegate(Skill skill)
        {
            skill.OnLevelUp -= skill_OnLevelUp;
        });
    }

    void skill_OnLevelUp(bool isMaxed)
    {
        if (isMaxed)
        {
            KongregateStatsTracker.PostAction("MaxSkill", 1);
        }
    }

    void ItemPurchase_OnPurchasedItem(string item)
    {
        KongregateStatsTracker.PostAction("ItemPurchase", 1);
    }

    void LevelPlayer_onPlayerXPGainedEvent(int value)
    {
        Debug.Log("XP :" + value);
        KongregateStatsTracker.PostAction("XP", value);
    }

    void LevelPlayer_onPlayerLevelEvent(int level)
    {       
        KongregateStatsTracker.PostAction("Level", 1);

        if (LevelPlayer.IsPlayerMaxLevel())
        {
            KongregateStatsTracker.PostAction("MaxLevel", 1);
        }
    }
}
