using UnityEngine;
using System.Collections;
using System;

public class LevelPlayer :Photon. MonoBehaviour
{
    public static LevelPlayer Instance;


    public static event Action<int> onPlayerLevelEvent;
    public static event Action<int> onPlayerXPGainedEvent;
    public ILevelExperiencreAlgorithm levelAlgorithm;

    public int maxPlayerLevel = 12;
    public int playerLevel = 0;
    public int currentExperience = 0;

    public int experienceRequirement = 0;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        levelAlgorithm = new SocialPlayLevelAlgorithm();
        LevelPlayerInit(1);
    }


    public static bool IsPlayerMaxLevel()
    {
        return Instance.playerLevel >= Instance.maxPlayerLevel;
    }

    public void LevelPlayerInit(int level)
    {
        playerLevel = level;
        currentExperience = 0;

        experienceRequirement = levelAlgorithm.CalcuateRequiredExperienceForLevel(playerLevel);
    }

    public int GetPlayerLevel()
    {
        return this.playerLevel;
    }

    public int GetPlayerCurrentExperience()
    {
        return this.currentExperience;
    }

    public int GetPlayerRequiredExperience()
    {
        return this.experienceRequirement;
    }

    public void GivePlayerExperience(int experience)
    {
        if (isMaxPlayerLevel(playerLevel))
            return;

        if (onPlayerXPGainedEvent != null) onPlayerXPGainedEvent(experience);

        if ((experience + currentExperience) >= experienceRequirement)
        {
            OnPlayerLevel(playerLevel += 1, (experience + currentExperience) - experienceRequirement);
        }
        else
        {
            currentExperience += experience;
        }
    }

    private bool isMaxPlayerLevel(int level)
    {
        if (level >= maxPlayerLevel)
        {
            return true;
        }
        return false;
    }

    void OnPlayerLevel(int newLevel, int carryoverExp)
    {
        int xpRequirement = levelAlgorithm.CalcuateRequiredExperienceForLevel(newLevel);
        bool isMaxed = isMaxPlayerLevel(newLevel);
        if (isMaxed)
        {
            newLevel = maxPlayerLevel;
            xpRequirement = levelAlgorithm.CalcuateRequiredExperienceForLevel(newLevel - 1);
            carryoverExp = xpRequirement;
        }

        playerLevel = newLevel;
        experienceRequirement = xpRequirement;
        currentExperience = carryoverExp;

        if (onPlayerLevelEvent != null) onPlayerLevelEvent(newLevel);
        if (!isMaxed && (carryoverExp) >= levelAlgorithm.CalcuateRequiredExperienceForLevel(newLevel + 1))
        {
            OnPlayerLevel(newLevel + 1, carryoverExp - xpRequirement);
        }
    }

    void OnLeftRoom()
    {
        LevelPlayerInit(1);
    }
}
