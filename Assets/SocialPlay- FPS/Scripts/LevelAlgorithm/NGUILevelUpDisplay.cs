﻿using UnityEngine;
using System.Collections;

public class NGUILevelUpDisplay : MonoBehaviour
{
    public GameObject labelLevelUp;
    public GameObject labelSkillUp;
    public float screenYPosition = 540;

    TweenAlpha taLevelUp;
    TweenAlpha taSkillUp;

    bool tweenComplete = false;

    void Start()
    {
        taLevelUp = labelLevelUp.GetComponent<TweenAlpha>();
        taSkillUp = labelSkillUp.GetComponent<TweenAlpha>();

        transform.localPosition = new Vector3((Screen.width / 2) - (labelLevelUp.GetComponent<UILabel>().width / 2), screenYPosition, 0);
        transform.localScale = Vector3.one;
    }

    private void ResetTweenAlpha(TweenAlpha newTween)
    {
        float a = newTween.from;
        newTween.from = newTween.to;
        newTween.to = a;
        newTween.delay = 0.5f;

        newTween.Reset();
    }

    public void AlphaTweenFinished()
    {
        if (tweenComplete)
        {
            Destroy(gameObject);
            return;
        }

        tweenComplete = true;

        ResetTweenAlpha(taLevelUp);
        ResetTweenAlpha(taSkillUp);
    }
}
