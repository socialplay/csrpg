﻿using UnityEngine;
using System.Collections;

public class NGUIExpDisplay : MonoBehaviour
{
    public UILabel label;
    public TweenScale labelScale;

    public Vector3 startingPosition = new Vector3(120, 40, 0);
    public Vector3 finishPosition = new Vector3(47, 24, 1);

    bool isSecondPass = false;

    void Start()
    {
        transform.localPosition = startingPosition;
        transform.localScale = Vector3.one;
    }

    public void SetXPGained(int xp)
    {
        label.text = xp.ToString() + "xp";
    }

    public void FadeComplete()
    {
        Destroy(gameObject, 0.1f);
    }

    public void ScaleInComplete()
    {
        if (isSecondPass)
        {
            return;
        }

        Vector3 previousScale = labelScale.from;
        labelScale.from = labelScale.to;
        labelScale.to = previousScale;
        labelScale.duration = 0.5f;
        labelScale.delay = 0.5f;

        TweenPosition tweenPosition = label.gameObject.AddComponent<TweenPosition>();
        tweenPosition.duration = 0.5f;
        tweenPosition.delay = 0.5f;
        tweenPosition.to = finishPosition;
        tweenPosition.animationCurve = labelScale.animationCurve;

        labelScale.Reset();

        isSecondPass = true;
    }
}
