﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AmmoReference : MonoBehaviour
{
    private static AmmoReference instance;

    public Transform ammoDisplayAnchor;

    internal List<WeaponSlotAmmo> weaponAmmos = new List<WeaponSlotAmmo>();

    void Start()
    {
        if (instance == null)
            instance = this;

        initWeaponAmmos();
    }

    void initWeaponAmmos()
    {
        for (int ws = 0; ws < UIReference.Instance.equipment.slots.Length; ws++)
        {
            WeaponSlotAmmo wsa = UIReference.Instance.equipment.slots[ws].GetComponent<WeaponSlotAmmo>();

            if (wsa == null) continue;

            weaponAmmos.Add(wsa);
        }
    }

    /// <summary>
    /// Get the NGUI anchor to keep the display position dynamic according to the screen resolution.
    /// </summary>
    /// <returns>The NGUI anchor to position the ammo display.</returns>
    public static Transform GetDisplayAnchor()
    {
        return instance.ammoDisplayAnchor;
    }

    /// <summary>
    /// Get the weapon slot ammo information to add ammo to the specified weapon.
    /// </summary>
    /// <param name="weaponName">The name of the weapon you want to add ammo to.</param>
    /// <returns>WeaponSlotAmmo with weapon ammo info.</returns>
    public static WeaponSlotAmmo GetWeaponSlotAmmo(string weaponName)
    {
        WeaponSlotAmmo selectedAmmo = null;

        for (int w = 0; w < instance.weaponAmmos.Count; w++)
        {
            //remove spaces from the weaponName in the weapon slot.
            string slotWeaponName = instance.weaponAmmos[w].gameObject.GetComponent<WeaponSlot>().weaponName.Replace(" ", string.Empty);

            if (slotWeaponName == weaponName)
            {
                selectedAmmo = instance.weaponAmmos[w];
                break;
            }
        }

        return selectedAmmo;
    }
}
