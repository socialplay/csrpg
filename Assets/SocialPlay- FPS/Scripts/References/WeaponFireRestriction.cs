﻿using UnityEngine;
using System.Collections;

public class WeaponFireRestriction : MonoBehaviour
{
    public WeaponManager weaponManager;

    bool isWeaponRestricted = false;

    void Awake()
    {
        if (weaponManager == null)
        {
            weaponManager = GetComponent<WeaponManager>();
        }
    }

    void Update()
    {
        weaponManager.allWeapons.ForEach(delegate(WeaponScript ws)
        {
            ws.canFire = !isWeaponRestricted;

            if (ws.GunType != WeaponScript.gunType.KNIFE)
            {
                ws.canAim = !isWeaponRestricted;
            }
        });
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="isRestricted"></param>
    public void SetFireRestriction(bool isRestricted)
    {
        isWeaponRestricted = isRestricted;

        weaponManager.GetComponent<WeaponCrosshair>().enabled = !isRestricted;

        weaponManager.allWeapons.ForEach(delegate(WeaponScript ws)
        {
            ws.aimed = false;
        });
    }
}
