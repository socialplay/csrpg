﻿using UnityEngine;
using System;
using System.Collections;

public class PlayerData : MonoBehaviour
{
    public static event Action<PlayerData> eventPlayerSpawned;

    public static WeaponManager weaponManager = null;

    public static bool isPlayerSpawned = false;

    private static PlayerData instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    void Start()
    {
        weaponManager = GetComponentInChildren<WeaponManager>();

        if (eventPlayerSpawned != null)
        {
            eventPlayerSpawned(this);
        }

        isPlayerSpawned = true;
    }

    public static PlayerData GetPlayer()
    {
        if (!isPlayerSpawned) return null;
        else return instance;
    }
}
