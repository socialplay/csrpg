﻿using UnityEngine;
using System.Collections;

public class WeaponDropComponents : MonoBehaviour
{
    public ParticleSystem weaponParticles;
    public GameObject sfxPrefab;
    public Renderer[] renderers;

    public void changeParticleColor(Color newColor)
    {
        weaponParticles.startColor = newColor;

        weaponParticles.Play();
    }
}
