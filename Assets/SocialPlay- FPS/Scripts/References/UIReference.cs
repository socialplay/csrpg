﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIReference : MonoBehaviour
{
    private static UIReference instance;
    public static UIReference Instance { get { return instance; } }

    public NGUISlottedItemContainerDisplay equipment;
    public NGUILimitedGridItemContainerDisplay inventory;

    public GameObject levelUIDisplay;

    GameObject player;

    //float popMessageTimer = 0;

    void OnEnable()
    {
        PlayerData.eventPlayerSpawned += PlayerData_eventPlayerSpawned;
        RoomMultiplayerMenu.OnPaused += RoomMultiplayerMenu_OnPaused;
    }
    void OnDisable()
    {
        PlayerData.eventPlayerSpawned -= PlayerData_eventPlayerSpawned;
        RoomMultiplayerMenu.OnPaused -= RoomMultiplayerMenu_OnPaused;
    }

    void PlayerData_eventPlayerSpawned(PlayerData pd)
    {
        player = pd.gameObject;
    }

    void RoomMultiplayerMenu_OnPaused(bool menuPause)
    {
        DisplayPlayerUI(false);
    }

    void OnLevelWasLoaded(int sceneNumber)
    {
        if (sceneNumber < 2)
        {
            ContainerKeybinding.DisableKeybinding("lobby");
        }
        else
        {
            ContainerKeybinding.EnableKeybinding("lobby");
        }
    }

    void Awake()
    {
        if (instance == null)
            instance = this;
    }

    void Update()
    {
        if (levelUIDisplay == null) return;


        if (player != null)
        {
            levelUIDisplay.SetActive(true);

            return;
        }

        levelUIDisplay.SetActive(false);
    }

    /// <summary>
    /// Call to display or hide player UI.
    /// </summary>
    /// <param name="display">True to display player UI.</param>
    public void DisplayPlayerUI(bool display)
    {
        equipment.SetWindowIsActive(display);
        inventory.SetWindowIsActive(display);
    }
}
