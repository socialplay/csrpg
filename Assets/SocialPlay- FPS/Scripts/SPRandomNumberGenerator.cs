﻿using UnityEngine;
using System;
using System.Collections;
using System.IO;
using System.Security.Cryptography;

/// <summary>
/// Source: http://msdn.microsoft.com/en-us/library/system.security.cryptography.rngcryptoserviceprovider.aspx
/// </summary>
public class SPRandomNumberGenerator
{
    RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider();

    /// <summary>
    /// Generates a number from the amount of probabilities given.
    /// </summary>
    /// <param name="probabilities">number of probabilities</param>
    /// <returns></returns>
    public byte GetRandomGeneratedNumber(byte probabilities)
    {
        byte result = RandomNumberGeneration(probabilities);

        return result;
    }

    private byte RandomNumberGeneration(byte probabilities)
    {
        if (probabilities <= 0)
            throw new ArgumentOutOfRangeException("probabilities");

        // Create a byte array to hold the random value. 
        byte[] randomNumber = new byte[1];
        do
        {
            // Fill the array with a random value.
            rngCsp.GetBytes(randomNumber);
        }
        while (!IsFairGeneration(randomNumber[0], probabilities));
        // Return the random number mod the number 
        // of sides.  The possible values are zero- 
        // based, so we add one. 
        return (byte)((randomNumber[0] % probabilities) + 1);
    }

    private bool IsFairGeneration(byte result, byte probabilities)
    {
        // There are MaxValue / numSides full sets of numbers that can come up 
        // in a single byte.  For instance, if we have a 6 sided die, there are 
        // 42 full sets of 1-6 that come up.  The 43rd set is incomplete. 
        int fullSetsOfValues = Byte.MaxValue / probabilities;

        // If the roll is within this range of fair values, then we let it continue. 
        // In the 6 sided die case, a roll between 0 and 251 is allowed.  (We use 
        // < rather than <= since the = portion allows through an extra 0 value). 
        // 252 through 255 would provide an extra 0, 1, 2, 3 so they are not fair 
        // to use. 
        return result < probabilities * fullSetsOfValues;
    }

    
}
