﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class RoomObjects : MonoBehaviour
{
    private static RoomObjects roomObjects;

    public static event Action<GameObject> OnNewItemAdded;

    List<GameObject> players = new List<GameObject>();
    List<GameObject> drops = new List<GameObject>();

    void Awake()
    {
        if (roomObjects == null)
        {
            roomObjects = this;
        }
    }

    void OnEnable()
    {
        RoomMultiplayerMenu.PlayerSpawned += RoomMultiplayerMenu_PlayerSpawned;
        RoomMultiplayerMenu.OnRoundEnd += RoomMultiplayerMenu_OnRoundEnd;
    }
    void OnDisable()
    {
        RoomMultiplayerMenu.PlayerSpawned -= RoomMultiplayerMenu_PlayerSpawned;
        RoomMultiplayerMenu.OnRoundEnd -= RoomMultiplayerMenu_OnRoundEnd;
    }

    void RoomMultiplayerMenu_PlayerSpawned(PlayerDamage damage)
    {
        if (damage.gameObject.Equals(gameObject)) return;

        AddPlayer(damage.gameObject);
    }

    void RoomMultiplayerMenu_OnRoundEnd()
    {
        players.Clear();
        drops.Clear();
    }

    public static void AddPlayer(GameObject player)
    {
        if (roomObjects.players.Contains(player)) return;

        roomObjects.players.Add(player);
    }

    public static void RemovePlayer(GameObject player)
    {
        if (!roomObjects.players.Contains(player)) return;

        roomObjects.players.Remove(player);
    }

    public static List<GameObject> GetPlayers()
    {
        return roomObjects.players;
    }

    public static void AddItemDrop(GameObject itemDrop)
    {
        if(roomObjects.drops.Contains(itemDrop)) return;

        roomObjects.drops.Add(itemDrop);

        if (OnNewItemAdded != null)
        {
            OnNewItemAdded(itemDrop);
        }
    }

    public static void RemoveItemDrop(GameObject itemDrop)
    {
        if (!roomObjects.drops.Contains(itemDrop)) return;

        roomObjects.drops.Remove(itemDrop);
    }

    public static List<GameObject> GetItemDrops()
    {
        return roomObjects.drops;
    }
}
