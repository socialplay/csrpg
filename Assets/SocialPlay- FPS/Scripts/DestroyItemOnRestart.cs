﻿using UnityEngine;
using System.Collections;

public class DestroyItemOnRestart : MonoBehaviour
{
    void OnEnable()
    {
        RoomMultiplayerMenu.OnRoundRestart += RoomMultiplayerMenu_OnRoundRestart;
    }
    void OnDisable()
    {
        RoomMultiplayerMenu.OnRoundRestart -= RoomMultiplayerMenu_OnRoundRestart;
    }

    void RoomMultiplayerMenu_OnRoundRestart()
    {
        Destroy(gameObject);
    }
}
