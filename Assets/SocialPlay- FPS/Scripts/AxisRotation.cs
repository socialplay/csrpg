﻿using UnityEngine;
using System.Collections;

public class AxisRotation : MonoBehaviour {

    public Vector3 rotationAxis;

	
	// Update is called once per frame
	void Update () {
        this.transform.Rotate(rotationAxis * Time.deltaTime);
	}
}
