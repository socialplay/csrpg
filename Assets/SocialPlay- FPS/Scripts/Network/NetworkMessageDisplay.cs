﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class NetworkMessageDisplay : Photon.MonoBehaviour
{
    public int messageDisaplyMaxCount = 10;

    public float displayTime = 2;
    public float fadeSpeedModifier = 1;

    private static NetworkMessageDisplay display = null;

    private List<string> messages = new List<string>();

    private int currentListDisaplyIndex = 0;

    private float guiAlpha = 0;
    private float displayTimer = 0;

    void Awake()
    {
        if (display == null)
        {
            display = this;
        }
    }


    void OnLeftRoom()
    {
        ClearNotifications();
    }

    public static void AddRecivedItemMessage(ItemData item, string reson)
    {
        string msg = "Recived ";
        if (item.stackSize != 1)
        {
            msg += item.stackSize + " ";
        }
        msg += NetworkMessageDisplay.AddColor(ItemQuailityColorSelector.GetColorForItem(item)) + item.itemName;    
        if (reson != string.Empty)
        {
            msg +=NetworkMessageDisplay.AddColor(Color.white) + reson;
        }
        //Debug.Log(msg);
        NetworkMessageDisplay.AddLocalMessage(msg);
    }

    public static void AddLocalMessage(string message)
    {
        display.AddMessage(message);
    }

    public static void AddNetworkMessage(string message)
    {
        display.photonView.RPC("NetworkMessageRecived", PhotonTargets.All, message);
    }

    public static void ClearNotifications()
    {
        display.clearNotifications();
    }

    [RPC]
    void NetworkMessageRecived(string message)
    {
        AddMessage(message);
    }

    private void clearNotifications()
    {
        messages.Clear();
        currentListDisaplyIndex = 0;
    }

    private void AddMessage(string msg)
    {
        initFadeTimer();

        messages.Add(msg);
        currentListDisaplyIndex = (messages.Count < messageDisaplyMaxCount ? 0 : messages.Count - messageDisaplyMaxCount);
    }

    private void initFadeTimer()
    {
        displayTimer = displayTime;
        guiAlpha = 1;
    }

    private void fadeTimer()
    {
        if (displayTimer > 0)
        {
            displayTimer -= Time.deltaTime * (1 / displayTime);
        }
        else
        {
            if (guiAlpha > 0)
            {
                guiAlpha -= (Time.deltaTime * (1 / fadeSpeedModifier));
            }
            else
            {
                guiAlpha = 0;
                ClearNotifications();
            }
        }
    }

    private Color getColorWithAlpha(Color color)
    {
        Color fadedColor = color;
        fadedColor.a = guiAlpha;

        return fadedColor;
    }

    void Update()
    {
        fadeTimer();
    }

    void OnGUI()
    {
        GUILayout.BeginArea(new Rect(Mathf.Ceil(Screen.width / 2.078f), 60, Screen.width / 2, 400));
        GUILayout.BeginVertical();

        if (guiAlpha > 0 || !PauseGamePlay.IsMenuOpen())
        {
            for (int i = currentListDisaplyIndex; i < messages.Count; i++)
            {
                string msg = messages[i];
                GUILayout.BeginHorizontal();
                GUI.color = getColorWithAlpha(Color.white);
                GUILayout.FlexibleSpace();

                foreach (string part in msg.Split(':'))
                {
                    if (part.Contains("*c"))
                    {
                        string color = part.Remove(0, 2);
                        GUI.color = getColorWithAlpha(NGUITools.ParseColor(color, 0));
                    }
                    else
                    {
                        GUILayout.Label(part);
                    }
                }

                GUILayout.EndHorizontal();
            }
        }

        GUILayout.EndVertical();
        GUILayout.EndArea();
    }

    public static string AddColor(Color clr)
    {
        return ":*c" + NGUITools.EncodeColor(clr) + ":";
    }
}
