﻿using UnityEngine;
using System.Collections;
using System;

public abstract class DamagableObject : Photon.MonoBehaviour
{
    public static event Action<float> DealDamage;

    public virtual void TotalDamage(float damage, HitBox source = null)
    {
        if (DealDamage != null)
        {
            DealDamage(damage);
        }
    }
}
