﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(PhotonView))]
public class TargetDamagableObject : DamagableObject
{
    public float hp = 10;
    public List<PlayerDamage.HitBoxes> frontHitBoxes = new List<PlayerDamage.HitBoxes>();
    public List<PlayerDamage.HitBoxes> RearHitBoxes = new List<PlayerDamage.HitBoxes>();


    public float SpawnImmunityDelay = 5f;
    public int XPValue = 20;
    public int EditorXPValue = 3000;
    public int killEnergyWorth = 825;
    public Renderer immuneObject;
    public Color immuneColor;
    public Color DamageColor;
    public AnimationMonitor animator;



    Color originalColor;
    float spawnCountdown = 0;
    bool isActive = false;
    float shortTimmer = 0;
    bool isImmune = false;
    ItemGetter itemGetter;
    bool isDamaged = false;
    bool isStanding = true;
    AmmoDropper ammoDropper;
    bool isAlive = true;

    void OnEnable()
    {
        if (animator != null)
        {
            animator.AnimationDeathEnded += animator_AnimationEneded;
            animator.AnimationSatndEnded += animator_AnimationSatndEnded;
        }
    }



    void OnDisable()
    {
        if (animator != null)
        {
            animator.AnimationDeathEnded -= animator_AnimationEneded;
            animator.AnimationSatndEnded -= animator_AnimationSatndEnded;
        }
    }

    void animator_AnimationSatndEnded()
    {
        isStanding = false;
    }

    void animator_AnimationEneded()
    {

        if (!PhotonNetwork.isMasterClient) return;
        PhotonNetwork.Destroy(photonView);
    }

    void Awake()
    {

#if UNITY_EDITOR
        XPValue = EditorXPValue;
#endif
        SinglePlayerTargetSpawner.AddObjectToTargetList(this.transform);
        this.name = "Target Dummy";
        itemGetter = this.GetComponentInChildren<ItemGetter>();
        originalColor = immuneObject.material.color;

        if (animator != null)
        {
            animator.animation.Play("Stand_Up");
        }

        ammoDropper = GameObject.FindObjectOfType<AmmoDropper>();
    }

    void Update()
    {
        if (isStanding) return;
        if (!isActive)
        {
            spawnCountdown += Time.deltaTime;
            shortTimmer += Time.deltaTime;

            if (shortTimmer > 0.4f)
            {
                shortTimmer = 0;
                immunityToggle();
            }

            if (spawnCountdown > SpawnImmunityDelay)
            {
                spawnCountdown += 0;
                shortTimmer += 0;
                Activate();
            }
        }
        else if (isDamaged)
        {
            shortTimmer += Time.deltaTime;
            if (shortTimmer > 0.4f)
            {
                shortTimmer = 0;
                isDamaged = !isDamaged;
            }
            DamageAnimate();
        }
    }

    void immunityToggle()
    {
        if (isActive)
        {
            immuneObject.material.color = originalColor;
            return;
        }
        if (!isImmune)
        {
            immuneObject.material.color = immuneColor;
        }
        else
        {
            immuneObject.material.color = originalColor;

        }
        isImmune = !isImmune;
    }

    void DamageAnimate()
    {
        if (!isDamaged)
        {
            immuneObject.material.color = originalColor;
            return;
        }
        immuneObject.material.color = DamageColor;
    }

    void Activate()
    {
        for (int i = 0; i < frontHitBoxes.Count; i++)
        {
            frontHitBoxes[i].box.gameObject.AddComponent<HitBox>();
            frontHitBoxes[i].box.gameObject.GetComponent<HitBox>().damageMultiplier = RearHitBoxes[i].damage;
            frontHitBoxes[i].box.gameObject.GetComponent<HitBox>().damagableObject = this;
            frontHitBoxes[i].box.isTrigger = false;
        }

        for (int i = 0; i < RearHitBoxes.Count; i++)
        {
            RearHitBoxes[i].box.gameObject.AddComponent<HitBox>();
            RearHitBoxes[i].box.gameObject.GetComponent<HitBox>().damageMultiplier = RearHitBoxes[i].damage;
            RearHitBoxes[i].box.gameObject.GetComponent<HitBox>().damagableObject = this;
            RearHitBoxes[i].box.isTrigger = false;
        }
        isActive = true;
        immunityToggle();
    }


    public override void TotalDamage(float damage, HitBox source = null)
    {
        if (!isAlive) return;
        hp -= damage;
        if (hp <= 0)
        {
            isAlive = false;

            GiveReward();
            SinglePlayerTargetSpawner.RemoveTarget(photonView.viewID);
            bool isFront = false;
            if (source != null && source.tag == "Dummy Front")
            {
                isFront = true;
            }
            AnimateDeath(isFront);
            photonView.RPC("MasterDestroyTarget", PhotonTargets.AllBuffered, isFront);

            //Give our player experience

            LevelPlayer.Instance.GivePlayerExperience(XPValue);
        }
        else
        {
            isDamaged = true;
            shortTimmer = 0;
        }
        base.TotalDamage(damage, source);
    }




    [RPC]
    void MasterDestroyTarget(bool isFromFront)
    {
        AnimateDeath(isFromFront);
    }


    public void AnimateDeath(bool isFront)
    {
        if (!isFront)
        {
            animator.animation.Play("Fall_Forward");
        }
        else
        {
            animator.animation.Play("Fall_Backward");
        }
    }

    /// <summary>
    /// Created by: Mike Oliver
    /// Drops item on the ground on death
    /// </summary>
    public void GiveReward()
    {
        if (itemGetter == null)
        {
            itemGetter = this.GetComponentInChildren<ItemGetter>();
        }

        itemGetter.MaxEnergy = killEnergyWorth;

        itemGetter.GetItems();
        itemGetter.transform.parent = transform.parent;

        if (ammoDropper != null)
        {
            //drops ammo reward for player. (added by: John Suratos)
            ammoDropper.InstantiateAmmoItemDrop(transform.position);
        }
    }

}
