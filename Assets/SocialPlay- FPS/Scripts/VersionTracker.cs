﻿using UnityEngine;
using System.Collections;

public class VersionTracker : MonoBehaviour
{
    internal string versionLocation;
    internal int versionNumber = 1;

    void OnLevelWasLoaded(int lvl)
    {
        ConnectMenu menu = GameObject.FindObjectOfType<ConnectMenu>();
        if (menu != null)
        {
            menu.CSRPGVersion = versionLocation + " v" + versionNumber;
        }
    }
}
