using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NGUISkillsDisplay : MonoBehaviour
{

    public SkillsManager skillsManager;

    public UILabel availableSPDisplay;

    public List<NGUISkillHookUp> skillSlots;

    void Start()
    {
        for (int i = 0; i < skillsManager.playerSkills.Count; i++)
        {
            if (i >= skillSlots.Count)
                break;

            UIEventListener.Get(skillSlots[i].skillLevelUpButton).onClick += OnLevelUpClicked;
        }
    }

    void Update()
    {
        if (skillsManager.skillPoints > 0)
            DisplaySkillsLevelUp();
        else
            HideSkillsLevelUp();

        UpdateSkillLevels();
    }

    void UpdateSkillLevels()
    {
        availableSPDisplay.text = skillsManager.skillPoints.ToString();

        for (int i = 0; i < skillSlots.Count; i++)
        {
            if (i >= skillsManager.playerSkills.Count)
                break;

            string levelDisplay = string.Empty;

            if (skillSlots[i].skillLevelUpButton.GetComponent<UIButton>().isEnabled)
            {
                if (skillsManager.playerSkills[i].skillLevel > 0)
                {
                    levelDisplay = "Lv." + skillsManager.playerSkills[i].skillLevel + " > " + (skillsManager.playerSkills[i].skillLevel + 1);
                }
                else
                {
                    levelDisplay = "+";
                }
            }
            else
            {
                levelDisplay = "Lv." + skillsManager.playerSkills[i].skillLevel.ToString();

                if (skillsManager.playerSkills[i].skillLevel == skillsManager.playerSkills[i].maxSkillLevel)
                {
                    levelDisplay = "MAX";
                }
            }

            skillSlots[i].skillLevelDisplay.text = levelDisplay;
        }
    }

    void HideSkillsLevelUp()
    {
        for (int i = 0; i < skillSlots.Count; i++)
        {
            if (i >= skillsManager.playerSkills.Count)
                break;

            if (skillsManager.playerSkills[i].skillLevel <= 0)
            {
                skillSlots[i].skillLevelUpButton.SetActive(false);
            }

            if (skillSlots[i].skillLevelUpButton.GetComponent<UIButton>().isEnabled)
            {
                skillSlots[i].skillLevelUpButton.GetComponent<UIButton>().isEnabled = false;
            }
        }
    }

    void DisplaySkillsLevelUp()
    {
        for (int i = 0; i < skillSlots.Count; i++)
        {
            if (i >= skillsManager.playerSkills.Count)
                break;

            if (!skillSlots[i].skillLevelUpButton.activeInHierarchy)
            {
                skillSlots[i].skillLevelUpButton.SetActive(true);
            }

            if (skillsManager.playerSkills[i].skillLevel < skillsManager.playerSkills[i].maxSkillLevel)
            {
                skillSlots[i].skillLevelUpButton.GetComponent<UIButton>().isEnabled = true;
            }
            else
            {
                skillSlots[i].skillLevelUpButton.GetComponent<UIButton>().isEnabled = false;
            }
        }
    }

    void OnLevelUpClicked(GameObject button)
    {
        int skillLocation = skillSlots.IndexOf(button.transform.parent.GetComponent<NGUISkillHookUp>());

        skillsManager.LevelUpSkill(skillLocation);
    }
}
