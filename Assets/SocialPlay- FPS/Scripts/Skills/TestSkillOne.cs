using UnityEngine;
using System.Collections;

public class TestSkillOne : Skill {

    public override void LevelUpSkill()
    {
        base.LevelUpSkill();
    }

    public override void UseSkill()
    {
        if (skillLevel > 0)
            Debug.Log(skillName + " skill used");
    }
}
