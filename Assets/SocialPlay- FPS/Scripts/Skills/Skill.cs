using UnityEngine;
using System.Collections;
using System;

public class Skill : MonoBehaviour
{

    public event Action<bool> OnLevelUp;

    public bool isPassive;
    public string skillName;
    public int skillLevel = 0;
    public int maxSkillLevel = 3;
    public KeyCode activationKey;

    public virtual void LevelUpSkill()
    {
        skillLevel++;
        if (skillLevel >= maxSkillLevel)
        {
            skillLevel = maxSkillLevel;
        }
        if (OnLevelUp != null)
        {
            OnLevelUp((skillLevel == maxSkillLevel));
        }
    }

    public virtual void ResetSkill()
    {
        skillLevel = 0;
        maxSkillLevel = 3;
    }

    public virtual void UseSkill() { }
}
