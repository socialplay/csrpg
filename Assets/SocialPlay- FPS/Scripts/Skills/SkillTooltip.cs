﻿using UnityEngine;
using System.Collections;

public class SkillTooltip : MonoBehaviour, ITooltipSetup {

    public string skillName;

    public string skillEffectOne;
    public string skillEffectTwo;
    public string skillEffectThree;


    public string Setup()
    {
        string skillText = "";

        skillText += skillName + " \n ";

        skillText += "Level 1: " + skillEffectOne + " \n ";
        skillText += "Level 2: " + skillEffectTwo + " \n ";
        skillText += "Level 3: " + skillEffectThree;

        return skillText;
    }
}
