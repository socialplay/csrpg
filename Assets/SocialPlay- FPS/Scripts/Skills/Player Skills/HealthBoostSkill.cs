using UnityEngine;
using System.Collections;

public class HealthBoostSkill : Skill
{
    public int stepValue = 34;
    //public float currentMaximumHealth = 100;

    int maxHealth = 200;

    PlayerDamage player;

    void OnEnable()
    {
        RoomMultiplayerMenu.PlayerSpawned += RoomMultiplayerMenu_PlayerSpawned;
    }

    void OnDisable()
    {
        RoomMultiplayerMenu.PlayerSpawned -= RoomMultiplayerMenu_PlayerSpawned;
    }


    void RoomMultiplayerMenu_PlayerSpawned(PlayerDamage obj)
    {
        player = obj;
        ApplyHealthBoost();
    }

    public override void LevelUpSkill()
    {
        base.LevelUpSkill();
        ApplyHealthBoost();
    }

    void ApplyHealthBoost()
    {
        float total = stepValue * skillLevel;
        float current = player.currentHp + total;
        if (current >= maxHealth)
            current = maxHealth;
        float max = player.hp + total;
        if (max >= maxHealth)
            max = maxHealth;

        player.SetHealth(current, max);
    }

}
