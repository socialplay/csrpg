using UnityEngine;
using System.Collections;

public class JumpSkill : Skill
{

    FPScontroller controller;

    public float currentJumpHeight = 1;

    public JumpLevel[] jumpLevels;

    float maxJumpHeight = 5;


    void OnEnable()
    {
        RoomMultiplayerMenu.PlayerSpawned += RoomMultiplayerMenu_PlayerSpawned;
    }

    void OnDisable()
    {
        RoomMultiplayerMenu.PlayerSpawned -= RoomMultiplayerMenu_PlayerSpawned;
    }


    void RoomMultiplayerMenu_PlayerSpawned(PlayerDamage player)
    {
        controller = player.GetComponent<FPScontroller>();
        SetJumpLevel();
    }

    void SetJumpLevel()
    {
        if (skillLevel < 1) return;
        float jumpHeight = jumpLevels[skillLevel - 1].height;

        if (jumpHeight > maxJumpHeight)
        {
            jumpHeight = maxJumpHeight;
        }
        controller.jumping.baseHeight = jumpHeight;
        controller.fallDamage.fallLimit = jumpLevels[skillLevel - 1].fallLimit;
    }

    public override void LevelUpSkill()
    {
        base.LevelUpSkill();
        SetJumpLevel();
    }

    [System.Serializable]
    public class JumpLevel
    {
        public float height = 1;
        public float fallLimit = 0.85f;

    }
}
