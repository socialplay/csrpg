using UnityEngine;
using System.Collections;

public class HealthRegenSkill : Skill
{

    PlayerDamage playerDamage = null;

    float time = 0.0f;
    public float regenTime = 3.0f;

    void Update()
    {      
        if (PlayerData.GetPlayer() != null && playerDamage == null)
        {
            playerDamage = PlayerData.GetPlayer().GetComponent<PlayerDamage>();
        }


        if (skillLevel > 0)
        {
            time += Time.deltaTime;

            if (time >= regenTime)
            {
                RegenPlayerHealth();
                time = 0.0f;
            }
        }
    }

    void RegenPlayerHealth()
    {
        playerDamage.currentHp += skillLevel;
        if (playerDamage.currentHp > playerDamage.hp)
            playerDamage.currentHp = playerDamage.hp;

    }

}
