using UnityEngine;
using System.Collections;

public class StealthSkill : Skill
{
    public float[] stealthLevels;

    Stealth stealth;

    void OnEnable()
    {
        RoomMultiplayerMenu.PlayerSpawned += RoomMultiplayerMenu_PlayerSpawned;
    }

    void OnDisable()
    {
        RoomMultiplayerMenu.PlayerSpawned -= RoomMultiplayerMenu_PlayerSpawned;
    }


    void RoomMultiplayerMenu_PlayerSpawned(PlayerDamage player)
    {
        stealth = player.GetComponent<Stealth>();
        SetPlayerStealthLevel();
    }

    public override void LevelUpSkill()
    {
        base.LevelUpSkill();
        SetPlayerStealthLevel();
    }

    void SetPlayerStealthLevel()
    {
        if (skillLevel < 1) return;
        stealth.UpdatePlayerStealth(stealthLevels[skillLevel - 1]);
    }
}
