using UnityEngine;
using System.Collections;

public class SpeedSkill : Skill
{
    //FPScontrollerManager movementManager;
    FPScontroller controller;

    MovementData currentMovementData;

    public float[] speedMultiplier = { 1.25f, 1.4f, 1.6f };

    void OnEnable()
    {
        RoomMultiplayerMenu.PlayerSpawned += RoomMultiplayerMenu_PlayerSpawned;
    }

    void OnDisable()
    {
        RoomMultiplayerMenu.PlayerSpawned -= RoomMultiplayerMenu_PlayerSpawned;
    }


    void RoomMultiplayerMenu_PlayerSpawned(PlayerDamage player)
    {
        controller = player.GetComponent<FPScontroller>();
        currentMovementData = new MovementData(controller.movement.WalkSpeed, controller.movement.RunSpeed, controller.movement.CrouchSpeed, controller.movement.ProneSpeed);
        SetCurrentMovementSpeed();
    }


    void SetCurrentMovementSpeed()
    {
        if (skillLevel < 1) return;
        currentMovementData.UpdateController(controller, speedMultiplier[skillLevel-1]);
    }

    public override void LevelUpSkill()
    {
        base.LevelUpSkill();
        SetCurrentMovementSpeed();
    }
}

[System.Serializable]
public class MovementData
{
    public float walkspeed = 0;
    public float runspeed = 0;
    public float crouchspeed = 0;
    public float pronespeed = 0;
    //public float backwardsspeed = 0;
    //public float sidewaysspeed = 0;

    public MovementData(float walk, float run, float crouch, float prone)
    {
        this.walkspeed = walk;
        this.runspeed = run;
        this.crouchspeed = crouch;
        this.pronespeed = prone;
    }

    public void UpdateController(FPScontroller cont, float currentMultiplier)
    {
        cont.movement.WalkSpeed = this.walkspeed * currentMultiplier;
        cont.movement.ProneSpeed = this.pronespeed * currentMultiplier;
        cont.movement.RunSpeed = this.runspeed * currentMultiplier;
        cont.movement.crouchHeight = this.crouchspeed * currentMultiplier;
    }
}
