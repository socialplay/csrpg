using UnityEngine;
using System.Collections;

public class LifeStealSkill : Skill
{
    public float[] lifeStealMultiplier = { 1.05f, 1.1f, 1.12f };

    PlayerDamage playerDamage;


    void OnEnable()
    {
        PlayerDamage.DealDamage += PlayerDamage_DealDamage;
        RoomMultiplayerMenu.PlayerSpawned += RoomMultiplayerMenu_PlayerSpawned;
    }

    void OnDisable()
    {
        PlayerDamage.DealDamage -= PlayerDamage_DealDamage;
        RoomMultiplayerMenu.PlayerSpawned -= RoomMultiplayerMenu_PlayerSpawned;
    }

    void RoomMultiplayerMenu_PlayerSpawned(PlayerDamage player)
    {
        playerDamage = player;
    }

    void PlayerDamage_DealDamage(float damageAmount)
    {
        if (skillLevel < 1) return;
        float healthRestored = Mathf.Ceil(damageAmount * (lifeStealMultiplier[skillLevel - 1]));
        if ((playerDamage.currentHp + healthRestored) < playerDamage.hp)
        {
            playerDamage.currentHp += healthRestored;
        }
        else
        {
            playerDamage.currentHp = playerDamage.hp;
        }
    }
}