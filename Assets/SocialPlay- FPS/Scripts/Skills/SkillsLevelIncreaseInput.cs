using UnityEngine;
using System.Collections;

public class SkillsLevelIncreaseInput : MonoBehaviour {

    public SkillsManager skillsManager;

    void Update()
    {
        if (skillsManager.skillPoints > 0)
        {
            if (Input.GetKeyDown(KeyCode.F1))
            {
                skillsManager.LevelUpSkill(0);
            }
            if (Input.GetKeyDown(KeyCode.F2))
            {
                skillsManager.LevelUpSkill(1);
            }
            if (Input.GetKeyDown(KeyCode.F3))
            {
                skillsManager.LevelUpSkill(2);
            }
            if (Input.GetKeyDown(KeyCode.F4))
            {
                skillsManager.LevelUpSkill(3);
            }
            if (Input.GetKeyDown(KeyCode.F5))
            {
                skillsManager.LevelUpSkill(4);
            }
            if (Input.GetKeyDown(KeyCode.F6))
            {
                skillsManager.LevelUpSkill(5);
            }
        }
    }
}
