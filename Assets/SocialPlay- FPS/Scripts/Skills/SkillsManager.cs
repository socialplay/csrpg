using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SkillsManager : Photon.MonoBehaviour
{

    public LevelPlayer levelPlayer;

    public int skillPoints;

    public List<Skill> playerSkills;

    int addOne = 1;
    int subtractOne = -1;

    void Start()
    {
        skillPoints = levelPlayer.GetPlayerLevel() - 1;
        LevelPlayer.onPlayerLevelEvent += AddSkillPoint;
    }

    void Update()
    {
        playerSkills.ForEach(delegate(Skill skill)
        {
            if (Input.GetKeyDown(skill.activationKey))
            {
                LevelUpSkill(skill);
            }
        });
    }

    public void LevelUpSkill(Skill skill)
    {
        if (skill.skillLevel >= skill.maxSkillLevel)
            return;

        skill.LevelUpSkill();

        modifySkillPoints(subtractOne);
    }


    public void LevelUpSkill(int index)
    {
        LevelUpSkill(playerSkills[index]);
    }

    void AddSkillPoint(int level)
    {
        modifySkillPoints(addOne);
    }

    void modifySkillPoints(int amount)
    {
        skillPoints += amount;
    }

    public void ResetSkills()
    {
        playerSkills.ForEach(delegate(Skill skill)
        {
            skill.ResetSkill();
        });
        skillPoints = 0;
    }

    void OnLeftRoom()
    {
        ResetSkills();
    }
}
