﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SkillsManager))]
public class RoundEnd : MonoBehaviour {

    SkillsManager manager;


    void OnEnable()
    {
        RoomMultiplayerMenu.OnRoundRestart += RoomMultiplayerMenu_OnRoundRestart;
    }

    void OnDisable()
    {
        RoomMultiplayerMenu.OnRoundRestart -= RoomMultiplayerMenu_OnRoundRestart;
    }

    void Awake()
    {
        manager = this.GetComponent<SkillsManager>();
    }

    void RoomMultiplayerMenu_OnRoundRestart()
    {
        manager.levelPlayer.LevelPlayerInit(1);
        manager.ResetSkills();
    }

}
