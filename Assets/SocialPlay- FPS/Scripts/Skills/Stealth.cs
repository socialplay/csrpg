using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(PhotonView))]
public class Stealth : Photon.MonoBehaviour
{
    public List<Renderer> objectsForStealth = new List<Renderer>();

    Shader stealthShader;  

    void Awake()
    {
        stealthShader = Shader.Find("Transparent/VertexLit with Z");
    }

    public void UpdatePlayerStealth(float alpha)
    {
        if (!photonView.isMine) return;        
        photonView.RPC("setPlayerStealthLevel", PhotonTargets.AllBuffered,alpha);
    }

    [RPC]
    void setPlayerStealthLevel(float alpha)
    {
        if (stealthShader == null)
        {
            stealthShader = Shader.Find("Transparent/VertexLit with Z");
        }
        foreach (Renderer rend in objectsForStealth)
        {
            ModifiedObjectTransparency(rend, alpha);
        }
    }

    void ModifiedObjectTransparency(Renderer render, float transparencyLevel)
    {
        if (render.materials.Length > 1)
        {
            for (int i = 0; i < render.materials.Length; i++)
            {
                ModifyMaterialShaderAndAlpha(render.materials[i], transparencyLevel);
            }
        }
        else
        {
            ModifyMaterialShaderAndAlpha(render.material, transparencyLevel);
        }
    }

    void ModifyMaterialShaderAndAlpha(Material rendMaterial, float alphaLevel)
    {
        rendMaterial.shader = stealthShader;

        if (rendMaterial.color == null) return;

        Color modifiedAlpha = rendMaterial.color;
        modifiedAlpha.a = alphaLevel;

        rendMaterial.color = modifiedAlpha;
    }

   
}

[System.Serializable]
public class PlayerViewObjects
{
    public string objectName;
    public List<GameObject> playerObjects = new List<GameObject>();
}