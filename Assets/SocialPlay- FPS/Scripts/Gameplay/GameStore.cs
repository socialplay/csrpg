﻿using UnityEngine;
using System.Collections;

public class GameStore : MonoBehaviour
{
    public static event System.Action<bool> eventDisplayGameStore;

    private static GameStore gameStore;

    public GUISkin guiSkin;
    public KeyCode openStoreBinding = KeyCode.E;
    //public DisplayItems storeDisplayItems;
    //public CurrencyBalance currencyBalance;

    internal bool inStoreZone = false;

    bool openStore = false;
    bool isViewMine = false;
    
    public static bool CanOpenStore = false;

    void OnEnable()
    {
        RoomMultiplayerMenu.OnPaused += RoomMultiplayerMenu_OnPaused;
        RoomMultiplayerMenu.PlayerSpawned += RoomMultiplayerMenu_PlayerSpawned;
        RoomMultiplayerMenu.OnRoundEnd += RoomMultiplayerMenu_OnRoundEnd;
        RoomMultiplayerMenu.OnRoundRestart += RoomMultiplayerMenu_OnRoundRestart;
    }
    void OnDisable()
    {
        RoomMultiplayerMenu.OnPaused -= RoomMultiplayerMenu_OnPaused;
        RoomMultiplayerMenu.PlayerSpawned -= RoomMultiplayerMenu_PlayerSpawned;
        RoomMultiplayerMenu.OnRoundEnd -= RoomMultiplayerMenu_OnRoundEnd;
        RoomMultiplayerMenu.OnRoundRestart -= RoomMultiplayerMenu_OnRoundRestart;
    }

    void RoomMultiplayerMenu_OnPaused(bool menuPause)
    {
        CanOpenStore = !menuPause;
        openStore = false;
        displayStore(false);
    }

    void RoomMultiplayerMenu_PlayerSpawned(PlayerDamage pd)
    {
        isViewMine = pd.GetComponent<PhotonView>().isMine;
    }

    void RoomMultiplayerMenu_OnRoundEnd()
    {
        displayStore(false);
    }

    void RoomMultiplayerMenu_OnRoundRestart()
    {
        openStore = false;
        inStoreZone = false;
    }

    void Update()
    {
        if (checkKeyBinding() && inStoreZone && isViewMine)
        {
            toggleStoreDisplay();
        }
    }

    void OnGUI()
    {
        GUI.skin = guiSkin;

        if (inStoreZone && isViewMine && CanOpenStore)
        {
            GUI.Label(new Rect(Screen.width - 190, Screen.height - 95, 190, 20), openStoreBinding.ToString() + " - Open/Close Store"); 
        }
    }

    bool checkKeyBinding()
    {
        if (Input.GetKeyDown(openStoreBinding) && CanOpenStore)
        {
            return true;
        }

        return false;
    }

    void toggleStoreDisplay()
    {
        openStore = !openStore;

        displayStore(openStore);
    }

    void displayStore(bool display)
    {
        UITooltip.ShowText(null); //hide any tooltips.

        if (eventDisplayGameStore != null)
        {
            eventDisplayGameStore(display);
        }

        if (display)
        {
            NGUIStore.ActivateStore(display);
            ContainerKeybinding.DisableKeybinding("store");
        }
        else
        {
            NGUIStore.DeactivateStore();
            ContainerKeybinding.EnableKeybinding("store");
        }
    }
}
