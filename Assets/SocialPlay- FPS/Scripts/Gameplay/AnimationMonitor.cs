﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(Animation))]
public class AnimationMonitor : MonoBehaviour
{

    public event Action AnimationDeathEnded;
    public event Action AnimationSatndEnded;

    public void AnimationDeathFinished()
    {
        if (AnimationDeathEnded != null)
        {
            AnimationDeathEnded();
        }
    }


    public void AnimationStandFinished()
    {
        if (AnimationSatndEnded != null)
        {
            AnimationSatndEnded();
        }
    }


}
