﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PauseGamePlay : MonoBehaviour
{
    public ContainerDisplay inventory;

    bool inventoryLastState = false;

    static bool isMenuOpen;

    EnableHelper enableHelper;
    WeaponFireRestriction weaponFireRestrictor;

    static List<string> pauseQueue = new List<string>();

    void OnEnable()
    {
        GameStore.eventDisplayGameStore += GameStore_eventDisplayGameStore;
        RoomMultiplayerMenu.OnPaused += RoomMultiplayerMenu_OnPaused;
        RoomMultiplayerMenu.PlayerSpawned += RoomMultiplayerMenu_PlayerSpawned;
    }
    void OnDisable()
    {
        GameStore.eventDisplayGameStore -= GameStore_eventDisplayGameStore;
        RoomMultiplayerMenu.OnPaused -= RoomMultiplayerMenu_OnPaused;
        RoomMultiplayerMenu.PlayerSpawned -= RoomMultiplayerMenu_PlayerSpawned;
    }

    void GameStore_eventDisplayGameStore(bool display)
    {
        processMenuRequest(display, "gameStore");
    }

    void RoomMultiplayerMenu_OnPaused(bool menuPause)
    {
        processMenuRequest(menuPause, "roomMenuPause");

        allowContainerKeyBinding(!menuPause);
    }

    void RoomMultiplayerMenu_PlayerSpawned(PlayerDamage pd)
    {
        if (pd.GetComponent<PhotonView>().isMine)
        {
            enableHelper = pd.GetComponent<EnableHelper>();
            weaponFireRestrictor = pd.GetComponent<WeaponFireRestriction>();
        }
    }

    void Update()
    {
        if (inventoryLastState != inventory.IsWindowActive())
        {
            GameStore.CanOpenStore = !inventory.IsWindowActive();
            inventoryLastState = inventory.IsWindowActive();

            processMenuRequest(inventory.IsWindowActive(), "inventory");
        }

        if (enableHelper == null || enableHelper.enablerReferenceObject == null) return;

        if (isMenuOpen)
        {
            if (enableHelper.enablerReferenceObject.activeSelf == true)
            {
                enableHelper.enablerReferenceObject.SetActive(false);

                weaponFireRestrictor.SetFireRestriction(true);
            }
            Screen.lockCursor = false;
        }
        else
        {
            if (enableHelper.enablerReferenceObject.activeSelf == false)
            {
                enableHelper.enablerReferenceObject.SetActive(true);

                weaponFireRestrictor.SetFireRestriction(false);
            }
            Screen.lockCursor = true;
        }
    }

    void allowContainerKeyBinding(bool allow)
    {
        if (allow)
        {
            ContainerKeybinding.EnableKeybinding("pause");
        }
        else
        {
            ContainerKeybinding.DisableKeybinding("pause");
        }
    }

    void processMenuRequest(bool requestState, string requester)
    {
        if (requestState)
        {
            requestOpenMenu(requester);
        }
        else
        {
            requestCloseMenu(requester);
        }
    }

    void requestOpenMenu(string openMenuRequester)
    {
        if (!pauseQueue.Contains(openMenuRequester))
        {
            pauseQueue.Add(openMenuRequester);
            isMenuOpen = true;
        }
    }

    void requestCloseMenu(string closeMenuRequester)
    {
        if (pauseQueue.Contains(closeMenuRequester))
        {
            pauseQueue.Remove(closeMenuRequester);

            if (pauseQueue.Count < 1)
            {
                isMenuOpen = false; 
            }
        }
    }

    /// <summary>
    /// Check if is paused.
    /// </summary>
    /// <returns>isPaused state.</returns>
    public static bool IsMenuOpen()
    {
        return isMenuOpen;
    }
}
