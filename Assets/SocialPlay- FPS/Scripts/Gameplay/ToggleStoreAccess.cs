﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider))]
public class ToggleStoreAccess : MonoBehaviour
{
    void OnTriggerEnter(Collider col)
    {
        if (col.transform.root.gameObject.GetComponent<PlayerDamage>())
        {
            playerInStore(col.transform.root.gameObject, true);
        }
    }

    void OnTriggerStay(Collider col)
    {
        if (col.transform.root.gameObject.GetComponent<PlayerDamage>())
        {
            playerInStore(col.transform.root.gameObject, true);
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.transform.root.gameObject.GetComponent<PlayerDamage>())
        {
            playerInStore(col.transform.root.gameObject, false);
        }
    }

    void playerInStore(GameObject player, bool inStore)
    {
        if (player.GetComponent<PhotonView>().isMine)
        {
            GetComponent<GameStore>().inStoreZone = inStore;
            player.GetComponent<PlayerDamage>().disableDamage = inStore;
            player.GetComponent<WeaponFireRestriction>().SetFireRestriction(inStore);
        }
    }
}
