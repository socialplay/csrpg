﻿using UnityEngine;
using System.Collections;
using System;

public class JumpBacktoLoginInEditor : MonoBehaviour
{

    void Awake()
    { 
        if (Application.isEditor && ItemSystemGameData.UserID == Guid.Empty)
        {
            Application.LoadLevel(0);
        }
    }
}
