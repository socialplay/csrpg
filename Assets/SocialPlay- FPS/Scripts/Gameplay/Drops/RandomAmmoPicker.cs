﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Randomly picks ammo set in string arrays in this class.
/// </summary>
public class RandomAmmoPicker
{
    private SPRandomNumberGenerator spRng = new SPRandomNumberGenerator();

    string[] common = new string[] { "Pistol", "SubmachineGun" };
    string[] good = new string[] { "AssaultRifle", "Shotgun", "SniperRifle" };
    string[] strong = new string[] { "GrenadeLauncher", "RPG" };

    List<string> ammoTypes = new List<string>();

    public string getRandomAmmoType()
    {
        ammoTypes = getAmmoTypeCatagory();

        byte result = spRng.GetRandomGeneratedNumber((byte)ammoTypes.Count);

        return ammoTypes[result - 1];
    }

    List<string> getAmmoTypeCatagory()
    {
        byte result = spRng.GetRandomGeneratedNumber((byte)16);

        if (result >= 15)
        {
            return transferArrayToList(strong);
        }
        else if (result >= 9 && result <= 14)
        {
            return transferArrayToList(good);
        }

        return transferArrayToList(common);
    }

    List<string> transferArrayToList(string[] selectedAmmoTypes)
    {
        List<string> transferredAmmoTypes = new List<string>();

        for (int i = 0; i < selectedAmmoTypes.Length; i++)
        {
            transferredAmmoTypes.Add(selectedAmmoTypes[i]);
        }

        return transferredAmmoTypes;
    }
}
