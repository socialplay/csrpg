using UnityEngine;
using System.Collections;
using SocialPlay.ItemSystems;
using System.Collections.Generic;

public class ItemPickup : MonoBehaviour
{
    public float reachDistance = 2;
    public GameObject sfxPickupPrefab;
    public GameObject sfxCannotPickupPrefab;

    ItemData item;
    GameItemContainerInserter itemInserter;
    LimitedItemContainer inventory;
    BoxCollider bCollider;

    WhoKilledWho networkMessenger;

    Transform player;

    AutoTimer pickUpCounter;

    bool pickedUp = false;
    bool playedError = false;

    int maxInventorySize = 0;
    int inventorySize = 0;

    void Start()
    {
        pickUpCounter = new AutoTimer(1);

        item = GetComponent<ItemData>();
        itemInserter = GetComponent<GameItemContainerInserter>();
        inventory = UIReference.Instance.inventory.GetComponent<LimitedItemContainer>();
        bCollider = GetComponent<BoxCollider>();

        maxInventorySize = inventory.containerMaxSize;

        networkMessenger = GameObject.FindGameObjectWithTag("Network").GetComponent<WhoKilledWho>();

        RoomObjects.AddItemDrop(gameObject);
    }

    void Update()
    {
        if (player == null)
        {
            if (GameObject.FindObjectOfType<PlayerData>() != null)
            {
                player = GameObject.FindObjectOfType<PlayerData>().transform;
            }

            return;
        }

        inventorySize = inventory.containerItems.Count;

        float distance = Vector3.Distance(player.transform.position, transform.position);

        if (distance < reachDistance && !pickedUp && bCollider.isTrigger)
        {
            pickedUp = true;

            if (inventorySize > (maxInventorySize - 1))
            {
                cannotPickUpItem();
            }
            else
            {
                pickUpItem(GetAppropriateItemContainer(item));
            }
        }
    }

    void cannotPickUpItem()
    {
        if (!playedError)
        {
            playedError = true;

            Instantiate(sfxCannotPickupPrefab);
        }

        if (pickUpCounter.IsDone())
        {
            pickedUp = false;
            playedError = false;

            pickUpCounter.Reset();
        }
    }

    void pickUpItem(ItemContainer container)
    {
        itemInserter.container = container;

        itemInserter.PutGameItem(new List<ItemData>(new ItemData[1] { item }));

        NetworkMessageDisplay.AddRecivedItemMessage(item, string.Empty);

        RoomObjects.RemoveItemDrop(gameObject);

        Instantiate(sfxPickupPrefab);
    }

    public ItemContainer GetAppropriateItemContainer(ItemData item)
    {
        ItemContainer appropriateItemContainer = UIReference.Instance.inventory.itemContainer;

        if (UIReference.Instance.equipment.itemContainer.GetContainerAddState(item).actionState == ContainerAddState.ActionState.Add)
        {
            appropriateItemContainer = UIReference.Instance.equipment.itemContainer;
        }

        return appropriateItemContainer;
    }
}
