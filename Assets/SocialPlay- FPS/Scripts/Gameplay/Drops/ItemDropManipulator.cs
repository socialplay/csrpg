﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(BoxCollider))]
public class ItemDropManipulator : MonoBehaviour
{
    public float transparency = 0.6f;

    public List<Renderer> itemRenderers = new List<Renderer>();

    AutoTimer countdownTimer;

    internal bool hasStoppedMovement = false;

    Color alpha;

    void Start()
    {
        initializeItemRenderer();

        countdownTimer = new AutoTimer(1);
        hasStoppedMovement = false;

        Vector3 force = new Vector3(Random.Range(-10, 10), 30, Random.Range(-10, 10));
        GetComponent<Rigidbody>().AddForce(force * 10);
    }

    void OnCollisionStay(Collision collision)
    {
        if (countdownTimer.IsDone())
        {
            hasStoppedMovement = true;
        }
    }

    void LateUpdate()
    {
        transform.Rotate(0, 0.8f, 0);

        if (itemRenderers.Count <= 0) return;

        if (hasStoppedMovement)
        {
            for (int i = 0; i < itemRenderers.Count; i++)
            {
                modifyAlpha(itemRenderers[i], 1);
            }

            hasStoppedMovement = false;
            Destroy(gameObject.rigidbody);
            GetComponent<BoxCollider>().isTrigger = true;
            gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
        }
    }

    void initializeItemRenderer()
    {
        if (itemRenderers.Count <= 0) return;

        Shader newShader = Shader.Find("Transparent/VertexLit with Z");

        for (int i = 0; i < itemRenderers.Count; i++)
        {
            itemRenderers[i].material.shader = newShader;

            modifyAlpha(itemRenderers[i], transparency);
        }
    }

    void modifyAlpha(Renderer renderer, float amount)
    {
        alpha = renderer.material.color;
        alpha.a = amount;

        renderer.material.color = alpha;
    }

    public void SetItemRenderer(GameObject item)
    {
        for (int p = 0; p < item.GetComponent<WeaponDropComponents>().renderers.Length; p++)
        {
            itemRenderers.Add(item.GetComponent<WeaponDropComponents>().renderers[p]);
        }

        initializeItemRenderer();
    }
}
