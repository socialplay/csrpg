﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AmmoDropper : Photon.MonoBehaviour
{
    public GameObject AmmoPrefab;
    public GameObject AmmoUIDisplayPrefab;
    public Transform displayAnchor;

    List<ReceivedAmmoInfo> receivedAmmoInfo = new List<ReceivedAmmoInfo>();

    void OnEnable()
    {
        DisplayReceivedAmmo.eventDisplayComplete += displayReceivedAmmo_eventDisplayComplete;
    }
    void OnDisable()
    {
        DisplayReceivedAmmo.eventDisplayComplete -= displayReceivedAmmo_eventDisplayComplete;
    }

    void displayReceivedAmmo_eventDisplayComplete(DisplayReceivedAmmo ammoDisplay)
    {
        ReceivedAmmoInfo previousInfo = ammoDisplay._receivedAmmoInfo;

        for (int i = 0; i < receivedAmmoInfo.Count; i++)
        {
            if (receivedAmmoInfo[i].ammoType == previousInfo.ammoType)
            {
                if (receivedAmmoInfo[i].ammoAmount == previousInfo.ammoAmount)
                {
                    receivedAmmoInfo.Remove(receivedAmmoInfo[i]);
                    break;
                }
            }
        }

        if (receivedAmmoInfo.Count > 0)
        {
            displayReceivedAmmoAmount(receivedAmmoInfo[0]);
        }
    }

    void Start()
    {
        if (displayAnchor == null) displayAnchor = AmmoReference.GetDisplayAnchor();
    }

    void displayReceivedAmmoAmount(ReceivedAmmoInfo ammoInfo)
    {
        GameObject ammoUI = (GameObject)Instantiate(AmmoUIDisplayPrefab);
        ammoUI.name = ammoInfo.ammoType + " Ammo";
        ammoUI.transform.parent = displayAnchor;
        ammoUI.GetComponent<DisplayReceivedAmmo>().SetDisplayedAmmoInfo(ammoInfo.ammoType, ammoInfo.ammoAmount);
    }

    /// <summary>
    /// Stores ammo info then displays first ammo received.
    /// </summary>
    /// <param name="ammoType">Type of ammo received.</param>
    /// <param name="ammo">Amount of ammo received.</param>
    public void InitializeReceivedAmmoDisplay(string ammoType, int ammo)
    {
        ReceivedAmmoInfo ammoInfo = new ReceivedAmmoInfo();
        ammoInfo.ammoType = ammoType;
        ammoInfo.ammoAmount = ammo;

        receivedAmmoInfo.Add(ammoInfo);

        if (receivedAmmoInfo.Count <= 1)
        {
            displayReceivedAmmoAmount(ammoInfo);
        }
    }

    /// <summary>
    /// Instantiates the ammo item drop.
    /// </summary>
    /// <param name="dropPoint">Ammo dropped position.</param>
    public void InstantiateAmmoItemDrop(Vector3 dropPoint)
    {
        dropPoint += Vector3.up;

        photonView.RPC("instantiateAmmoItemDrop", PhotonTargets.MasterClient, dropPoint);
    }

    [RPC]
    void instantiateAmmoItemDrop(Vector3 dropPoint)
    {
        GameObject ammoDrop = (GameObject)PhotonNetwork.InstantiateSceneObject(AmmoPrefab.name, dropPoint, Quaternion.identity, 0, null);
        ammoDrop.transform.parent = transform;
    }
}
