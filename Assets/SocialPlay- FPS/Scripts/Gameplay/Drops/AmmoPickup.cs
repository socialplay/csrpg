﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AmmoPickup : Photon.MonoBehaviour
{
    /// <summary>
    /// A position in game that will hold the gameObject, away from users.
    /// </summary>
    public int ReceivedAmmoPercentage = 12;

    /// <summary>
    /// The amount of time it will be in the game before it destroys itself.
    /// </summary>
    public float ObjectLifeTime = 30;

    public float reachDistance = 1;

    public GameObject sfxPickupPrefab;
    public GameObject sfxAmmoFullPrefab;

    RandomAmmoPicker ammoPicker = new RandomAmmoPicker();
    AmmoDropper ammoDropper = new AmmoDropper();
    AutoTimer selfDestructTimer;
    AutoTimer playErrorSFX;
    Transform player;
    BoxCollider bCollider;

    string ammoType = "Pistol";

    bool setToDestroy = false;
    bool playedErrorSound = false;

    void Awake()
    {
        bCollider = GetComponent<BoxCollider>();
        ammoDropper = GameObject.FindObjectOfType<AmmoDropper>();

        RoomObjects.AddItemDrop(gameObject);

        selfDestructTimer = new AutoTimer(ObjectLifeTime);
        playErrorSFX = new AutoTimer(1);

        //set the type of ammo this pick up is giving.
        ammoType = ammoPicker.getRandomAmmoType();
    }

    void Update()
    {
        if (selfDestructTimer.IsDone())
        {
            callDestroyAmmoObject();
        }

        if (player == null)
        {
            if (GameObject.FindObjectOfType<PlayerData>() != null)
            {
                player = GameObject.FindObjectOfType<PlayerData>().transform;
            }

            return;
        }

        float distance = Vector3.Distance(player.transform.position, transform.position);

        if (distance < reachDistance && !setToDestroy && bCollider.isTrigger)
        {
            setToDestroy = true;

            addAmmoToPlayer(player.transform.root.gameObject);
        }
    }

    void addAmmoToPlayer(GameObject player)
    {
        if (player.GetComponent<PhotonView>().isMine)
        {
            WeaponSlotAmmo ammoInfo = AmmoReference.GetWeaponSlotAmmo(ammoType);

            if (ammoInfo.TotalAmmoReserve != ammoInfo.MaxAmmoReserve)
            {
                giveAmmo(ammoInfo);
            }
            else
            {
                cannotGiveAmmo();
            }
        }
    }

    void giveAmmo(WeaponSlotAmmo ammoInfo)
    {
        int ammoToBeAdded = ammoInfo.CalculateAmountOfAmmoToAdd(ReceivedAmmoPercentage);

        ammoDropper.InitializeReceivedAmmoDisplay(ammoType, ammoToBeAdded);
        ammoInfo.AddAmmo(ammoToBeAdded);

        Instantiate(sfxPickupPrefab);

        callDestroyAmmoObject();
    }

    void cannotGiveAmmo()
    {
        ammoDropper.InitializeReceivedAmmoDisplay(ammoType, 0);

        if (!playedErrorSound)
        {
            Instantiate(sfxAmmoFullPrefab);
        }
        else
        {
            if (playErrorSFX.IsDone())
            {
                playedErrorSound = false;
            }
        }
    }

    void callDestroyAmmoObject()
    {
        photonView.RPC("destroyAmmoObject", PhotonTargets.MasterClient);
    }

    [RPC]
    void destroyAmmoObject()
    {
        RoomObjects.RemoveItemDrop(gameObject);

        PhotonNetwork.Destroy(gameObject);
    }
}
