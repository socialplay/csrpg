﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ItemPickup))]
[RequireComponent(typeof(ItemDropManipulator))]
public class ItemDropWeaponSelector : MonoBehaviour
{
    public GameObject[] weaponModels = new GameObject[7];

    ItemDropManipulator itemDropManipulator;
    ItemData itemData;

    void Start()
    {
        itemDropManipulator = GetComponent<ItemDropManipulator>();
        itemData = GetComponent<ItemData>();

        GetWeaponModelAccordingToItemDataTag();
    }

    void GetWeaponModelAccordingToItemDataTag()
    {
        foreach (string tag in itemData.tags)
        {
            switch (tag)
            {
                case "Pistol":
                    InitWeaponModel(0);
                    return;
                case "SubmachineGun":
                    InitWeaponModel(1);
                    break;
                case "Shotgun":
                    InitWeaponModel(2);
                    break;
                case "AssaultRifle":
                    InitWeaponModel(3);
                    break;
                case "SniperRifle":
                    InitWeaponModel(4);
                    break;
                case "GrenadeLauncher":
                    InitWeaponModel(5);
                    break;
                case "RPG":
                    InitWeaponModel(6);
                    break;
                case "Grenade":
                    InitWeaponModel(7);
                    break;
            }
        }
    }

    void InitWeaponModel(int weaponIndex)
    {
        GameObject weapon = (GameObject)Instantiate(weaponModels[weaponIndex], transform.position, weaponModels[weaponIndex].transform.rotation);
        weapon.transform.parent = transform;

        if (itemData.quality > 1)
        {
            weapon.GetComponent<WeaponDropComponents>().changeParticleColor(ItemQuailityColorSelector.GetColorQuality(itemData.quality)); 
        }

        GetComponent<ItemPickup>().sfxPickupPrefab = weapon.GetComponent<WeaponDropComponents>().sfxPrefab;
        itemDropManipulator.SetItemRenderer(weapon);
    }
}
