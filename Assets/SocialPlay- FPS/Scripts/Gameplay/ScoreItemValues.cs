﻿using UnityEngine;
using System.Collections;

public class ScoreItemValues : MonoBehaviour
{

    const int DefaultValue = 2500;

    static ScoreItemValues instance;

    public int killValue;
    public int deathValue;
    public int baseValue;



    void Awake()
    {
        instance = this;
    }

    public static int GetPlayersScoreValue(PhotonPlayer player)
    {
        if (instance == null) return DefaultValue;

        KillDeathValueReader.KDR kdr = KillDeathValueReader.GetKDRForPlayer(player);

        int value = kdr.kills * instance.killValue + kdr.deaths * instance.deathValue + instance.baseValue;
        if (value <= instance.baseValue)
        {
            value = instance.baseValue;
        }
        return value;
    }

}
