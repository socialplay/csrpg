﻿using UnityEngine;
using System;
using System.Collections;

public class LogoutCleanUp : MonoBehaviour
{
    void OnEnable()
    {
        SPLogout.SPUserLogout += SPLogout_SPUserLogout;
    }
    void OnDisable()
    {
        SPLogout.SPUserLogout -= SPLogout_SPUserLogout;
    }

    void SPLogout_SPUserLogout()
    {
        DontDestroyOnLoad[] objectsToDestoy = GameObject.FindObjectsOfType<DontDestroyOnLoad>();

        foreach (DontDestroyOnLoad objectToDestroy in objectsToDestoy)
        {
            Destroy(objectToDestroy.gameObject);
        }

        Application.LoadLevel(0);
    }

    void Awake()
    {
        SPLogin spLogin = GameObject.FindObjectOfType<SPLogin>();

        if (!spLogin.enabled)
        {
            GetComponent<SPLogout>().logoutButton.SetActive(false);
        }
    }
}
