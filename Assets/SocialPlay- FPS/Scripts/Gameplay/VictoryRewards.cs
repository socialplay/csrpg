﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//[RequireComponent(typeof(PhotonView))]

public class VictoryRewards : Photon.MonoBehaviour
{

    public int TDMEnergy = 3000;
    public int DMEnergy = 5000;

    public GameObject getterObject;
    GameItemContainerInserter inserter;
    ItemGetter getter;
    WhoKilledWho whoKilledWho;
    LimitedItemContainer inventory;

    static VictoryRewards instance = null;

    void Awake()
    {
        instance = this;
        whoKilledWho = GameObject.FindObjectOfType<WhoKilledWho>();
        inventory = GameObject.FindObjectOfType<LimitedItemContainer>();
        getter = getterObject.GetComponent<ItemGetter>();
        inserter = getterObject.GetComponent<GameItemContainerInserter>();
        inserter.container = inventory;
    }

    void OnEnable()
    {
        inserter.onReciveItems += inserter_onReciveItems;
    }


    void OnDisable()
    {
        inserter.onReciveItems -= inserter_onReciveItems;
    }

    void inserter_onReciveItems(System.Collections.Generic.List<ItemData> obj)
    {
        obj.ForEach(delegate(ItemData item)
        {
            NetworkMessageDisplay.AddRecivedItemMessage(item, " as a reward");
        });
    }



    public static void TDMVictory(string teamName)
    {
        if (teamName == "")
        {
            instance.getter.MaxEnergy = instance.TDMEnergy / 2;
            instance.GiveReward();
            KongregateStatsTracker.PostAction("TDMDraw", 1);
        }
        else if (PhotonNetwork.player.customProperties["TeamName"].ToString() == teamName)
        {
            instance.getter.MaxEnergy = instance.TDMEnergy;
            instance.GiveReward();
            KongregateStatsTracker.PostAction("TDMVictory", 1);
        }
    }

    public static void DMVictory(int NetworkID)
    {
        if (PhotonNetwork.player.ID == NetworkID)
        {
            instance.getter.MaxEnergy = instance.DMEnergy;
            instance.GiveReward();
            KongregateStatsTracker.PostAction("DMVictory", 1);
        }
    }

    void GiveReward()
    {
        getter.GetItems();

        ClearRoundItems();
    }


    void ClearRoundItems()
    {
        GameObject[] itemDropObjects = GameObject.FindGameObjectsWithTag("ItemDrop");

        for (int i = 0; i < itemDropObjects.Length; i++)
        {
            Destroy(itemDropObjects[i]);
        }
    }

}
