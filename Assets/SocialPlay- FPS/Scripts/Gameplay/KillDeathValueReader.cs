﻿using UnityEngine;
using System.Collections;

public class KillDeathValueReader : MonoBehaviour
{

    public class KDR
    {
        public int kills;
        public int deaths;
        public bool isTeamMVP;
        public bool isMVP;


        public KDR(int k, int d, bool TMVP, bool MVP)
        {
            kills = k;
            deaths = d;
            isTeamMVP = TMVP;
            isMVP = MVP;
        }
    }

    public static KDR GetKDRForPlayer(PhotonPlayer player)
    {

        int k = (int)player.customProperties["Kills"];
        int d = (int)player.customProperties["Deaths"];
        bool isMVP = true;
        bool isTMVP = true;
        for (int i = 0; i < PhotonNetwork.playerList.Length; i++)
        {
            if ((int)PhotonNetwork.playerList[i].customProperties["Kills"] < k)
            {
                isMVP = false;
                if ((string)PhotonNetwork.playerList[i].customProperties["TeamName"] == (string)PhotonNetwork.player.customProperties["TeamName"])
                {
                    isTMVP = false;
                    break;
                }
            }
        }


        return new KDR(k, d, isTMVP, isMVP);
    }


    public static float GetKDRValueFromPlayer(PhotonPlayer player)
    {
        float value = 1;
        KDR kdr = GetKDRForPlayer(player);
        if (kdr.deaths == 0)
        {
            if (kdr.kills == 0)
            {
                value = 1;
            }
            value = kdr.kills;
        }
        else
        {
            value = kdr.kills / kdr.deaths;
        }
        if (value < 1)
            value = 1;
        return value;
    }

}
