﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(SlotData))]
public class WeaponSlot : MonoBehaviour
{
    static List<WeaponSlot> allWeaponSlots = new List<WeaponSlot>();

    public string weaponName = "Enter Weapon Name";

    WeaponScript weapon;

    SlottedItemContainer equipment;

    internal SlottedContainerSlotData equipmentSlot;

    void Awake()
    {
        allWeaponSlots.Add(this);
        equipment = transform.parent.parent.GetComponent<SlottedItemContainer>();
    }

    void OnEnable()
    {
        PlayerData.eventPlayerSpawned += playerData_eventPlayerSpawned;
        equipment.ClearItems += itemContainer_ClearItems;
    }
    void OnDisable()
    {
        PlayerData.eventPlayerSpawned -= playerData_eventPlayerSpawned;
        equipment.ClearItems -= itemContainer_ClearItems;

        if (PlayerData.weaponManager != null)
        {
            PlayerData.weaponManager.eventFinishedSwitching -= weaponManager_eventFinishedSwitching;
        }

        if (equipmentSlot != null)
        {
            equipmentSlot.OnItemChanged -= equipmentSlot_OnItemChanged;
        }
    }

    void itemContainer_ClearItems()
    {
        if (PlayerData.weaponManager == null) return;

        PlayerData.weaponManager.index = 0;
        PlayerData.weaponManager.allWeapons.Remove(weapon);
    }

    void playerData_eventPlayerSpawned(PlayerData pd)
    {
        PlayerData.weaponManager.eventFinishedSwitching -= weaponManager_eventFinishedSwitching;
        int slotId = this.GetComponent<SlotData>().slotID;

        equipmentSlot = equipment.slots[slotId];

        setWeapon();

        if (equipmentSlot.slotData != null)
        {
            updateWeapon(false);
        }

        equipmentSlot.OnItemChanged += equipmentSlot_OnItemChanged;
        PlayerData.weaponManager.eventFinishedSwitching += weaponManager_eventFinishedSwitching;
    }

    void setWeapon()
    {
        for (int aw = 0; aw < PlayerData.weaponManager.availableWeapons.Length; aw++)
        {
            if (PlayerData.weaponManager.availableWeapons[aw].weaponName == weaponName)
            {
                weapon = PlayerData.weaponManager.availableWeapons[aw];
            }
        }
    }

    void equipmentSlot_OnItemChanged()
    {
        if (equipmentSlot.slotData == null)
        {
            updateWeapon(false);
        }
        else
        {
            updateWeapon(true);
        }
    }

    void updateWeapon(bool isAutoEquippedWeapon)
    {
        manageAllWeapons();

        if (isAutoEquippedWeapon)
        {
            switchWeapons();
        }

        PlayerData.weaponManager.allWeapons[PlayerData.weaponManager.allWeapons.Count - 1].canFire = !PauseGamePlay.IsMenuOpen();
    }

    void manageAllWeapons()
    {
        if (equipmentSlot.slotData == null)
        {
            PlayerData.weaponManager.allWeapons.Remove(weapon);
            switchWeapons();
        }
        else
        {
            if (!PlayerData.weaponManager.allWeapons.Contains(weapon))
            {
                PlayerData.weaponManager.allWeapons.Add(weapon);
            }

            updateWeaponStats(weapon, equipmentSlot.slotData.stats);
        }
    }

    void updateWeaponStats(WeaponScript modWeapon, Dictionary<string, float> stats)
    {
        switch (modWeapon.GunType)
        {
            case WeaponScript.gunType.MACHINE_GUN:
                modWeapon.machineGun.fireRate = stats["Fire Rate"];
                break;
            case WeaponScript.gunType.GRENADE_LAUNCHER:
                modWeapon.grenadeLauncher.reloadTime = stats["Fire Delay"];
                break;
            case WeaponScript.gunType.SHOTGUN:
                modWeapon.ShotGun.fireRate = stats["Fire Rate"];
                break;
        }
    }

    void switchWeapons()
    {
        if (PlayerData.weaponManager.canSwitch)
        {
            PlayerData.weaponManager.index = PlayerData.weaponManager.allWeapons.Count - 1;
            StartCoroutine(PlayerData.weaponManager.SwitchWeapons(PlayerData.weaponManager.SelectedWeapon.gameObject,
                PlayerData.weaponManager.allWeapons[PlayerData.weaponManager.allWeapons.Count - 1].gameObject));
        }
    }

    void weaponManager_eventFinishedSwitching()
    {
        PlayerData.weaponManager.allWeapons.ForEach(delegate(WeaponScript ws)
        {
            if (ws != PlayerData.weaponManager.SelectedWeapon)
            {
                ws.gameObject.SetActive(false);
            }
        });
    }

    /// <summary>
    /// Get the currently active weapon.
    /// </summary>
    /// <returns>WeaponSlot component of the active weapon.</returns>
    public static WeaponSlot GetActiveWeapon()
    {
        WeaponSlot selectedSlot = null;

        allWeaponSlots.ForEach(delegate(WeaponSlot ws)
        {
            if (PlayerData.weaponManager.SelectedWeapon.weaponName == ws.weaponName)
            {
                selectedSlot = ws;
            }
        });

        return selectedSlot;
    }

    /// <summary>
    /// Gets the weapon damage according to the weapon stat.
    /// </summary>
    /// <param name="weaponSlot">WeaponSlot component of the weapon you want the damage stat.</param>
    /// <returns>Weapon damage of the passed weapon.</returns>
    public static float GetWeaponDamage(WeaponSlot weaponSlot)
    {
        float damage = 0;

        switch (weaponSlot.weapon.GunType)
        {
            case WeaponScript.gunType.SHOTGUN:
                float sgDamage = weaponSlot.equipmentSlot.slotData.stats["Damage"];
                damage = sgDamage / weaponSlot.weapon.ShotGun.fractions;
                break;
            default:
                damage = weaponSlot.equipmentSlot.slotData.stats["Damage"];
                break;
        }

        return damage;
    }
}
