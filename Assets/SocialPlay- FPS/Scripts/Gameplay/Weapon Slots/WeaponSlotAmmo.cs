﻿using UnityEngine;
using System.Collections;

public class WeaponSlotAmmo : MonoBehaviour
{
    public UILabel ammoLabel;

    //MaxAmmoReserve is only used to check the maximum a player can hold.
    public int MaxAmmoReserve = 100;
    public int AmmoReserveAtStart = 30;
    public int ClipAmmoAtStart = 10;

    internal int TotalAmmoReserve = 0;

    WeaponScript weapon;

    void OnEnable()
    {
        PlayerData.eventPlayerSpawned += PlayerData_eventPlayerSpawned;
    }
    void OnDisable()
    {
        PlayerData.eventPlayerSpawned -= PlayerData_eventPlayerSpawned;
    }

    void PlayerData_eventPlayerSpawned(PlayerData pd)
    {
        SetThisWeapon();
    }

    void SetThisWeapon()
    {
        for (int aw = 0; aw < PlayerData.weaponManager.availableWeapons.Length; aw++)
        {
            if (PlayerData.weaponManager.availableWeapons[aw].weaponName == GetComponent<WeaponSlot>().weaponName)
            {
                weapon = PlayerData.weaponManager.availableWeapons[aw];
            }
        }

        SetWeaponAmmo();
    }

    //Set the initial value for ammo in the WeaponScript.
    void SetWeaponAmmo()
    {
        switch (weapon.GunType)
        {
            case WeaponScript.gunType.MACHINE_GUN:
                weapon.machineGun.bulletsPerClip = ClipAmmoAtStart;
                weapon.machineGun.bulletsLeft = ClipAmmoAtStart;
                weapon.machineGun.clips = AmmoReserveAtStart;
                break;
            case WeaponScript.gunType.GRENADE_LAUNCHER:
                weapon.grenadeLauncher.ammoCount = AmmoReserveAtStart;
                break;
            case WeaponScript.gunType.SHOTGUN:
                weapon.ShotGun.bulletsPerClip = ClipAmmoAtStart;
                weapon.ShotGun.bulletsLeft = ClipAmmoAtStart;
                weapon.ShotGun.clips = AmmoReserveAtStart;
                break;
        }
    }

    void Update()
    {
        if (weapon == null) return;

        UpdateAmmoReserve();
    }

    void UpdateAmmoReserve()
    {
        switch (weapon.GunType)
        {
            case WeaponScript.gunType.MACHINE_GUN:
                TotalAmmoReserve = weapon.machineGun.clips + weapon.machineGun.bulletsLeft;
                break;
            case WeaponScript.gunType.GRENADE_LAUNCHER:
                TotalAmmoReserve = weapon.grenadeLauncher.ammoCount;
                break;
            case WeaponScript.gunType.SHOTGUN:
                TotalAmmoReserve = weapon.ShotGun.clips + weapon.ShotGun.bulletsLeft;
                break;
        }

        ammoLabel.text = TotalAmmoReserve.ToString();
    }

    int CalculateAmmoToAdd(int totalAmmo, float ammoPercentage)
    {
        float calculatedAmmo = ammoPercentage * totalAmmo / 100;

        return Mathf.CeilToInt(calculatedAmmo);
    }

    /// <summary>
    /// Add ammo to the weapon.
    /// </summary>
    /// <param name="ammo">Amount of ammo to add.</param>
    public void AddAmmo(int ammo)
    {
        int additionalAmmo = ammo;

        switch (weapon.GunType)
        {
            case WeaponScript.gunType.MACHINE_GUN:
                weapon.machineGun.clips += additionalAmmo;
                break;
            case WeaponScript.gunType.GRENADE_LAUNCHER:
                weapon.grenadeLauncher.ammoCount += additionalAmmo;
                break;
            case WeaponScript.gunType.SHOTGUN:
                weapon.ShotGun.clips += additionalAmmo;
                break;
        }
    }

    /// <summary>
    /// Get ammo amount that will be added to the weapon.
    /// </summary>
    /// <param name="receivedAmmoPercentage">The percentage the will be calculated from the weapon's maximum ammo.</param>
    public int CalculateAmountOfAmmoToAdd(int receivedAmmoPercentage)
    {
        int additionalAmmo = CalculateAmmoToAdd(MaxAmmoReserve, receivedAmmoPercentage);
        int estimatedAmmo = TotalAmmoReserve + additionalAmmo;

        if (estimatedAmmo > MaxAmmoReserve)
        {
            additionalAmmo = MaxAmmoReserve - TotalAmmoReserve;
        }

        return additionalAmmo;
    }
}
