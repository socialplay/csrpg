﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponSlotGrenade : MonoBehaviour
{
    public LimitedItemContainer inventory;
    public SlottedItemContainer equipment;
    public UILabel AmmoLabel;

    bool canThrow = false;
    bool grenadeThrown = false;
    bool canGetNewGrenade = false;

    AutoTimer thrownTimer;

    WeaponScript grenade;

    SlottedContainerSlotData equipmentSlot;

    WeaponManager weaponManager;


    void OnEnable()
    {
        PlayerData.eventPlayerSpawned += PlayerData_eventPlayerSpawned;
    }
    void OnDisable()
    {
        PlayerData.eventPlayerSpawned -= PlayerData_eventPlayerSpawned;
    }

    void PlayerData_eventPlayerSpawned(PlayerData pd)
    {
        weaponManager = PlayerData.weaponManager;

        for (int aw = 0; aw < PlayerData.weaponManager.availableWeapons.Length; aw++)
        {
            if (PlayerData.weaponManager.availableWeapons[aw].weaponName == "Grenade")
            {
                grenade = PlayerData.weaponManager.availableWeapons[aw];
            }
        }

        if (thrownTimer == null)
        {
            thrownTimer = new AutoTimer(grenade.grenadeLauncher.waitBeforeReload);
        }

        //set slot data container.
        int slotId = this.GetComponent<SlotData>().slotID;
        equipmentSlot = equipment.slots[slotId];

        updateAmmo();
        canThrow = true;
    }

    void callRemoveEquipmentItem()
    {
        equipmentSlot.slotData.ownerContainer.Remove(equipmentSlot.slotData, false, 1);
        canGetNewGrenade = true;
    }

    void updateAmmo()
    {
        int grenadeCount = 0;

        foreach (ItemData item in inventory.containerItems)
        {
            if (item.tags.Contains("Grenade"))
            {
                grenadeCount += item.stackSize;
            }
        }

        if (equipmentSlot != null && equipmentSlot.slotData != null)
        {
            grenadeCount += equipmentSlot.slotData.stackSize;
        }

        if (grenade) grenade.grenadeLauncher.ammoCount = grenadeCount;

        AmmoLabel.text = grenadeCount.ToString();
    }

    void Update()
    {
        if (grenade == null)
        {
            return;
        }

        updateAmmo();

        if (PauseGamePlay.IsMenuOpen()) return;

        if (equipmentSlot.slotData == null)
        {
            if (canGetNewGrenade)
            {
                getNextGrenade();
                canGetNewGrenade = false;
            }

            if (grenadeThrown)
            {
                waitForGrenadeThrow();
            }

            return;
        }

        if (!canThrow && grenadeThrown)
        {
            waitForGrenadeThrow();
            return;
        }

        if (!canThrow)
        {
            waitForGrenadReloadTime();
            return;
        }

        if (Input.GetKeyDown(KeyCode.G) && canThrow && !grenadeThrown && grenade.grenadeLauncher.ammoCount > 0)
        {
            canThrow = false;

            useEquippedGrenade();
        }
    }

    void getNextGrenade()
    {
        ItemData highestEnergyGrenade = null;
        int highestEnergy = 0;

        foreach (ItemData item in inventory.containerItems)
        {
            if (item.tags.Contains("Grenade") && item.totalEnergy > highestEnergy)
            {
                highestEnergy = item.totalEnergy;
                highestEnergyGrenade = item;
            }
        }

        if (highestEnergyGrenade != null)
        {
            ItemContainerManager.MoveItem(highestEnergyGrenade, equipment);
        }
        else
        {
            waitForGrenadeThrow();
            canGetNewGrenade = true;
        }
    }

    void useEquippedGrenade()
    {
        displayGrenadeHands(true);

        //changed grenadeLauncherOneShot to public from private method in WeaponScript.
        StartCoroutine(grenade.grenadeLauncherOneShot());

        callRemoveEquipmentItem();

        grenadeThrown = true;
    }

    void waitForGrenadeThrow()
    {
        if (thrownTimer.IsDone())
        {
            displayGrenadeHands(false);

            grenadeThrown = false;
            thrownTimer.Reset(grenade.grenadeLauncher.reloadTime);
        }
    }

    void waitForGrenadReloadTime()
    {
        if (thrownTimer.IsDone())
        {
            canThrow = true;
            thrownTimer.Reset(grenade.grenadeLauncher.waitBeforeReload);
        }
    }

    void displayGrenadeHands(bool show)
    {
        weaponManager.SelectedWeapon.gameObject.SetActive(!show);
        grenade.gameObject.SetActive(show);
    }
}
