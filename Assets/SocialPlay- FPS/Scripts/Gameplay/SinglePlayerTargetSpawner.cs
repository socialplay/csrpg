﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(PhotonView))]
public class SinglePlayerTargetSpawner : Photon.MonoBehaviour
{

    public static int currentTargetCount = 0;

    public float delayAtStart = 2;
    public float respawnDelay = 20;
    public int maxTargetsCount = 10;

    public GameObject targetPrefab;
    public Transform spawnPointParent;
    public Transform targetParent;

    public List<SpawnPoint> possibleSpawns = new List<SpawnPoint>();

    public bool ShowPoints = false;

    public List<SpawnTimmer> currentTimmers = new List<SpawnTimmer>();

    private static SinglePlayerTargetSpawner instance = null;




    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

        possibleSpawns.Clear();

        for (int i = 0; i < spawnPointParent.childCount; i++)
        {
            if (!ShowPoints || !Application.isEditor)
            {
                foreach (Renderer rend in spawnPointParent.GetChild(i).GetComponentsInChildren<Renderer>())
                {
                    rend.enabled = false;
                }
            }
            SpawnPoint point = new SpawnPoint(i, spawnPointParent.GetChild(i).localPosition);
            possibleSpawns.Add(point);
        }
        if (possibleSpawns.Count == 0)
        {
            Debug.LogError("Can not sapwn targets with no spawn points (consider adding gameObjects to Spawn Point Parent)");
            this.enabled = false;
            return;
        }

        if (maxTargetsCount > possibleSpawns.Count)
        {
            Debug.LogWarning("Can not have more dummies then spawn points (Setting max Targets Count to " + (possibleSpawns.Count) + ")");
            maxTargetsCount = possibleSpawns.Count;
        }

        if (maxTargetsCount == -1)
        {
            maxTargetsCount = possibleSpawns.Count;
        }
        float deltaDelay = 0;
        for (int i = 0; i < maxTargetsCount; i++)
        {
            currentTimmers.Add(new SpawnTimmer(deltaDelay));
            deltaDelay += delayAtStart;
        }
    }

    void Update()
    {
        if (!PhotonNetwork.isMasterClient || (PhotonNetwork.room != null && PhotonNetwork.room.playerCount > 1))
        {
            return;
        }

        for (int i = 0; i < currentTimmers.Count; i++)
        {
            if (currentTimmers[i].RunTimmer())
            {
                currentTimmers.RemoveAt(i);
                SpawnNewTarget();
                return;
            }
        }
    }

    public void SpawnNewTarget()
    {
        instance.photonView.RPC("NetworkMasterSpawnTarget", PhotonTargets.AllBuffered);
    }

    public static void RemoveTarget(int targetDestroyed)
    {
        instance.photonView.RPC("NetworkMasterRemoveTarget", PhotonTargets.AllBuffered, targetDestroyed);
    }



    [RPC]
    void NetworkMasterSpawnTarget()
    {
        if (!PhotonNetwork.isMasterClient) return;
        SpawnTarget();
    }


    [RPC]
    void NetworkMasterRemoveTarget(int targetDestroyed)
    {
        if (!PhotonNetwork.isMasterClient) return;
        if (targetDestroyed != -1)
        {
            possibleSpawns.ForEach(delegate(SpawnPoint point)
            {
                if (point.IsMyView(targetDestroyed))
                {
                    point.UnOccupy();
                }
            });
        }
        AddNewTimmer();
    }

    void AddNewTimmer()
    {
        currentTimmers.Add(new SpawnTimmer(respawnDelay));
    }

    void SpawnTarget()
    {
        SpawnPoint pickedPoint = GetRandomSpawnPoint();
        
        GameObject target = PhotonNetwork.InstantiateSceneObject(targetPrefab.name, SPHelper.GetRandomPointOnParameter(2, new Vector3(1, 0, 1), pickedPoint.possition), Quaternion.identity, 0, null);
        //AddObjectToTargetList(target.transform);
        pickedPoint.Occupy(target.GetComponent<PhotonView>().viewID);
    }

    SpawnPoint GetRandomSpawnPoint()
    {
        List<SpawnPoint> openPoints = possibleSpawns.FindAll(x => x.IsOccupied == false);
        int pickedIndex = Random.Range(0, openPoints.Count - 1);
        return openPoints[pickedIndex];
    }

    public static void AddObjectToTargetList(Transform target)
    {
        if (instance == null) return;

        target.parent = instance.targetParent;
    }

    [System.Serializable]
    public class SpawnTimmer
    {
        public string timeString ="0";
        public float currentTime;
        public float timeSpan;

        public SpawnTimmer(float runTime)
        {
            currentTime = 0;
            this.timeSpan = runTime;
        }

        public bool RunTimmer()
        {
            currentTime += Time.deltaTime;
            timeString = currentTime.ToString();
            if (currentTime >= timeSpan)
            {
                return true;
            }
            return false;
        }
    }


    public class SpawnPoint
    {
        public int index = 1;
        bool isOccupied = false;
        public bool IsOccupied { get { return isOccupied; } }
        int viewID = -1;
        public Vector3 possition;

        public SpawnPoint(int i, Vector3 pos)
        {
            this.index = i;
            this.isOccupied = false;
            this.viewID = -1;
            this.possition = pos;
        }


        public void Occupy(int viewID)
        {
            this.viewID = viewID;
            isOccupied = true;
        }

        public void UnOccupy()
        {
            isOccupied = false;
            viewID = -1;
        }

        public bool IsMyView(int ID)
        {
            if (this.viewID == -1)
                return false;
            return ID == this.viewID;
        }

    }

}
