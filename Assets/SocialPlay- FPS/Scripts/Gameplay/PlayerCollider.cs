﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerCollider : MonoBehaviour
{
    public float maxPlayerDetectDistance = 1;

    float initCharControllerStepOffset = 0;

    List<GameObject> collidedPlayers = new List<GameObject>();

    CharacterController charController;

    void OnEnable()
    {
        RoomObjects.OnNewItemAdded += RoomObjects_OnNewItemAdded;
    }
    void OnDisable()
    {
        RoomObjects.OnNewItemAdded -= RoomObjects_OnNewItemAdded;
    }

    void RoomObjects_OnNewItemAdded(GameObject itemDrop)
    {
        Physics.IgnoreCollision(itemDrop.collider, charController.collider);
    }

    void Start()
    {
        charController = GetComponent<CharacterController>();
        initCharControllerStepOffset = charController.stepOffset;

        IgnoreExsistingItemColliders();
    }

    void Update()
    {
        //getCollidedPlayers();

        //if (collidedPlayers.Count > 0)
        //{
        //    charController.stepOffset = 0;
        //}
        //else
        //{
        //    charController.stepOffset = initCharControllerStepOffset;
        //}
    }

    void IgnoreExsistingItemColliders()
    {
        List<GameObject> itemDrops = RoomObjects.GetPlayers();

        if (itemDrops.Count < 1) return;

        for (int i = 0; i < itemDrops.Count; i++)
        {
            if (itemDrops[i].collider != charController.collider)
            {
                Physics.IgnoreCollision(itemDrops[i].collider, charController.collider);
            }
        }
    }

    //void getCollidedPlayers()
    //{
    //    if (RoomObjects.GetPlayers().Count > 0)
    //    {
    //        List<GameObject> players = RoomObjects.GetPlayers();

    //        for (int p = 0; p < players.Count; p++)
    //        {
    //            float distance = Vector3.Distance(transform.position, players[p].transform.position);

    //            if (distance < maxPlayerDetectDistance)
    //            {
    //                collidedPlayers.Add(players[p]);
    //            }
    //            else
    //            {
    //                collidedPlayers.Remove(players[p]);
    //            }
    //        }
    //    }
    //}
}
