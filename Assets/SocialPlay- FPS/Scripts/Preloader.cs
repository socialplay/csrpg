﻿using UnityEngine;
using System;
using System.Collections;

public class Preloader : MonoBehaviour
{
    void Start()
    {
        StatsTracker.Init(new KongregateStatsTracker());        
#if UNITY_WEBPLAYER
        KongregateStatsTracker.PostAction("initialized", 1);
#endif
    }

    void Update()
    {
        if (ItemSystemGameData.UserID != Guid.Empty)
        {
            Application.LoadLevel(1);
        }
    }
}
