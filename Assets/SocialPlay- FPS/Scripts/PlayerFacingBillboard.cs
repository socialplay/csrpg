﻿using UnityEngine;
using System.Collections;

public class PlayerFacingBillboard : MonoBehaviour
{

    Camera cam;

    void OnEnable()
    {
        RoomMultiplayerMenu.PlayerSpawned += RoomMultiplayerMenu_PlayerSpawned;
    }

    void RoomMultiplayerMenu_PlayerSpawned(PlayerDamage obj)
    {
        cam = obj.GetComponentInChildren<Camera>();
    }

    void OnDisable()
    {
        RoomMultiplayerMenu.PlayerSpawned += RoomMultiplayerMenu_PlayerSpawned;
    }

    void Update()
    {
        if (cam != null)
        {
            transform.LookAt(transform.position + cam.transform.rotation * Vector3.back, cam.transform.rotation * Vector3.up);
        }
    }
}