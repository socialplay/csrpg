﻿using UnityEngine;
using System;
using System.Collections;

public class DisplayReceivedAmmo : MonoBehaviour
{
    public static event Action<DisplayReceivedAmmo> eventDisplayComplete;

    public UILabel ammoAmountDisplay;
    public UISprite ammoIcon;

    private ReceivedAmmoInfo receivedAmmoInfo = new ReceivedAmmoInfo();
    public ReceivedAmmoInfo _receivedAmmoInfo
    {
        get { return receivedAmmoInfo; }
    }

    void Start()
    {
        transform.localPosition = transform.position;
        transform.localScale = Vector3.one;
    }

    /// <summary>
    /// Callback for Tween Alpha OnFinished.
    /// </summary>
    public void AmmoDisplayComplete()
    {
        if (eventDisplayComplete != null)
        {
            eventDisplayComplete(this);
        }

        Destroy(gameObject, 0.1f);
    }

    /// <summary>
    /// Callback for Tween Position OnFinished.
    /// </summary>
    public void ActivateAlphaTween()
    {
        for (int i = 0; i < GetComponentsInChildren<UITweener>().Length; i++)
        {
            GetComponentsInChildren<UITweener>()[i].enabled = true;
        }
    }

    /// <summary>
    /// Set the info for the ui display of received ammo.
    /// </summary>
    /// <param name="ammoType">Type of ammo received that will be displayed.</param>
    /// <param name="ammoAmount">Amount of ammo received that will be displayed</param>
    public void SetDisplayedAmmoInfo(string ammoType, int ammoAmount)
    {
        // Set ammo sprite icon.
        string spriteName = ammoType + "_32px";
        ammoIcon.spriteName = spriteName.ToLower();

        receivedAmmoInfo.ammoType = ammoType;
        receivedAmmoInfo.ammoAmount = ammoAmount;

        int receivedAmmo = ammoAmount;

        if (receivedAmmo > 0)
        {
            ammoAmountDisplay.text = "x " + receivedAmmo.ToString();
        }
        else
        {
            ammoAmountDisplay.text = "MAX";
        }
    }
}


[System.Serializable]
public class ReceivedAmmoInfo
{
    public string ammoType;
    public int ammoAmount;
}