﻿using UnityEngine;
using System.Collections;
using System;
using Newtonsoft.Json.Linq;

public class CraftingDisplay : MonoBehaviour {

    public static bool isCraftingDisplayOpen = false;
    public GameObject buttonPanel;
    public GameObject displayCraftingPanelButton;
    public GameObject closeCraftingPanelButton;

    public GameObject craftingPanel;

    public GameObject getPINButton;
    public UILabel PINLabel;

    void OnEnable()
    {
        UIEventListener.Get(displayCraftingPanelButton).onClick += OnCraftingButtonPressed;
        UIEventListener.Get(closeCraftingPanelButton).onClick += OnCraftingClosePressed;
        UIEventListener.Get(getPINButton).onClick += OnGetPINButtonPressed;
    }

    void OnDisable()
    {
        UIEventListener.Get(displayCraftingPanelButton).onClick -= OnCraftingButtonPressed;
        UIEventListener.Get(closeCraftingPanelButton).onClick -= OnCraftingClosePressed;
        UIEventListener.Get(getPINButton).onClick -= OnGetPINButtonPressed;
    }

    void OnCraftingButtonPressed(GameObject go)
    {
        craftingPanel.SetActive(true);
        buttonPanel.SetActive(false);

        getPINButton.SetActive(true);
        PINLabel.gameObject.SetActive(false);

        CraftingDisplay.isCraftingDisplayOpen = true;
    }

    void OnCraftingClosePressed(GameObject go)
    {
        craftingPanel.SetActive(false);
        buttonPanel.SetActive(true);

        CraftingDisplay.isCraftingDisplayOpen = false;
    }

    void OnGetPINButtonPressed(GameObject go)
    {
        WebserviceCalls.webservice.GetAccessPinForUser(ItemSystemGameData.UserID.ToString(), OnReceivedAccessPIN);
    }

    void OnReceivedAccessPIN(string callbackString)
    {
        getPINButton.SetActive(false);
        PINLabel.gameObject.SetActive(true);

        try
        {
            JToken jObj = JToken.Parse(callbackString);
            JToken token = JToken.Parse(jObj.ToString());
            PINLabel.text = token.ToString();
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);

            PINLabel.text = "This user is not Authorized for a PIN";

        }
    }
}
