﻿using UnityEngine;
using System.Collections;

public class FitNGUIProgressBar : MonoBehaviour
{
    public UISprite background;
    public UISprite foreground;

    UISlider slider;

    void Start()
    {
        slider = GetComponent<UISlider>();

        setNewWidth();
    }

    void Update()
    {
        if (Screen.width != background.width)
        {
            setNewWidth();
        }
    }

    void setNewWidth()
    {
        background.width = Screen.width;
        foreground.width = Screen.width;

        updateSlider();
    }

    void updateSlider()
    {
        slider.fullSize = new Vector2(foreground.width, foreground.height);
    }
}
