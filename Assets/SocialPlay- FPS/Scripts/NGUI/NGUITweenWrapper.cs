﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(TweenPosition))]
public class NGUITweenWrapper : MonoBehaviour
{
    public enum Direction { horizontal, vertical, top, bottom, right, left };
    public Direction tweenDirection;
    public float tweenDuration;
    public int inDirPosition, outDirPosition;
    public bool isActive = false;

    private Vector3 hidePosition, visiblePosition, startPosition;
    private TweenPosition tween = null;
    private bool isCurrentlyActive = false;

    // Use this for initialization
    void Start()
    {
        startPosition = gameObject.transform.localPosition;
        UpdatePosition();

        if (isActive) gameObject.transform.localPosition = visiblePosition;
        else gameObject.transform.localPosition = hidePosition;

        if (tween == null)
        {
            tween = gameObject.GetComponent<TweenPosition>();
            tween.duration = tweenDuration;
            tween.enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isActive && !isCurrentlyActive)
        {
            UpdatePosition();
            tween.from = gameObject.transform.localPosition;
            tween.to = visiblePosition;
            tween.Reset();
            tween.enabled = true;
            isCurrentlyActive = true;
        }
        else if (!isActive && isCurrentlyActive)
        {
            UpdatePosition();
            tween.from = gameObject.transform.localPosition;
            tween.to = hidePosition;
            tween.Reset();
            tween.enabled = true;
            isCurrentlyActive = false;
        }
    }

    void UpdatePosition()
    {
        visiblePosition = hidePosition = startPosition;
        switch (tweenDirection)
        {
            case Direction.horizontal:
                visiblePosition.x = inDirPosition;
                hidePosition.x = outDirPosition;
                break;
            case Direction.vertical:
                visiblePosition.y = inDirPosition;
                hidePosition.y = outDirPosition;
                break;
            case Direction.top:
                visiblePosition.y = inDirPosition;
                hidePosition.y = inDirPosition + Screen.height;
                break;
            case Direction.bottom:
                visiblePosition.y = inDirPosition;
                hidePosition.y = inDirPosition - Screen.height;
                break;
            case Direction.right:
                visiblePosition.x = inDirPosition;
                hidePosition.x = inDirPosition + Screen.height;
                break;
            case Direction.left:
                visiblePosition.x = inDirPosition;
                hidePosition.x = inDirPosition - Screen.height;
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="state"></param>
    public void SetWindowIsActive(bool state)
    {
        isActive = state;
    }
}
