﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SlotData))]
public class DisplayEquipmentName : MonoBehaviour
{
    public UILabel uiName;

    SlotData slot;

    void OnEnable()
    {
        //PlayerUIReference.Instance.equipment.itemContainer.AddedItem += equipment_AddedItem;
        //PlayerUIReference.Instance.equipment.itemContainer.RemovedItem += equipment_removedItem;
    }

    //void equipment_removedItem(ItemData arg1, bool arg2)
    //{
    //    if ((PlayerUIReference.Instance.equipment.itemContainer as SlottedItemContainer).FindItemInSlot(arg1) == slot.slotID)
    //    {
    //        uiName.text = "Empty";
    //    }
    //}

    //void equipment_AddedItem(ItemData arg1, bool arg2)
    //{
    //    if ((PlayerUIReference.Instance.equipment.itemContainer as SlottedItemContainer).FindItemInSlot(arg1) == slot.slotID)
    //    {
    //        uiName.text = arg1.itemName;
    //    }
    //}

    // Use this for initialization
    void Awake()
    {
        slot = GetComponent<SlotData>();

    }
}
