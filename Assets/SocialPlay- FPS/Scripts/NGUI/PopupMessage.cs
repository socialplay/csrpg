﻿using UnityEngine;
using System.Collections;

public class PopupMessage : MonoBehaviour
{
    PopupMessageBackground messageBackground;

    AutoTimer fadeWaitTimer;
    TweenAlpha tweenAlpha;

    bool isPermanentMessage = false;
    bool waitForFade = false;

    void Start()
    {
        messageBackground = GetComponentInChildren<PopupMessageBackground>();
        tweenAlpha = GetComponent<TweenAlpha>();
    }

    void Update()
    {
        if (waitForFade)
        {
            if (fadeWaitTimer.IsDone())
            {
                waitForFade = false;

                fadeMessage();
            }
        }
    }

    void initializeMessage(string message, float displayTime, bool toggleDisplay)
    {
        if (messageBackground == null)
        {
            messageBackground = GetComponentInChildren<PopupMessageBackground>();
        }

        isPermanentMessage = toggleDisplay;

        fadeWaitTimer = new AutoTimer(displayTime);

        messageBackground.SetMessage(message);

        messageBackground.AdjustBackgroundFrame();
    }

    void fadeMessage()
    {
        GetComponent<TweenAlpha>().enabled = true;
    }

    /// <summary>
    /// Initialize the popup message and keep displayed.  Call FadeMessage() to fade this message.
    /// </summary>
    /// <param name="message">String message you want to display.</param>
    public void InitMessage(string message)
    {
        initializeMessage(message, 0, true);
    }

    /// <summary>
    /// Initialize the popup message that will fade once display time finishes.
    /// </summary>
    /// <param name="message">String message you want to display.</param>
    /// <param name="displayTime">Duration in seconds you want the message to display before fading.</param>
    public void InitMessage(string message, float displayTime)
    {
        initializeMessage(message, displayTime, false);
    }

    /// <summary>
    /// Start the fade tween for this popup message.
    /// </summary>
    public void FadeMessage() {
        if (isPermanentMessage)
        {
            tweenAlpha.enabled = isPermanentMessage;
        }
    }

    /// <summary>
    /// Hooked on to Tween Scale 'On Finished'.
    /// </summary>
    public void MessageScaleComplete()
    {
        waitForFade = true;
    }

    /// <summary>
    /// Hooked on to Tween Alpha 'On Finished'.
    /// </summary>
    public void MessageAlphaFadeComplete()
    {
        Destroy(gameObject);
    }
}
