﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(TweenPosition))]
public class SkillTweenManager : MonoBehaviour
{

    public NGUILimitedGridItemContainerDisplay inventory;

    public Vector3 outPos;
    public Vector3 inPos;

    bool isShown = false;
    TweenPosition tween;

    void Awake()
    {
        tween = this.GetComponent<TweenPosition>();
    }

    void Update()
    {
        if (isShown != inventory.IsWindowActive())
        {
            isShown = inventory.IsWindowActive();
            if (isShown)
            {
                tween.from = this.transform.localPosition;
                tween.to = inPos;               
            }
            else
            {
                tween.from = this.transform.localPosition;
                tween.to = outPos;
            }
            tween.Reset();
            tween.enabled = true;
        }
    }
}
