﻿using UnityEngine;
using System.Collections;

public class NGUITweenKeybinding : MonoBehaviour
{
    public KeyCode binding;

    private NGUITweenWrapper tweenWrapper;

    void Start()
    {
        tweenWrapper = gameObject.GetComponent<NGUITweenWrapper>();
    }

    void Update()
    {
        if (Input.GetKeyDown(binding))
        {
            tweenWrapper.isActive = !tweenWrapper.isActive;
        }
    }
}
