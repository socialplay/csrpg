﻿using UnityEngine;
using System.Collections;

public class PopupMessageBackground : MonoBehaviour
{
    public int backgroundHeightOffset = 8;

    UILabel label;
    UISprite background;

    void Awake()
    {
        label = GetComponentInChildren<UILabel>();
        background = GetComponent<UISprite>();
    }

    /// <summary>
    /// Adjust the background frame to fit around the message label.
    /// </summary>
    public void AdjustBackgroundFrame()
    {
        background.height = label.height + backgroundHeightOffset;
    }

    /// <summary>
    /// Set the label text for this popup message.
    /// </summary>
    /// <param name="message">String of the message you want to show.</param>
    public void SetMessage(string message)
    {
        label.text = message;
    }
}
